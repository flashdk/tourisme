module.exports = {
    "root": true,
    "env": {
        "browser": true,
        "es6": true
    },
    "extends": [
        "eslint:recommended",
        "plugin:@typescript-eslint/recommended"
    ],
    "parser": "@typescript-eslint/parser",
    "parserOptions": {
        "ecmaVersion": 2018,
        "sourceType": "module",
        "parser": "babel-eslint"
    },
    "plugins": [
        "@typescript-eslint"
    ],
    "rules": {
        "eqeqeq": "warn",
        "indent": ["warn", 2, { "SwitchCase": 1 }],
        "multiline-ternary": ["warn", "always-multiline"]
    }
}
