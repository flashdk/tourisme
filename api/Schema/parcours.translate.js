const mongoose = require('mongoose');

const parcoursTranslationSchema = mongoose.Schema({
    title: { type: String, required: true },
    startAt: { type: Date, default: Date.now },
    endAt: { type: Date, default: Date.now },
    coverImg: String,
    step: {
        type: Schema.Types.ObjectId,
        ref: 'step'
    },
    createdBy: {
        type: Schema.Types.ObjectId,
        ref: 'User'
    },
    createdAt: { type: Date, default: Date.now },
    updatedAt: { type: Date, default: Date.now }
});

module.exports = mongoose.model('parcoursTranslation', parcoursTranslationSchema);