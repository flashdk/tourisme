const mongoose = require('mongoose');

const userSchema = mongoose.Schema({
    firstName: { type: String, required: false },
    lastName: { type: String, required: true },
    email: {
        type: String,
        trim: true,
        lowercase: true,
        unique: true,
        match: [/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/, 'Please fill a valid email address']
    },
    telephone: String,
    country: String,
    role: {
        type: Schema.Types.ObjectId,
        ref: 'roles'
    },
    password: String,
    parcours: {
        type: Schema.Types.ObjectId,
        ref: 'parcours'
    },
    defaultLang: String,
    avatar: String,
    createdAt: { type: Date, default: Date.now },
    updatedAt: { type: Date, default: Date.now }
});

module.exports = mongoose.model('users', userSchema);