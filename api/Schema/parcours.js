const mongoose = require('mongoose');

const parcoursSchema = mongoose.Schema({
    title: { type: String, required: true },
    users: Array,
    startAt: { type: Date, default: Date.now },
    endAt: { type: Date, default: Date.now },
    coverImg: String,
    step: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'step'
    },
    createdAt: { type: Date, default: Date.now },
    updatedAt: { type: Date, default: Date.now }
});

module.exports = mongoose.model('parcours', parcoursSchema);