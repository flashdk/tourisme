const mongoose = require('mongoose');

const stepsSchema = mongoose.Schema({
    title: { type: String, required: true },
    startAt: { type: Date, default: Date.now },
    endAt: { type: Date, default: Date.now },
    pictures: String,
    description: String,
    basicPrice: Number,
    service: Array,
    createdAt: { type: Date, default: Date.now },
    updatedAt: { type: Date, default: Date.now }
});

module.exports = mongoose.model('steps', stepsSchema);