const mongoose = require('mongoose');

const roleSchema = mongoose.Schema({
    title: { type: String, required: true },
    permissions: Array,
    isDefault: Boolean,
    createdAt: { type: Date, default: Date.now },
    updatedAt: { type: Date, default: Date.now },
    updatedBy: { type: Schema.Types.ObjectId },
});

module.exports = mongoose.model('roles', roleSchema);