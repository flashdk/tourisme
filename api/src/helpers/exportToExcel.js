// import * as XLSX from 'xlsx/xlsx.mjs';
var XLSX = require("xlsx");

const generateWorkSheet = (workssheetInfo) => {
  const ws = XLSX.utils.json_to_sheet(workssheetInfo.data, { skipHeader: false })
  return ws
}


const generateExcel = ({ filename = 'output.xlsx', worksheetsInfo = [] } = {}) => {

  const wb = XLSX.utils.book_new();

  worksheetsInfo.forEach((element) => {
    const ws = generateWorkSheet(element)
    XLSX.utils.book_append_sheet(wb, ws, element.title)
  })

  XLSX.writeFile(wb, filename)
}

module.exports = generateExcel;


