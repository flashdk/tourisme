const generateExcel = require('./exportToExcel')

const wsv = {
    title: 'Ville',
    data: [
        {
            Ville: "paris",
            Pays: "France"
        }
    ]
}

const wsa = {
    title: 'Animals',
    data: [
        {
            name: "singe",
            type: "terrestre"
        }
    ]
}

generateExcel({
    filename: './../../allFiles/excel/blabla.xlsx',
    worksheetsInfo: [
       wsa, wsv
    ]
})