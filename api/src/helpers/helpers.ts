export class helpers {
  static dataPagination(p=0, pp=0, f='', s='') {
    const page = p
    const perPage = pp 
    const sort = f 
    const search = s 

    return { page, perPage, sort, search } as Pagination
  }
}

export interface Pagination {
  page: number,
  perPage: number,
  sort: string,
  search: string
}