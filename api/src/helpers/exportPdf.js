import jsPDF from "jspdf";
// import autoTable from 'jspdf-autotable';
// const jsPDF = require('jspdf')
const autoTable = require('jspdf-autotable')

const doc = new jsPDF()

autoTable(doc, {
  head: [['Name', 'Email', 'Country']],
  body: [
    ['David', 'david@example.com', 'Sweden'],
    ['Castille', 'castille@example.com', 'Spain'],
    // ...
  ],
})

doc.save('./../../allFiles/pdf/table.pdf')