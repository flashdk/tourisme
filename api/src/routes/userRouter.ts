import { Router } from "express";
import { createUserController } from "../useCases/User/CreateUser";
import { deleteUserController } from "../useCases/User/DeleteUser";
import { getUserController } from "../useCases/User/GetUsers";
import { updateUserController } from "../useCases/User/UpdateUsers";
import { authUserController } from "../useCases/User/Auth";
import { getUserByIdController } from "../useCases/User/GetUserById";
import { findUserController } from "../useCases/User/UserExist";
import  { multerUpload } from "./../middleware/multer-config";
import { auth } from "../middleware/auth";

const userRouter = Router()

userRouter.post('/api/users', auth, multerUpload('avatar'), (request, response) => {
  return createUserController.handle(request, response);
});

userRouter.get('/api/users', auth, (request, response) => {
  return getUserController.handle(request, response);
});

userRouter.get('/api/users/:id', auth, (request, response) => {
  return getUserByIdController.handle(request, response);
});

userRouter.get('/api/users/:email?/:telephone?', auth, (request, response) => {
  return findUserController.handle(request, response);
});

userRouter.put('/api/users/:id', auth, multerUpload('avatar'), (request, response) => {
  return updateUserController.handle(request, response);
});

userRouter.delete('/api/users/:id', auth, (request, response) => {  
  return deleteUserController.handle(request, response);
});

userRouter.post('/api/auth', (request, response) => {
  return authUserController.handle(request, response);
});

export { userRouter }