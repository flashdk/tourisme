import { userRouter } from "./userRouter";
import { roleRouter } from "./roleRouter";
import { parcoursRouter } from "./parcoursRouter";
import { stepsRouter } from "./StepsRouter";
import { uploadFileRouter } from "./uploadFileRouter";



export { userRouter, roleRouter, parcoursRouter, stepsRouter, uploadFileRouter}