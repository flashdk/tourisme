import { Router } from "express";
import  { multerUpload } from "./../middleware/multer-config";
import { auth } from "../middleware/auth";
import { addFileController } from "../useCases/uploadFile/create";

const uploadFileRouter = Router()

uploadFileRouter.post('/api/uploadFile', auth, multerUpload('file'), (request, response) => {
  console.log('request', request.body);
    
  return addFileController.handle(request, response);
});

export { uploadFileRouter }