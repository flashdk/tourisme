import { Router } from "express";
import { createStepsController } from "../useCases/Steps/Create";
import { getStepsController } from "../useCases/Steps/GetSteps";
import { getStepsByIdController } from "../useCases/Steps/GetStepsById";
import { deleteStepsController } from "../useCases/Steps/DeleteStep";
import { updateStepsController } from "../useCases/Steps/UpdateSteps";
import { multerUpload } from "./../middleware/multer-config";

import { auth } from "../middleware/auth";

const stepsRouter = Router()

stepsRouter.post('/api/steps', auth, multerUpload('pictures'), (request, response) => {
  return createStepsController.handle(request, response);
});

stepsRouter.get('/api/steps', auth, (request, response) => {
  return getStepsController.handle(request, response);
});

stepsRouter.get('/api/steps/:id', auth, (request, response) => {
  return getStepsByIdController.handle(request, response);
});


stepsRouter.put('/api/steps/:id', auth, multerUpload('pictures'), (request, response) => {
  return updateStepsController.handle(request, response);
});

stepsRouter.delete('/api/steps/:id', auth, (request, response) => {
  return deleteStepsController.handle(request, response);
});

export { stepsRouter }