import { Router } from "express";
import { createServiceController } from "../useCases/services/CreateService";
import { deleteServiceController } from "../useCases/services/DeleteService";
import { getServiceController } from "../useCases/services/GetServices";
import { getServiceByIdController } from "../useCases/services/GetServiceById";
import { updateServiceController } from "../useCases/services/UpdateService";
import { auth } from "../middleware/auth";

const serviceRouter = Router()

serviceRouter.post('/api/services', auth, (request, response) => {
  console.log('save service');
  
  return createServiceController.handle(request, response);
});

serviceRouter.get('/api/services', auth, (request, response) => {
  return getServiceController.handle(request, response);
});

serviceRouter.get('/api/services/:id', auth, (request, response) => {
  return getServiceByIdController.handle(request, response);
});


serviceRouter.put('/api/services/:id', auth, (request, response) => {
  return updateServiceController.handle(request, response);
});

serviceRouter.delete('/api/services/:id', auth, (request, response) => {
  return deleteServiceController.handle(request, response);
});

export { serviceRouter }