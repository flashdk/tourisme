import { Router } from "express";
import { createParcoursController } from "../useCases/Parcours/CreateParcours";
import { multerUpload } from "./../middleware/multer-config";

import { deleteParcoursController } from "../useCases/Parcours/DeleteParcours";
import { getParcoursController } from "../useCases/Parcours/GetParcours";
import { getParcoursByIdController } from "../useCases/Parcours/GetParcoursById"
import { updateParcoursController } from "../useCases/Parcours/UpdateParcours";
import { auth } from "../middleware/auth";

const parcoursRouter = Router()

parcoursRouter.post('/api/parcours', auth, multerUpload('coverImg'), (request, response) => {
  return createParcoursController.handle(request, response);
});

parcoursRouter.get('/api/parcours', auth, (request, response) => {
  return getParcoursController.handle(request, response);
});

parcoursRouter.get('/api/parcours/:id', auth, (request, response) => {
  return getParcoursByIdController.handle(request, response);
});


parcoursRouter.put('/api/parcours/:id', auth, multerUpload('coverImg'), (request, response) => {
  return updateParcoursController.handle(request, response);
});

parcoursRouter.delete('/api/parcours/:id', auth, (request, response) => {
  return deleteParcoursController.handle(request, response);
});

export { parcoursRouter }