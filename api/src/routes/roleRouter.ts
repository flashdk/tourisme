import { Router } from "express";
import { createRoleController } from "../useCases/Role/CreateRole";
import { deleteRoleController } from "../useCases/Role/DeleteRole";
import { getRoleController } from "../useCases/Role/GetRoles";
import { getRoleByIdController } from "../useCases/Role/GetRoleById";
import { updateRoleController } from "../useCases/Role/UpdateRole";
import { auth } from "../middleware/auth";

const roleRouter = Router()

roleRouter.post('/api/roles', auth, (request, response) => {
  return createRoleController.handle(request, response);
});

roleRouter.get('/api/roles', auth, (request, response) => {
  return getRoleController.handle(request, response);
});

roleRouter.get('/api/roles/:id', auth, (request, response) => {
  return getRoleByIdController.handle(request, response);
});


roleRouter.put('/api/roles/:id', auth, (request, response) => {
  return updateRoleController.handle(request, response);
});

roleRouter.delete('/api/roles/:id', auth, (request, response) => {
  return deleteRoleController.handle(request, response);
});

export { roleRouter }