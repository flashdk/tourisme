const jwt = require('jsonwebtoken');

const auth = (req, res, next) => {

    try {
        const token = req.headers.authorization.split(' ')[1];
        const decodedToken = jwt.verify(token, 'OZVV5TpP4U6wJ&thaCORZ@EQ');
        
        if (decodedToken.userId) {
            next();
        } else {
            throw 'Non authentifié';
        }
    } catch {
        res.status(401).json({
            error: 'Utilisateur non authentifié'
        });
    }
};

export { auth }