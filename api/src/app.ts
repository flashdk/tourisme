
import express from 'express'
import { userRouter } from './routes/routes'
import { roleRouter } from './routes/routes'
import { parcoursRouter } from './routes/routes'
import { stepsRouter } from './routes/routes'
import { uploadFileRouter } from './routes/routes'
import { serviceRouter } from './routes/serviceRouter'
import { UserModel } from "./entities/User";
import { RoleModel } from "./entities/Role";
import mongoose from "mongoose";


// import {morgan} from 'morgan'
const morgan = require('morgan');
const bodyParser = require('body-parser');
// const mongoose = require('mongoose');
const path = require('path');
const cors = require('cors')

const app = express()

app.use(cors())
app.use(morgan('dev'))
app.use(bodyParser.json())
app.use(bodyParser.urlencoded({ extends: false }))
app.use('/images', express.static(path.join(process.env.PWD, 'images')));

// const uri =
// process.env.NODE_ENV === 'production'
// ? `mongodb://${process.env.MONGO_USERNAME}:${process.env.MONGO_PWD}@db/tourismInAfricaProd`
// : `mongodb://db/tourismInAfrica`;

const uri =
process.env.NODE_ENV === 'production'
? `mongodb://${process.env.MONGO_USERNAME}:${process.env.MONGO_PWD}@db/tourismInAfricaProd`
: `mongodb://db/tourismInAfrica`;

mongoose.connect(uri,
  {
    useNewUrlParser: true,
    useUnifiedTopology: true
  })
  .then(async () => {
    console.log('Connexion à MongoDB réussie !')
    let exist = undefined 
    exist = await UserModel.findOne().or([
      { email: "admin@gmail.com" },
    ]);

    if (!exist) {
      const newUser = new UserModel({
        "firstName": "user",
        "lastName": "system",
        "email": "admin@gmail.com",
        "telephone": "901235874",
        "country": "Tog",
        "role": mongoose.Types.ObjectId("62c3596754e2c42dd478c18c"),
        "password": "$2a$12$rfbtNIXjh80wOXdd54/bBOVpYL21KbOUj/Xwj1MbFUHS5J7br6rDO",
        "avatar": "http://localhost:3003/images/avatar_1677173440586.jpg",
        "createdAt": new Date(),
        "updatedAt": new Date()
      }
      );

      const newRole = new RoleModel({
        "_id": mongoose.Types.ObjectId("62c3596754e2c42dd478c18c"),
        "title": "admin",
        "permissions": [
          "App.Admin"
        ],
        "isDefault": true,
        "createdAt": new Date(),
        "updatedAt": new Date()
      })

      try {
        await newRole.save();
        await newUser.save();
      } catch (err) {
        console.log('Failed to init db !', err);
        return err
      }
    }
  })
  .catch(() => console.log('Connexion à MongoDB échouée !'));


app.use(express.json())
app.use(userRouter)
app.use(uploadFileRouter)
app.use(roleRouter)
app.use(serviceRouter)
app.use(parcoursRouter)
app.use(stepsRouter)

export { app }