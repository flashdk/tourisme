import { model, Schema } from "mongoose";

export interface Role {
  _id: Schema.Types.ObjectId;
  title: string;
  permissions: Array<any>;
  isDefault: boolean;
  createdAt?: Date;
  updatedAt?: Date;
  createdBy?: Schema.Types.ObjectId;
  updatedBy?: Schema.Types.ObjectId;
  
}

const schema = new Schema({
  title: { type: String, required: true },
  permissions: { type: Array, required: false },
  isDefault: { type: Boolean, required: true },
  createdAt: { type: Date, default: Date.now },
  updatedAt: { type: Date, default: Date.now },
  createdBy: {
    type: Schema.Types.ObjectId,
    ref: 'User'
  },
  updatedBy: {
    type: Schema.Types.ObjectId,
    ref: 'User'
  }
});

export const RoleModel = model<Role>('role', schema);
