import { uuid } from 'uuidv4'
import { model, Schema } from "mongoose";

export class ParcoursTranslate {

  public readonly id?: string;
   
  public title: string;
  public startAt: Date;
  public endAt: Date;
  public coverImg: string;
  //   public step: Schema.Types.ObjectId;
  public createdBy = Schema.Types.ObjectId;
  public createdAt: Date;
  public updatedAt: Date;


  constructor(props: Omit<ParcoursTranslate, 'id'>, id?: string) {
    this.title = props.title
    this.startAt = props.startAt
    this.endAt = props.endAt
    this.coverImg = props.coverImg
    // this.step = props.step
    this.createdBy = props.createdBy
    this.createdAt = props.createdAt
    this.updatedAt = props.updatedAt
  }
}

const parcoursTranslateSchema = new Schema<ParcoursTranslate>({
  title: { type: String, required: true },
  startAt: { type: Date, default: Date.now },
  endAt: { type: Date, default: Date.now },
  coverImg: String,
  //   step: {
  //     type: Schema.Types.ObjectId,
  //     ref: 'step'
  //   },
  createdBy: {
    type: Schema.Types.ObjectId,
    ref: 'User'
  },
  createdAt: { type: Date, default: Date.now },
  updatedAt: { type: Date, default: Date.now }
});

export const ParcoursTranslateSchema = model<ParcoursTranslate>('parcoursTranslate', parcoursTranslateSchema);