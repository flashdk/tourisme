import { model, Schema } from "mongoose";

export interface Service {
  readonly id?: string;   
  title: string;
  pictures?: object;
  description?: string;
  basicPrice: string;
  createdAt: Date;
  createdBy?: Schema.Types.ObjectId;
  updatedBy?: Schema.Types.ObjectId;
}

const schema = new Schema({
  title: { type: String, required: true },
  pictures: Object,
  description: String,
  basicPrice: String,
  createdAt: { type: Date, default: Date.now },
  createdBy: {
    type: Schema.Types.ObjectId,
    ref: 'User'
  },
  updatedBy: {
    type: Schema.Types.ObjectId,
    ref: 'User'
  }
});

export const ServiceModel = model<Service>('services', schema);