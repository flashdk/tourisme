import { model, Schema, Document } from "mongoose";

export interface User {

  id: Schema.Types.ObjectId;
  firstName: string;
  lastName: string;
  email: string;
  telephone: string;
  country: string;
  role?: Schema.Types.ObjectId;
  parcours?: Schema.Types.ObjectId;
  password: string;
  defaultLang: string;
  avatar?: string;
  createdAt?: Date;
  updatedAt?: Date;
}

const schema = new Schema({
  firstName: { type: String, required: false },
  lastName: { type: String, required: true },
  email: {
    type: String,
    trim: true,
    lowercase: true,
    unique: true,
    match: [/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/, 'Please fill a valid email address']
  },
  telephone: { type: String },
  country: String,
  role: {
    type: Schema.Types.ObjectId,
    ref: 'role'
  },
  password: String,
  parcours: {
    type: Schema.Types.ObjectId,
    ref: 'parcours'
  },
  defaultLang: String,
  avatar: String,
  createdAt: { type: Date, default: Date.now },
  updatedAt: { type: Date, default: Date.now }
});

export const UserModel = model<User>('user', schema);
