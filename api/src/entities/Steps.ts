import { number } from "joi";
import { model, Schema } from "mongoose";

export interface Steps {
  parcoursId?: Schema.Types.ObjectId;
  title: string,
  startAt: Date,
  endAt: Date,
  pictures: string,
  description: string,
  basicPrice: number,
  services: Array<unknown>,
  createdAt?: Date;
  updatedAt?: Date;
  createdBy?: Schema.Types.ObjectId;
  updatedBy?: Schema.Types.ObjectId;
}

export interface services {
  title: string,
  description: string,
  price: number,
}

const services = new Schema({
  title: String,
  description: String,
  price: Number,
})

const schema = new Schema({
  parcoursId: {
    type: Schema.Types.ObjectId,
    ref: 'Parcours'
  },
  title: { type: String, required: true },
  startAt: { type: Date, default: Date.now, required: true },
  endAt: { type: Date, default: Date.now, required: true },
  pictures: { type: Object },
  description: { type: String },
  basicPrice: { type: Number },
  services: {
    type: [Schema.Types.ObjectId], default: []
  },
  createdAt: { type: Date, default: Date.now },
  updatedAt: { type: Date, default: Date.now },
  createdBy: {
    type: Schema.Types.ObjectId,
    required: false,
    ref: 'User'
  },
  updatedBy: {
    type: Schema.Types.ObjectId,
    required: false,
    ref: 'User'
  }
});


export const StepsModel = model<Steps>('steps', schema);