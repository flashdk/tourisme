import { model, Schema } from "mongoose";

export interface Parcours {
    _id: Schema.Types.ObjectId;
    title: string;
    startAt: Date;
    endAt: Date;
    coverImg?: object;
    // step?: Schema.Types.ObjectId;
    createdBy: Schema.Types.ObjectId;
    updatedBy?: Schema.Types.ObjectId;
    createdAt: Date;
    updatedAt?: Date;
}

const schema = new Schema({
  title: { type: String, required: true },
  startAt: { type: Date, default: Date.now },
  endAt: { type: Date, default: Date.now },
  coverImg: Object,
  // step: {
  //     type: Schema.Types.ObjectId,
  //     ref: 'Step'
  // },
  createdBy: {
    type: Schema.Types.ObjectId,
    ref: 'User'
  },
  updatedBy: {
    type: Schema.Types.ObjectId,
    ref: 'User'
  },
  createdAt: { type: Date, default: Date.now },
  updatedAt: { type: Date, default: Date.now }
});

export const ParcoursModel = model<Parcours>('parcours', schema);