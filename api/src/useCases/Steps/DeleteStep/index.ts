
import { StepsRepository } from "../../../repositories/implementations/mongodb/StepsRepository";
import { DeleteStepsUseCase } from "./DeleteStepsUseCase";
import { DeleteStepsController } from "./DeleteStepsController";

const stepsRepository = new StepsRepository();

const deleteStepsUseCase = new DeleteStepsUseCase(stepsRepository);

const deleteStepsController = new DeleteStepsController(deleteStepsUseCase);

export { deleteStepsUseCase, deleteStepsController };