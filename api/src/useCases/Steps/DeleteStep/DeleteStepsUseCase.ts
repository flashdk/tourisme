import { IStepsRepository } from "../../../repositories/IStepsRepository";

export class DeleteStepsUseCase {
  constructor(private stepsRepository: IStepsRepository) {}

  async execute(id: string): Promise<void> {
    await this.stepsRepository.delete(id);
  }
}