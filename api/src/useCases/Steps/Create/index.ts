
import { StepsRepository } from "../../../repositories/implementations/mongodb/StepsRepository";
import { CreateStepsUseCase } from "./CreateStepsUseCase";
import { CreateStepsController } from "./CreateStepsController";

const stepsRepository = new StepsRepository()

const createStepUseCase = new CreateStepsUseCase(
  stepsRepository,
)

const createStepsController = new CreateStepsController(
  createStepUseCase
)

export { createStepUseCase, createStepsController }