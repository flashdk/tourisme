import { Schema } from "mongoose";

export interface ICreateStepsDTO {
  parcoursId: string,
  title: string,
  startAt: Date,
  endAt: Date,
  pictures: string,
  description: string,
  basicPrice: number,
  curency: string,
  services: Array<unknown>,
  createdAt?: Date,
  createdBy?: Schema.Types.ObjectId
} 