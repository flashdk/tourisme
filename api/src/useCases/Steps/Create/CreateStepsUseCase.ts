import { IStepsRepository } from "../../../repositories/IStepsRepository";
import { ICreateStepsDTO } from "./CreateStepsDTO";
import { Steps } from "../../../entities/Steps";

export class CreateStepsUseCase {
  constructor(
    private stepsRepository: IStepsRepository,
  ) {}
 
  async execute(data: ICreateStepsDTO): Promise<Steps> {
    return await this.stepsRepository.save(data);
  }
}