import { Request, Response } from "express";
import { CreateStepsUseCase } from "./CreateStepsUseCase";
import Joi from "joi";

export class CreateStepsController {
  constructor(
    private createStepsUseCase: CreateStepsUseCase,
  ) { }

  async handle(request: Request, response: Response): Promise<Response> {

    // const x = JSON.parse(request.body.services)

    const validation = Joi.object({
      title: Joi.string(),
      startAt: Joi.date().required(),
      endAt: Joi.date().required(),
      pictures: Joi.object(),
      description: Joi.string(),
      curency: Joi.string(),
      basicPrice: Joi.number(),
      // services: Joi.string().optional().empty(''),
      // services: Joi.array().items(Joi.object({
      //   "_id": Joi.string(),
      //   "title": Joi.string(),
      //   "description": Joi.string(),
      //   "basicPrice": Joi.number(),
      //   "createdAt": Joi.date(),
      //   "updatedAt": Joi.date(),
      //   "createdBy": Joi.string(),
      //   "pictures": Joi.object(),
      //   "__v": Joi.number()
      // })).optional(),
      services: Joi.array().optional(),
      createdAt: Joi.date(),
      createdBy: Joi.string(),
      parcoursId: Joi.string(),
    }).validate({
      ...request.body,
    })

    if (validation.error) {
      return response.status(400).json({
        success: false,
        message: `${validation.error.message}`
      })
    }

    try {
      const data = await this.createStepsUseCase.execute(validation.value)
      return response.status(201).json({ success: true, data: data })
    } catch (error: any) {
      console.log('error', error);
      
      if (error.error === 'steps_exist') {
        return response.status(400).json(error)
      } else {
        return response.status(400).json({ code: 'Unexpected error.' })
      }
    }
  }
}