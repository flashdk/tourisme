
import { StepsRepository } from "../../../repositories/implementations/mongodb/StepsRepository";
import { GetStepsByIdUseCase } from "./GetStepsByIdUseCase";
import { GetStepsByIdController } from "./GetStepsByIdController";

const stepsRepository = new StepsRepository();

const getStepsByIdUseCase = new GetStepsByIdUseCase(stepsRepository);

const getStepsByIdController = new GetStepsByIdController(getStepsByIdUseCase);

export { getStepsByIdUseCase, getStepsByIdController };