import { Request, Response } from "express";
import { GetStepsByIdUseCase } from "./GetStepsByIdUseCase";
import Joi from "joi"; 

export class GetStepsByIdController {
  constructor(private getStepsByIdUseCase: GetStepsByIdUseCase) {}

  async handle(request: Request, response: Response): Promise<Response> {
    const validation = Joi.object({
      id: Joi.string(),
    }).validate({
      ...request.params,
    });

    if (validation.error) {
      return response.status(400).json({
        success: false,
        message: `${validation.error.message}`,
      });
    }

    try {
      const steps = await this.getStepsByIdUseCase.execute(validation.value.id);

      return response.status(200).send({ sucess: true, data: steps });
    } catch (error: any) {
      return response.status(400).json(error || "Unexpected error.");
    }
  }
}