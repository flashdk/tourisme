import { IStepsRepository } from "../../../repositories/IStepsRepository";
import { IGetStepsByIdDTO } from "./GetStepsByIdDTO";

export class GetStepsByIdUseCase {
  constructor(
    private stepssRepository: IStepsRepository,
  ) {}
 
  async execute(id: string): Promise<IGetStepsByIdDTO> {
    return this.stepssRepository.getById(id);
  }
}