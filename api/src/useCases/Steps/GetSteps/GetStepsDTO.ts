import { Schema } from "mongoose";

export interface IGetStepsDTO {
  // _id: Schema.Types.ObjectId;
  title: string, 
  startAt: Date,
  endAt: Date,
  pictures: string,
  description: string,
  basicPrice: number,
  services: Array<unknown>,
  createdAt?: Date;
  updatedAt?: Date;
  createdBy?: Schema.Types.ObjectId;
  updatedBy?: Schema.Types.ObjectId;
} 