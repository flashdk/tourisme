import { Request, Response } from "express";
import { GetStepsUseCase } from "./GetStepsUseCase";
import { IGetStepsDTO } from "./GetStepsDTO";

export class GetStepsController {
  constructor(private getStepsUseCase: GetStepsUseCase) {}

  async handle(request: Request, response: Response): Promise<Response> {
    try {
      const roles: IGetStepsDTO[] = await this.getStepsUseCase.execute();

      return response.status(200).send({ sucess: true, data: roles });
    } catch (error: any) {
      return response.status(400).json(error.message || "Unexpected error.");
    }
  }
}