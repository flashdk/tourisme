
import { StepsRepository } from "../../../repositories/implementations/mongodb/StepsRepository";
import { GetStepsUseCase } from "./GetStepsUseCase";
import { GetStepsController } from "./GetRoleController";

const stepsRepository = new StepsRepository();

const getStepsUseCase = new GetStepsUseCase(stepsRepository);

const getStepsController = new GetStepsController(getStepsUseCase);

export { getStepsUseCase, getStepsController };