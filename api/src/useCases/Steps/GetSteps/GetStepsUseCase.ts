import { IStepsRepository } from "../../../repositories/IStepsRepository";
import { IGetStepsDTO } from "./GetStepsDTO";

export class GetStepsUseCase {
  constructor(private stepsRepository: IStepsRepository) {}

  async execute(): Promise<IGetStepsDTO[]> {
    const steps: IGetStepsDTO[] = await this.stepsRepository.get();
    return steps;
  }
}