import { Schema } from "mongoose";

export interface IUpdateStepsDTO {
  id: Schema.Types.ObjectId,
  title: string,
  startAt: Date,
  endAt: Date,
  pictures: string,
  description: string,
  basicPrice: number,
  services: Array<unknown>,
  updatedAt?: Date,
  updatedBy?: Schema.Types.ObjectId
} 