import { Steps } from "../../../entities/Steps";
import { IStepsRepository } from "../../../repositories/IStepsRepository";
import { IUpdateStepsDTO } from "./UpdateStepsDTO";

export class UpdateStepsUseCase {
  constructor(private stepsRepository: IStepsRepository) {}

  async execute(step: IUpdateStepsDTO): Promise<IUpdateStepsDTO> {
    const update = await this.stepsRepository.update(step);
    return update as IUpdateStepsDTO;
  }
}