import { Request, Response } from "express";
import { UpdateStepsUseCase } from "./UpdateStepsUseCase";
import Joi from "joi"; 

export class UpdateStepsController {
  
  constructor(private updateStepsUseCase: UpdateStepsUseCase) {}

  async handle(request: Request, response: Response): Promise<Response> {
    
    const validation = Joi.object({
      id: Joi.string(),
      title: Joi.string(),
      startAt: Joi.date().required(),
      endAt: Joi.date().required(),
      pictures: Joi.string(),
      description: Joi.string(),
      basicPrice: Joi.number(),
      service: Joi.array().optional().empty(''),
      updatedAt: Joi.date(),
      updatedBy: Joi.string(),
    }).validate({
      ...request.body,
      ...request.params
    });

    if (validation.error) {
      try {
        return response.status(400).json(JSON.parse(validation.error.message));
      } catch (err) {
        return response.status(400).json({
          success: false,
          message: `${validation.error.message}`,
        });
      }
    }

    try {
      const updateData = await this.updateStepsUseCase.execute(validation.value);

      return response.status(200).send({ sucess: true, data: updateData });
    } catch (error: any) {
      return response.status(400).json(error.message || "Unexpected error.");
    }
  }
}