
import { StepsRepository } from "../../../repositories/implementations/mongodb/StepsRepository";
import { UpdateStepsUseCase } from "./UpdateStepsUseCase";
import { UpdateStepsController } from "./UpdateStepsController";

const stepsRepository = new StepsRepository();

const updateStepsUseCase = new UpdateStepsUseCase(stepsRepository);

const updateStepsController = new UpdateStepsController(updateStepsUseCase);

export { updateStepsUseCase, updateStepsController };