import { Request, Response } from "express";
import { DeleteParcoursUseCase } from "./DeleteParcoursUseCase";
import Joi from "joi"; 

export class DeleteParcoursController {
  constructor(private deleteParcoursUseCase: DeleteParcoursUseCase) {}

  async handle(request: Request, response: Response): Promise<Response> {
    const validation = Joi.object({
      id: Joi.string(),
    }).validate({
      ...request.params,
    });

    if (validation.error) {
      try {
        return response.status(400).json(JSON.parse(validation.error.message));
      } catch (err) {
        return response.status(400).json({
          success: false,
          message: `${validation.error.message}`,
        });
      }
    }

    try {
      await this.deleteParcoursUseCase.execute(validation.value.id);
      return response.status(200).send({ sucess: true });
    } catch (error: any) {
      return response.status(400).json(error.message || "Unexpected error.");
    }
  }
}