import { IParcoursRepository } from "../../../repositories/IParcoursRepository";

export class DeleteParcoursUseCase {
  constructor(private parcoursRepository: IParcoursRepository) {}

  async execute(id: string): Promise<void> {
    await this.parcoursRepository.delete(id);
  }
}