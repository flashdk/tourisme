import { Schema } from "mongoose";

export interface IDeleteParcoursRequestDTO {
  _id:Schema.Types.ObjectId;
} 