
import { ParcoursRepository } from "../../../repositories/implementations/mongodb/ParcoursRepository";
import { DeleteParcoursUseCase } from "./DeleteParcoursUseCase";
import { DeleteParcoursController } from "./DeleteParcoursController";

const parcoursRepository = new ParcoursRepository();

const deleteParcoursUseCase = new DeleteParcoursUseCase(parcoursRepository);

const deleteParcoursController = new DeleteParcoursController(deleteParcoursUseCase);

export { deleteParcoursUseCase, deleteParcoursController };