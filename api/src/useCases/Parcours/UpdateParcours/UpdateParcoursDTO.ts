import { Schema } from "mongoose";

export interface IUpdateParcoursRequestDTO {
  id: Schema.Types.ObjectId;
  title: string;
  startAt: Date;
  endAt: Date;
  coverImg: string;
  // step?: Schema.Types.ObjectId;
  updatedBy?: Schema.Types.ObjectId;
  updatedAt?: Date;
} 