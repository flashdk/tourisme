import { IParcoursRepository } from "../../../repositories/IParcoursRepository";
import { IUpdateParcoursRequestDTO } from "./UpdateParcoursDTO";

export class UpdateParcoursUseCase {
  constructor(private parcoursRepository: IParcoursRepository) {}

  async execute(parcours: IUpdateParcoursRequestDTO): Promise<IUpdateParcoursRequestDTO> {
    const update = await this.parcoursRepository.updateParcours(parcours);
    return update as IUpdateParcoursRequestDTO;
  }
}