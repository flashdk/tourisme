import { Request, Response } from "express";
import { UpdateParcoursUseCase } from "./UpdateParcoursUseCase";
import Joi from "joi"; 

export class UpdatePArcoursController {
  
  constructor(private updateParcoursUseCase: UpdateParcoursUseCase) {}

  async handle(request: Request, response: Response): Promise<Response> {
    
    const validation = Joi.object({
      id: Joi.string(),
      title: Joi.string(),
      startAt: Joi.date(),
      endAt: Joi.date(),
      coverImg: Joi.string(),
      updatedAt: Joi.date(),
      updatedBy: Joi.string(),
    }).validate({
      ...request.body,
      ...request.params
    });

    if (validation.error) {
      try {
        return response.status(400).json(JSON.parse(validation.error.message));
      } catch (err) {
        return response.status(400).json({
          success: false,
          message: `${validation.error.message}`,
        });
      }
    }

    try {
      const parcoursUpdate = await this.updateParcoursUseCase.execute(validation.value);

      return response.status(200).send({ sucess: true, data: parcoursUpdate });
    } catch (error: any) {
      return response.status(400).json(error.message || "Unexpected error.");
    }
  }
}