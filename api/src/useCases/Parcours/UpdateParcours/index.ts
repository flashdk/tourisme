
import { ParcoursRepository } from "../../../repositories/implementations/mongodb/ParcoursRepository";
import { UpdateParcoursUseCase } from "./UpdateParcoursUseCase";
import { UpdatePArcoursController } from "./UpdateParcoursController";

const parcoursRepository = new ParcoursRepository();

const updateParcoursUseCase = new UpdateParcoursUseCase(parcoursRepository);

const updateParcoursController = new UpdatePArcoursController(updateParcoursUseCase);

export { updateParcoursUseCase, updateParcoursController };