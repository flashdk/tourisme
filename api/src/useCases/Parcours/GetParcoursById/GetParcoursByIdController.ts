import { Request, Response } from "express";
import { GetParcoursByIdUseCase } from "./GetParcoursByIdUseCase";
import Joi from "joi"; 

export class GetParcoursByIdController {
  constructor(private getParcoursByIdUseCase: GetParcoursByIdUseCase) {}

  async handle(request: Request, response: Response): Promise<Response> {
    const validation = Joi.object({
      id: Joi.string(),
    }).validate({
      ...request.params,
    });

    if (validation.error) {
      return response.status(400).json({
        success: false,
        message: `${validation.error.message}`,
      });
    }

    try {
      const parcours = await this.getParcoursByIdUseCase.execute(validation.value.id);

      return response.status(200).send({ sucess: true, data: parcours });
    } catch (error: any) {
      return response.status(400).json(error || "Unexpected error.");
    }
  }
}