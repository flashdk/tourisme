
import { ParcoursRepository } from "../../../repositories/implementations/mongodb/ParcoursRepository";
import { GetParcoursByIdController } from "./GetParcoursByIdController";
import { GetParcoursByIdUseCase } from "./GetParcoursByIdUseCase";

const parcoursRepository = new ParcoursRepository();

const getParcoursByIdUseCase = new GetParcoursByIdUseCase(parcoursRepository);

const getParcoursByIdController = new GetParcoursByIdController(getParcoursByIdUseCase);

export { getParcoursByIdUseCase, getParcoursByIdController };