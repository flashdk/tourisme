import { IParcoursRepository } from "../../../repositories/IParcoursRepository";
import { IGetParcoursByIdRequestDTO } from "./GetParcoursByIdDTO";

export class GetParcoursByIdUseCase {
  constructor(
    private parcoursRepository: IParcoursRepository,
  ) {}
 
  async execute(id: string): Promise<IGetParcoursByIdRequestDTO> {
    return this.parcoursRepository.getParcoursById(id);
  }
}