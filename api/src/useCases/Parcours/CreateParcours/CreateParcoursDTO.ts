import { Schema } from "mongoose";

export interface ICreateParcourstDTO {
  title: string;
  startAt: Date;
  endAt: Date;
  coverImg: object;
  // step?: Schema.Types.ObjectId;
  createdBy: Schema.Types.ObjectId;
  createdAt: Date;
} 