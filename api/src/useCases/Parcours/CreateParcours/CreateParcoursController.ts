import { Request, Response } from "express";
import { CreateParcoursUseCase } from "./CreateParcoursUseCase";
import Joi from "joi";

export class CreateParcoursController {
  constructor(
    private createParcoursUseCase: CreateParcoursUseCase,
  ) { }

  async handle(request: Request, response: Response): Promise<Response> {
    const validation = Joi.object({
      title: Joi.string(),
      startAt: Joi.date().required(),
      endAt: Joi.date().required(),
      coverImg: Joi.object().optional(),
      // step: Joi.string().optional(),
      createdAt: Joi.date(),
      createdBy: Joi.string().optional()
    }).validate({
      ...request.body
    })

    if (validation.error) {
      return response.status(400).json({
        success: false,
        message: `${validation.error.message}`
      })
    }

    // let uploadData = ''
    // if(request.file.filename) {
    //   uploadData = `${request.protocol}://${request.get('host')}/images/${request.file.filename}`
    // }

    try {
      const parcours = await this.createParcoursUseCase.execute({...validation.value})
      console.log('parcours', parcours);
      
      return response.status(201).json({success: true, data: parcours})
    } catch (error: any) {
      return response.status(400).json(error)
    }
  }
}