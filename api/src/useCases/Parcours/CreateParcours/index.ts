
import { ParcoursRepository } from "../../../repositories/implementations/mongodb/ParcoursRepository";
import { CreateParcoursUseCase } from "./CreateParcoursUseCase";
import { CreateParcoursController } from "./CreateParcoursController";

const parcoursRepository = new ParcoursRepository()

const createParcoursUseCase = new CreateParcoursUseCase(
  parcoursRepository,
)

const createParcoursController = new CreateParcoursController(
  createParcoursUseCase
)

export { createParcoursUseCase, createParcoursController }