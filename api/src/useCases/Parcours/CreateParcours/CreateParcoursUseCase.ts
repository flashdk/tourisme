import { IParcoursRepository } from "../../../repositories/IParcoursRepository";
import { ICreateParcourstDTO } from "./CreateParcoursDTO";
import { Parcours } from "../../../entities/Parcours";

export class CreateParcoursUseCase {
  constructor(
    private parcoursRepository: IParcoursRepository,
  ) {}
 
  async execute(data: ICreateParcourstDTO): Promise<Parcours> {
    return await this.parcoursRepository.save(data);
  }
}