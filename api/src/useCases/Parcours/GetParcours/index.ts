
import { ParcoursRepository } from "../../../repositories/implementations/mongodb/ParcoursRepository";
import { GetParcoursUseCase } from "./GetParcoursUseCase";
import { GetParcoursController } from "./GetParcoursController";

const parcoursRepository = new ParcoursRepository();

const getParcoursUseCase = new GetParcoursUseCase(parcoursRepository);

const getParcoursController = new GetParcoursController(getParcoursUseCase);

export { getParcoursUseCase, getParcoursController };