import { IParcoursRepository } from "../../../repositories/IParcoursRepository";
import { IGetParcoursRequestDTO } from "./GetParcoursDTO";

export class GetParcoursUseCase {
  constructor(private parcoursRepository: IParcoursRepository) {}

  async execute(): Promise<IGetParcoursRequestDTO[]> {
    const parcours: IGetParcoursRequestDTO[] = await this.parcoursRepository.getParcours();
    return parcours;
  }
}