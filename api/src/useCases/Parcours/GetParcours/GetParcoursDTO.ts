import { Schema } from "mongoose";

export interface IGetParcoursRequestDTO {
  _id: Schema.Types.ObjectId;
  title: string;
  startAt: Date;
  endAt: Date;
  coverImg: string;
  step: Schema.Types.ObjectId;
  createdAt: Date;
  createdBy: Schema.Types.ObjectId;
  updatedAt?: Date;
  updatedBy?: Schema.Types.ObjectId;
} 