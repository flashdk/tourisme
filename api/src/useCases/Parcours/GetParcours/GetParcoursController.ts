import { Request, Response } from "express";
import { GetParcoursUseCase } from "./GetParcoursUseCase";
import { IGetParcoursRequestDTO } from "./GetParcoursDTO";

export class GetParcoursController {
  constructor(private getParcoursUseCase: GetParcoursUseCase) {}

  async handle(request: Request, response: Response): Promise<Response> {
    try {
      const parcours: IGetParcoursRequestDTO[] = await this.getParcoursUseCase.execute();

      return response.status(200).send({ sucess: true, data: parcours });
    } catch (error: any) {
      return response.status(400).json(error.message || "Unexpected error.");
    }
  }
}