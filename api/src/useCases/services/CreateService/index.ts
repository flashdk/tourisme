
import { ServiceRepository } from "../../../repositories/implementations/mongodb/ServiceRepository";
import { CreateServiceUseCase } from "./CreateServiceUseCase";
import { CreateServiceController } from "./CreateServiceController";

const serviceRepository = new ServiceRepository()

const createServiceUseCase = new CreateServiceUseCase(
  serviceRepository,
)

const createServiceController = new CreateServiceController(
  createServiceUseCase
)

export { createServiceUseCase, createServiceController }