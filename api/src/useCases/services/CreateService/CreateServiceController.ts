import { Request, Response } from "express";
import { CreateServiceUseCase } from "./CreateServiceUseCase";
import Joi from "joi";

export class CreateServiceController {
  constructor(
    private createServiceUseCase: CreateServiceUseCase,
  ) { }

  async handle(request: Request, response: Response): Promise<Response> {
    console.log('request.body', request.body);
    
    const validation = Joi.object({
      title: Joi.string().required(),
      description: Joi.string(),
      pictures: Joi.object().optional(),
      basicPrice: Joi.string().required(),
      createdAt: Joi.date(),
      createdBy: Joi.string().required(),
      updatedAt: Joi.string().optional()
    }).validate({
      ...request.body
    })

    if (validation.error) {
      return response.status(400).json({
        success: false,
        message: `${validation.error.message}`
      })
    }

    try {
      const service = await this.createServiceUseCase.execute(validation.value)
      return response.status(201).json({success: true, data: service})
    } catch (error: any) {
      if(error.error === 'service_exist') {
        return response.status(400).json(error)
      }else {
        return response.status(400).json({code:'Unexpected error.'})
      }
    }
  }
}