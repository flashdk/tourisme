import { IServiceRepository } from "../../../repositories/IServicesRepository";
import { ICreateServiceRequestDTO } from "./CreateServiceDTO";
import { Service } from "../../../entities/Service";

export class CreateServiceUseCase {
  constructor(
    private sevicesRepository: IServiceRepository,
  ) {}
 
  async execute(data: ICreateServiceRequestDTO): Promise<Service> {
    
    // const serviceAlreadyExists = await this.sevicesRepository.findByName(data.name);

    // if (serviceAlreadyExists) {
    //   throw new Error('Service already exists.');
    // }

    return await this.sevicesRepository.save(data);
  }
}