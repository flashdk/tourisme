import { Schema } from "mongoose";

export interface ICreateServiceRequestDTO {
  title: string;
  pictures?: object;
  description?: string;
  basicPrice: number;
  createdAt: Date;
  updatedAt?: Date;
  createdBy: Schema.Types.ObjectId
} 