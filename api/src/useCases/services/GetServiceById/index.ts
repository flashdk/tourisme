
import { ServiceRepository } from "../../../repositories/implementations/mongodb/ServiceRepository";
import { GetServiceByIdUseCase } from "./GetServiceByIdUseCase";
import { GetServiceByIdController } from "./GetServiceByIdController";

const serviceRepository = new ServiceRepository();

const getServiceByIdUseCase = new GetServiceByIdUseCase(serviceRepository);

const getServiceByIdController = new GetServiceByIdController(getServiceByIdUseCase);

export { getServiceByIdUseCase, getServiceByIdController };