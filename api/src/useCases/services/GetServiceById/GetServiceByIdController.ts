import { Request, Response } from "express";
import { GetServiceByIdUseCase } from "./GetServiceByIdUseCase";
import Joi from "joi"; 

export class GetServiceByIdController {
  constructor(private getServiceByIdUseCase: GetServiceByIdUseCase) {}

  async handle(request: Request, response: Response): Promise<Response> {
    const validation = Joi.object({
      id: Joi.string(),
    }).validate({
      ...request.params,
    });

    if (validation.error) {
      return response.status(400).json({
        success: false,
        message: `${validation.error.message}`,
      });
    }

    try {
      const role = await this.getServiceByIdUseCase.execute(validation.value.id);

      return response.status(200).send({ sucess: true, data: role });
    } catch (error: any) {
      return response.status(400).json(error || "Unexpected error.");
    }
  }
}