import { IServiceRepository } from "../../../repositories/IServicesRepository";
import { IGetServiceByIdRequestDTO } from "./GetServiceByIdDTO";

export class GetServiceByIdUseCase {
  constructor(
    private serviceRepository: IServiceRepository,
  ) {}
 
  async execute(id: string): Promise<IGetServiceByIdRequestDTO> {
    return this.serviceRepository.getServiceById(id);
  }
}