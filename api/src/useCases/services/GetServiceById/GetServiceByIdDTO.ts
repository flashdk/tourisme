import { Schema, Types } from "mongoose";

export interface IGetServiceByIdRequestDTO {
  _id: Types.ObjectId;
  title: string;
  // startAt: Date;
  pictures?: object;
  description?: string;
  basicPrice: string;
  createdAt: Date;
  updatedAt?: Date;
  createdBy: Schema.Types.ObjectId
  updatedBy: Schema.Types.ObjectId
} 