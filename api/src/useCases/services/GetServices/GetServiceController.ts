import { Request, Response } from "express";
import { GetServiceUseCase } from "./GetServiceUseCase";
import { IServiceRequestDTO } from "./GetServiceDTO";
import { helpers } from "../../../helpers/helpers"
import Joi from "joi";
export class GetServiceController {
  constructor(private getServiceUseCase: GetServiceUseCase) { }

  async handle(request: Request, response: Response): Promise<Response> {
    console.log('le type de ', request.query.createdBy);
    
    const validation = Joi.object({
      createdBy: Joi.string().required(),
    }).validate({
      ...request.query
    })

    if (validation.error) {
      return response.status(400).json({
        success: false,
        message: `${validation.error.message}`
      })
    }

    try {
      const pagination = helpers.dataPagination(+request.query.page, +request.query.perPage)
      const createdBy = request.query.createdBy
      const services: IServiceRequestDTO[] = await this.getServiceUseCase.execute(pagination, createdBy as string);

      return response.status(200).send({ sucess: true, page: request.query.page || 0, perPage: request.query.perPage || 0, data: services });
    } catch (error: any) {
      return response.status(400).json(error.message || "Unexpected error.");
    }
  }
}