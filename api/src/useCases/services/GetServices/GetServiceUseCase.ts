import { IServiceRepository } from "../../../repositories/IServicesRepository";
import { IServiceRequestDTO } from "./GetServiceDTO";

export class GetServiceUseCase {
  constructor(private serviceRepository: IServiceRepository) {}

  async execute(pagination, createdBy: string): Promise<IServiceRequestDTO[]> {
    const services: IServiceRequestDTO[] = await this.serviceRepository.getServices(pagination, createdBy);
    return services;
  }
}