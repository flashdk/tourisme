
import { ServiceRepository } from "../../../repositories/implementations/mongodb/ServiceRepository";
import { GetServiceUseCase } from "./GetServiceUseCase";
import { GetServiceController } from "./GetServiceController";

const serviceRepository = new ServiceRepository();

const getServiceUseCase = new GetServiceUseCase(serviceRepository);

const getServiceController = new GetServiceController(getServiceUseCase);

export { getServiceUseCase, getServiceController };