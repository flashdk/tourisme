import { Schema, Types } from "mongoose";

export interface IServiceRequestDTO {
  _id?: Types.ObjectId;
  title: string;
  pictures?: object;
  description?: string;
  basicPrice: string;
  createdAt: Date;
  updatedAt?: Date;
  createdBy: Schema.Types.ObjectId
  updatedBy: Schema.Types.ObjectId
} 