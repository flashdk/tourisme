import { Schema } from "mongoose";

export interface IDeleteServiceRequestDTO {
  _id:Schema.Types.ObjectId;
  createdBy:Schema.Types.ObjectId;
} 