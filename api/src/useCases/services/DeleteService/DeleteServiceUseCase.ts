import { IServiceRepository } from "../../../repositories/IServicesRepository";

export class DeleteServiceeUseCase {
  constructor(private serviceRepository: IServiceRepository) {}

  async execute(item): Promise<any> {
    await this.serviceRepository.delete(item);
  }
}