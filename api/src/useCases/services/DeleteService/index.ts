
import { ServiceRepository } from "../../../repositories/implementations/mongodb/ServiceRepository";
import { DeleteServiceeUseCase } from "./DeleteServiceUseCase";
import { DeleteServiceController } from "./DeleteServiceController";

const serviceRepository = new ServiceRepository();

const deleteServiceUseCase = new DeleteServiceeUseCase(serviceRepository);

const deleteServiceController = new DeleteServiceController(deleteServiceUseCase);

export { deleteServiceUseCase, deleteServiceController };