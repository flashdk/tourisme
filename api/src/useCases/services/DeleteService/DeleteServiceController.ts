import { Request, Response } from "express";
import { DeleteServiceeUseCase } from "./DeleteServiceUseCase";
import Joi from "joi"; 

export class DeleteServiceController {
  constructor(private deleteService: DeleteServiceeUseCase) {}

  async handle(request: Request, response: Response): Promise<any> {
    const validation = Joi.object({
      id: Joi.string().required(),
      createdBy: Joi.string().required(),
    }).validate({
      ...request.params,
      ...request.query
    });

    if (validation.error) {
      try {
        return response.status(400).json(JSON.parse(validation.error.message));
      } catch (err) {
        return response.status(400).json({
          success: false,
          message: `${validation.error.message}`,
        });
      }
    }

    try {
      await this.deleteService.execute(validation.value);
      return response.status(200).send({ sucess: true });
    } catch (error: any) {
      if(error.error === 'failed_verify') {
        return response.status(400).json(error)
      }
      return response.status(400).json(error.message || "Unexpected error.");
    }
  }
}