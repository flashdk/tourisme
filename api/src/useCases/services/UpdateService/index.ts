
import { ServiceRepository } from "../../../repositories/implementations/mongodb/ServiceRepository";
import { UpdateServiceUseCase } from "./UpdateServiceUseCase";
import { UpdateServiceController } from "./UpdateServiceController";

const serviceRepository = new ServiceRepository();

const updateServiceUseCase = new UpdateServiceUseCase(serviceRepository);

const updateServiceController = new UpdateServiceController(updateServiceUseCase);

export { updateServiceUseCase, updateServiceController };