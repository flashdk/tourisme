import { Request, Response } from "express";
import { UpdateServiceUseCase } from "./UpdateServiceUseCase";
import Joi from "joi"; 

export class UpdateServiceController {
  
  constructor(private updateServiceUseCase: UpdateServiceUseCase) {}

  async handle(request: Request, response: Response): Promise<Response> {
    
    const validation = Joi.object({
      _id: Joi.string(),
      title: Joi.string(),
      permissions: Joi.array().required(),
      isDefault: Joi.boolean().required(),
      createdAt: Joi.date(),
      createdBy: Joi.string(),
      updatedAt: Joi.date(),
      updatedBy: Joi.string(),
    }).validate({
      ...request.body,
    });

    if (validation.error) {
      try {
        return response.status(400).json(JSON.parse(validation.error.message));
      } catch (err) {
        return response.status(400).json({
          success: false,
          message: `${validation.error.message}`,
        });
      }
    }

    try {
      const roleUpdate = await this.updateServiceUseCase.execute(validation.value);

      return response.status(200).send({ sucess: true, data: roleUpdate });
    } catch (error: any) {
      return response.status(400).json(error.message || "Unexpected error.");
    }
  }
}