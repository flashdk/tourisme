import { Service } from "../../../entities/Service";
import { IServiceRepository } from "../../../repositories/IServicesRepository";
import { IUpdateServiceRequestDTO } from "./UpdateServiceDTO";

export class UpdateServiceUseCase {
  constructor(private serviceRepository: IServiceRepository) {}

  async execute(service: IUpdateServiceRequestDTO): Promise<IUpdateServiceRequestDTO> {
    const update = await this.serviceRepository.updateService(service);
    return update as IUpdateServiceRequestDTO;
  }
}