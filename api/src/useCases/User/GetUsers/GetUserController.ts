import { Request, Response } from "express";
import { GetUserUseCase } from "./GetUserUseCase";
import { User } from "../../../entities/User";

export class GetUserController {
  constructor(
    private getUserUseCase: GetUserUseCase,
  ) { }

  async handle(request: Request, response: Response): Promise<Response> {

    try {
      const users: unknown = await this.getUserUseCase.execute()

      return response.status(200).send({sucess: true, data:users});
    } catch (error: any) {
      return response.status(400).json(error.message || 'Unexpected error.')
    }
  }
}