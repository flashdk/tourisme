import { IUsersRepository } from "../../../repositories/IUsersRepository";
import { IGetUserRequestDTO } from "./GetUserDTO";
import { User } from "../../../entities/User";

export class GetUserUseCase {
  constructor(
    private usersRepository: IUsersRepository,
  ) {}
 
  async execute(): Promise<unknown> {
    const users: unknown = await this.usersRepository.getUsers();
    return users
  }
}