import { Schema } from "mongoose";

export interface ICreateUserRequestDTO {
  firstName: string;
  lastName: string;
  email: string;
  telephone?: string;
  country?: string;
  role?: Schema.Types.ObjectId;
  password: string;
  parcours?: Schema.Types.ObjectId;
  defaultLang: string;
  avatar?: string;
  createdAt: Date
  updatedAt?: Date
} 