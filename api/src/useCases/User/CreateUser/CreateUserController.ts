import { Request, Response } from "express";
import { CreateUserUseCase } from "./CreateUserUseCase";
import Joi, { string } from "joi";
// import * as bcrypt from "bcrypt";
const bcrypt = require('bcrypt')

export class CreateUserController {
  constructor(
    private createUserUseCase: CreateUserUseCase,
  ) { }

  async handle(request: Request, response: Response): Promise<Response> {
    const validation = Joi.object({
      role: Joi.string(),
      firstName: Joi.string(),
      lastName: Joi.string().required(),
      email: Joi.string().email().required(),
      telephone: Joi.string().optional(),
      country: Joi.string().optional(),
      password: Joi.string(),
      defaultLang: Joi.string().optional(),
      avatar: Joi.string().optional(),
      createdAt: Joi.date()
    }).validate({
      ...request.body
    })

    if (validation.error) {
      return response.status(400).json({
        success: false,
        message: `${validation.error.message}`
      })
    }
    
    const uploadData = `${request.protocol}://${request.get('host')}/images/${request.file?.filename}`

    try {
      const hashKey = await bcrypt.hash(validation.value.password, 10)
      const addUser = { ...validation.value, password: hashKey,avatar: uploadData}
      
      const user = await this.createUserUseCase.execute(addUser)
      return response.status(201).json({success: true, data: user})
    } catch (error: any) {
      if(error.error === 'user_exist') {
        return response.status(400).json(error)
      }else {
        return response.status(400).json({code:'Unexpected error.'})
      }
    }
  }
}