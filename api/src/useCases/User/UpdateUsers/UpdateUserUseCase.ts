import { User } from "../../../entities/User";
import { IUsersRepository } from "../../../repositories/IUsersRepository";
import { IUpdateUserRequestDTO } from "./UpdateUserDTO";

export class UpdateUserUseCase {
  constructor(
    private usersRepository: IUsersRepository,
  ) {}
 
  async execute(user: IUpdateUserRequestDTO): Promise<User> {
    const update = await this.usersRepository.updateUsers(user);
    return update
  }
}