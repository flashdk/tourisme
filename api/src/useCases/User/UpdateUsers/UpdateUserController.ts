import { Request, Response } from "express";
import { UpdateUserUseCase } from "./UpdateUserUseCase";
import Joi from "joi"; 

export class UpdateUserController {
  constructor(
    private updateUserUseCase: UpdateUserUseCase,
  ) { }

  async handle(request: Request, response: Response): Promise<Response> {
    const validation = Joi.object({
      id: Joi.string().required(),
      firstName: Joi.string(),
      lastName: Joi.string().required(),
      email: Joi.string().email().required(),
      role: Joi.string(),
      telephone: Joi.string(),
      country: Joi.string(),
      password: Joi.string(),
      defaultLang: Joi.string(),
      avatar: Joi.string().optional(),
      updatedAt: Joi.date(),
    }).validate({
      ...request.body,
      ...request.params
    })

    if(validation.error) {
      try {
        return response.status(400).json(JSON.parse(validation.error.message))
      } catch(err) {
        return response.status(400).json({
          success: false,
          message: `${validation.error.message}`        
        })
      }
    }

    const uploadData = `${request.protocol}://${request.get('host')}/images/${request.file?.filename}`
    
    try {
      const userUpdate = await this.updateUserUseCase.execute({...validation.value, avatar: uploadData})

      return response.status(200).send({sucess: true, data:userUpdate});
    } catch (error) {
      return response.status(400).json(error.message || 'Unexpected error.')
    }
  }
}