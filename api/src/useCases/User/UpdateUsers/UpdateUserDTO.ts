import { Schema } from "mongoose";

export interface IUpdateUserRequestDTO {
  id:Schema.Types.ObjectId;
  firstName: string;
  lastName: string;
  email: string;
  telephone: string;
  country: string;
  role?: Schema.Types.ObjectId;
  password: string;
  parcours?: Schema.Types.ObjectId;
  defaultLang: string;
  avatar?: string  | undefined;
  updatedAt?: Date
} 