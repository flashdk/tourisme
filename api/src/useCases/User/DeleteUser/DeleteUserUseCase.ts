import { ObjectID } from "bson";
import { User } from "../../../entities/User";
import { IUsersRepository } from "../../../repositories/IUsersRepository";
import { IDeleteUserRequestDTO } from "./DeleteUserDTO";

export class DeleteUserUseCase {
  constructor(
    private usersRepository: IUsersRepository,
  ) {}
 
  async execute(item: IDeleteUserRequestDTO): Promise<void> {
    console.log(item);
    
    await this.usersRepository.delete(item.id.toString());
  }
}