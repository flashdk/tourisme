import { Request, Response } from "express";
import { DeleteUserUseCase } from "./DeleteUserUseCase";
import Joi from "joi"; 

export class DeleteUserController {
  constructor(
    private deleteUserUseCase: DeleteUserUseCase,
  ) { }

  async handle(request: Request, response: Response): Promise<Response> {
    const validation = Joi.object({
      id: Joi.string(),
    }).validate({
      ...request.params
    })

    console.log('request.params', request.params);
    

    if(validation.error) {
      try {
        console.log('bommbomm', validation.error);
        
        return response.status(400).json(JSON.parse(validation.error.message))
      } catch(err) {
        return response.status(400).json({
          success: false,
          message: `${validation.error.message}`        
        })
      }
    }

    try {
      console.log('validation value',validation.value);
      
      await this.deleteUserUseCase.execute(validation.value)
      return response.status(200).send({sucess: true});
    } catch (error: any) {
      return response.status(400).json(error.message || 'Unexpected error.')
    }
  }
}