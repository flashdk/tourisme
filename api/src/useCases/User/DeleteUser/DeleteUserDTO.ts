import { Schema } from "mongoose";

export interface IDeleteUserRequestDTO {
  id:Schema.Types.ObjectId;
} 