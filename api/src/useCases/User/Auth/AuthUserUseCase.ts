import { IUsersRepository } from "../../../repositories/IUsersRepository";
import { IAuthUserRequestDTO, IAuthUserResponsetDTO } from "./AuthUserDTO";

export class AuthUserUseCase {
  constructor(
    private usersRepository: IUsersRepository,
  ) { }

  async execute(data: IAuthUserRequestDTO): Promise<IAuthUserResponsetDTO> {
    return await this.usersRepository.auth(data);
  }
}