
import { PostgresUsersRepository } from "../../../repositories/implementations/mongodb/UsersRepository";
import { AuthUserUseCase } from "./AuthUserUseCase";
import { AuthUserController } from "./AuthUserController";

const postgresUsersRepository = new PostgresUsersRepository()

const authUserUseCase = new AuthUserUseCase(
  postgresUsersRepository,
)

const authUserController = new AuthUserController(
  authUserUseCase
)

export { authUserUseCase, authUserController }