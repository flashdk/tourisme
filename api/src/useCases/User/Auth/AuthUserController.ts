import { Request, Response } from "express";
import { AuthUserUseCase } from "./AuthUserUseCase";
import Joi from "joi"; 
import { IAuthUserResponsetDTO } from "./AuthUserDTO";

export class AuthUserController {
  constructor(
    private authUserUseCase: AuthUserUseCase,
  ) { }

  async handle(request: Request, response: Response): Promise<Response> {
    const validation = Joi.object({
      email: Joi.string().email().optional(),
      telephone: Joi.string().optional,
      password: Joi.string().required(),
    }).validate({
      ...request.body
    })

    if(validation.error) {
      try {
        return response.status(400).json(JSON.parse(validation.error.message))
      } catch(err) {
        return response.status(400).json({
          success: false,
          message: `${validation.error.message}`        
        })
      }
    }

    try {
      const user: IAuthUserResponsetDTO = await this.authUserUseCase.execute(validation.value)
      return response.status(201).send(user);
    } catch (error: any) {
      return response.status(400).json(JSON.parse(error.message ) || 'Unexpected error.')
    }
  }
}