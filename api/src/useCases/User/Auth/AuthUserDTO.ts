
export interface IAuthUserRequestDTO {
  email?: string;
  telephone?: string;
  password: string;
} 


export interface IAuthUserResponsetDTO {
  // userId: string;
  token: string;
} 