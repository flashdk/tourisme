import { Schema } from "mongoose";

export interface IGetUserByIdRequestDTO {
  _id:Schema.Types.ObjectId;
  firstName: string;
  lastName: string;
  email: string;
  telephone: string;
  country: string;
  role?: Schema.Types.ObjectId;
  password: string;
  parcours?: Schema.Types.ObjectId;
  defaultLang: string;
  avatar?: string  | undefined;
  createdAt?: Date
  updatedAt?: Date
} 