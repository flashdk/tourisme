import { IUsersRepository } from "../../../repositories/IUsersRepository";
import { IGetUserByIdRequestDTO } from "./GetUserByIdDTO";

export class GetUserByIdUseCase {
  constructor(
    private usersRepository: IUsersRepository,
  ) {}
 
  async execute(id: string): Promise<IGetUserByIdRequestDTO> {
    return this.usersRepository.getUserById(id);
  }
}