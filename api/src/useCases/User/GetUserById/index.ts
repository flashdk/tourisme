
import { PostgresUsersRepository } from "../../../repositories/implementations/mongodb/UsersRepository";
import { GetUserByIdUseCase } from "./GetUserByIdUseCase";
import { GetUserByIdController } from "./GetUserByIdController";

const postgresUsersRepository = new PostgresUsersRepository()

const getUserByIdUseCase = new GetUserByIdUseCase(
  postgresUsersRepository,
)

const getUserByIdController = new GetUserByIdController(
  getUserByIdUseCase
)

export { getUserByIdUseCase, getUserByIdController }