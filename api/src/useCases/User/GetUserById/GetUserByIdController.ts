import { Request, Response } from "express";
import { GetUserByIdUseCase } from "./GetUserByIdUseCase";
import Joi from "joi"; 

export class GetUserByIdController {
  constructor(
    private getUserByIdUseCase: GetUserByIdUseCase,
  ) { }

  async handle(request: Request, response: Response): Promise<Response> {

    const validation = Joi.object({
      id: Joi.string(),
    }).validate({
      ...request.params
    })

    if (validation.error) {
      return response.status(400).json({
        success: false,
        message: `${validation.error.message}`
      })
    }

    try {
      const user = await this.getUserByIdUseCase.execute(validation.value.id)

      return response.status(200).send({sucess: true, data:user});
    } catch (error: any) {
      return response.status(400).json(error || 'Unexpected error.')
    }
  }
}