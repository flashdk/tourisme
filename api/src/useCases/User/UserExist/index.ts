
import { PostgresUsersRepository } from "../../../repositories/implementations/mongodb/UsersRepository";
import { UserUseCase } from "./UserUseCase";
import { FindUserController } from "./FindUserController";

const postgresUsersRepository = new PostgresUsersRepository()

const getUserUseCase = new UserUseCase(
  postgresUsersRepository,
)

const findUserController = new FindUserController(
  getUserUseCase
)

export { getUserUseCase, findUserController }