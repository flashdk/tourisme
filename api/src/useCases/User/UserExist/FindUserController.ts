import { Request, Response } from "express";
import { UserUseCase } from "./UserUseCase";
import Joi from "joi"; 

export class FindUserController {
  constructor(
    private userUseCase: UserUseCase,
  ) { }

  async handle(request: Request, response: Response): Promise<Response> {

    const validation = Joi.object({
      email: Joi.string().optional(),
      telephone: Joi.string().optional(),
    }).validate({
      ...request.params
    })

    if (validation.error) {
      return response.status(400).json({
        success: false,
        message: `${validation.error.message}`
      })
    }

    try {
      const user = await this.userUseCase.execute(JSON.stringify(validation.value))

      return response.status(200).send({sucess: true, data:user});
    } catch (error: any) {
      return response.status(400).json(error || 'Unexpected error.')
    }
  }
}