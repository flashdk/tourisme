import { IUsersRepository } from "../../../repositories/IUsersRepository";
import { IUserRequestDTO } from "./UserDTO";

export class UserUseCase {
  constructor(
    private usersRepository: IUsersRepository,
  ) {}
 
  async execute(value: string): Promise<IUserRequestDTO> {
    return this.usersRepository.getUser(value);
  }
}