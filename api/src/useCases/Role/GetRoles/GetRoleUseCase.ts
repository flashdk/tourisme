import { IRolesRepository } from "../../../repositories/IRolesRepository";
import { IGetRoleRequestDTO } from "./GetRoleDTO";

export class GetRoleUseCase {
  constructor(private rolesRepository: IRolesRepository) {}

  async execute(pagination): Promise<IGetRoleRequestDTO[]> {
    const users: IGetRoleRequestDTO[] = await this.rolesRepository.getRoles(pagination);
    return users;
  }
}