import { Schema } from "mongoose";

export interface IGetRoleRequestDTO {
  _id: Schema.Types.ObjectId;
  title: string;
  permissions: Array<string>;
  isDefault: boolean;
  createdAt: Date;
  createdBy: Schema.Types.ObjectId;
  updatedAt?: Date;
  updatedBy?: Schema.Types.ObjectId;
} 