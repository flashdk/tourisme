
import { RoleRepository } from "../../../repositories/implementations/mongodb/RolesRepository";
import { GetRoleUseCase } from "./GetRoleUseCase";
import { GetRoleController } from "./GetRoleController";

const roleRepository = new RoleRepository();

const getRoleUseCase = new GetRoleUseCase(roleRepository);

const getRoleController = new GetRoleController(getRoleUseCase);

export { getRoleUseCase, getRoleController };