import { Request, Response } from "express";
import { GetRoleUseCase } from "./GetRoleUseCase";
import { IGetRoleRequestDTO } from "./GetRoleDTO";
import { helpers } from "./../../../helpers/helpers"
export class GetRoleController {
  constructor(private getRoleUseCase: GetRoleUseCase) { }

  async handle(request: Request, response: Response): Promise<Response> {
    try {
      const pagination = helpers.dataPagination(+request.query.page, +request.query.perPage)
      const roles: IGetRoleRequestDTO[] = await this.getRoleUseCase.execute(pagination);

      return response.status(200).send({ sucess: true, page: request.query.page || 0, perPage: request.query.perPage || 0, data: roles });
    } catch (error: any) {
      return response.status(400).json(error.message || "Unexpected error.");
    }
  }
}