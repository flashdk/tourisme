
import { RoleRepository } from "../../../repositories/implementations/mongodb/RolesRepository";
import { UpdateRoleUseCase } from "./UpdateRoleUseCase";
import { UpdateRoleController } from "./UpdateRoleController";

const roleRepository = new RoleRepository();

const updateRoleUseCase = new UpdateRoleUseCase(roleRepository);

const updateRoleController = new UpdateRoleController(updateRoleUseCase);

export { updateRoleUseCase, updateRoleController };