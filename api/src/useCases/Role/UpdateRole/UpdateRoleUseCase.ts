import { Role } from "../../../entities/Role";
import { IRolesRepository } from "../../../repositories/IRolesRepository";
import { IUpdateRoleRequestDTO } from "./UpdateRoleDTO";

export class UpdateRoleUseCase {
  constructor(private rolesRepository: IRolesRepository) {}

  async execute(role: IUpdateRoleRequestDTO): Promise<IUpdateRoleRequestDTO> {
    const update = await this.rolesRepository.updateRole(role);
    return update as IUpdateRoleRequestDTO;
  }
}