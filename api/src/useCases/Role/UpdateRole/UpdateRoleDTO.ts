import { Schema } from "mongoose";

export interface IUpdateRoleRequestDTO {
  _id: Schema.Types.ObjectId;
  title: string;
  permissions: Array<any>;
  isDefault: boolean;
  createdAt?: Date;
  updatedAt?: Date;
  createdBy?: Schema.Types.ObjectId;
  updatedBy?: Schema.Types.ObjectId;
} 