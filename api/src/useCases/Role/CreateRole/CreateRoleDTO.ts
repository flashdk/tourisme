import { Schema } from "mongoose";

export interface ICreateRoleRequestDTO {
  title: string;
  permissions: Array<string>;
  isDefault: boolean;
  createdAt: Date;
  createdBy: Schema.Types.ObjectId
} 