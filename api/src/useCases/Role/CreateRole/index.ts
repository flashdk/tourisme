
import { RoleRepository } from "../../../repositories/implementations/mongodb/RolesRepository";
import { CreateRoleUseCase } from "./CreateRoleUseCase";
import { CreateRoleController } from "./CreateRoleController";

const roleRepository = new RoleRepository()

const createRoleUseCase = new CreateRoleUseCase(
  roleRepository,
)

const createRoleController = new CreateRoleController(
  createRoleUseCase
)

export { createRoleUseCase, createRoleController }