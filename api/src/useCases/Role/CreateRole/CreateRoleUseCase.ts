import { IRolesRepository } from "../../../repositories/IRolesRepository";
import { ICreateRoleRequestDTO } from "./CreateRoleDTO";
import { Role } from "../../../entities/Role";

export class CreateRoleUseCase {
  constructor(
    private rolesRepository: IRolesRepository,
  ) {}
 
  async execute(data: ICreateRoleRequestDTO): Promise<Role> {
    
    // const userAlreadyExists = await this.usersRepository.findByEmail(data.email);

    // if (userAlreadyExists) {
    //   throw new Error('User already exists.');
    // }

    return await this.rolesRepository.save(data);
  }
}