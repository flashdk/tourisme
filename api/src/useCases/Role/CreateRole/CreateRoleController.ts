import { Request, Response } from "express";
import { CreateRoleUseCase } from "./CreateRoleUseCase";
import Joi from "joi";

export class CreateRoleController {
  constructor(
    private createRoleUseCase: CreateRoleUseCase,
  ) { }

  async handle(request: Request, response: Response): Promise<Response> {
    const validation = Joi.object({
      title: Joi.string(),
      permissions: Joi.array().required(),
      isDefault: Joi.boolean().required(),
      createdAt: Joi.date(),
      //createdBy: Joi.string().required()
    }).validate({
      ...request.body
    })

    if (validation.error) {
      return response.status(400).json({
        success: false,
        message: `${validation.error.message}`
      })
    }

    try {
      const role = await this.createRoleUseCase.execute(validation.value)
      return response.status(201).json({success: true, data: role})
    } catch (error: any) {
      if(error.error === 'role_exist') {
        return response.status(400).json(error)
      }else {
        return response.status(400).json({code:'Unexpected error.'})
      }
    }
  }
}