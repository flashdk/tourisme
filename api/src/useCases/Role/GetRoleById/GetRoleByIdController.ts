import { Request, Response } from "express";
import { GetRoleByIdUseCase } from "./GetRoleByIdUseCase";
import Joi from "joi"; 

export class GetRoleByIdController {
  constructor(private getRoleByIdUseCase: GetRoleByIdUseCase) {}

  async handle(request: Request, response: Response): Promise<Response> {
    const validation = Joi.object({
      id: Joi.string(),
    }).validate({
      ...request.params,
    });

    if (validation.error) {
      return response.status(400).json({
        success: false,
        message: `${validation.error.message}`,
      });
    }

    try {
      const role = await this.getRoleByIdUseCase.execute(validation.value.id);

      return response.status(200).send({ sucess: true, data: role });
    } catch (error: any) {
      return response.status(400).json(error || "Unexpected error.");
    }
  }
}