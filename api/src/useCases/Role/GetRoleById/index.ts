
import { RoleRepository } from "../../../repositories/implementations/mongodb/RolesRepository";
import { GetRoleByIdUseCase } from "./GetRoleByIdUseCase";
import { GetRoleByIdController } from "./GetRoleByIdController";

const roleRepository = new RoleRepository();

const getRoleByIdUseCase = new GetRoleByIdUseCase(roleRepository);

const getRoleByIdController = new GetRoleByIdController(getRoleByIdUseCase);

export { getRoleByIdUseCase, getRoleByIdController };