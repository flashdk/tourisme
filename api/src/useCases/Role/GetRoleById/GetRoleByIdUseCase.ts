import { IRolesRepository } from "../../../repositories/IRolesRepository";
import { IGetRoleByIdRequestDTO } from "./GetRoleByIdDTO";

export class GetRoleByIdUseCase {
  constructor(
    private rolesRepository: IRolesRepository,
  ) {}
 
  async execute(id: string): Promise<IGetRoleByIdRequestDTO> {
    return this.rolesRepository.getRoleById(id);
  }
}