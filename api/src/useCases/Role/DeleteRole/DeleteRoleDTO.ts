import { Schema } from "mongoose";

export interface IDeleteRoleRequestDTO {
  _id:Schema.Types.ObjectId;
} 