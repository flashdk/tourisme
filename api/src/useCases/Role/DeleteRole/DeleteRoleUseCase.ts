import { IRolesRepository } from "../../../repositories/IRolesRepository";

export class DeleteRoleUseCase {
  constructor(private rolesRepository: IRolesRepository) {}

  async execute(id: string): Promise<void> {
    await this.rolesRepository.delete(id);
  }
}