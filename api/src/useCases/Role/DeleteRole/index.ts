
import { RoleRepository } from "../../../repositories/implementations/mongodb/RolesRepository";
import { DeleteRoleUseCase } from "./DeleteRoleUseCase";
import { DeleteRoleController } from "./DeleteRoleController";

const roleRepository = new RoleRepository();

const deleteRoleUseCase = new DeleteRoleUseCase(roleRepository);

const deleteRoleController = new DeleteRoleController(deleteRoleUseCase);

export { deleteRoleUseCase, deleteRoleController };