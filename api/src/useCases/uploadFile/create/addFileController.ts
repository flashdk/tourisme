import { Request, Response } from "express";
import Joi from "joi";
const crypto = require('crypto');

export class AddFileController {

  async handle(request: Request, response: Response): Promise<Record<string, any>> {
    // console.log('req.file', request.file);

    const validation = Joi.object({
      file: Joi.string(),
    }).validate({
      ...request.body
    })

    if (validation.error) {
      return response.status(400).json({
        success: false,
        message: `${validation.error.message}`
      })
    }

    try {
      const x = {
        location: `${request.protocol}://${request.get('host')}/images/${request.file?.filename}`,
        name: request.file.filename,
        id: crypto.randomUUID(),
        key: crypto.randomUUID(),
        originalname: request.file.originalname
      }
      // console.log('xxxxxxxxxxxxxxx', x);
      

      return response.status(201).json({ success: true, data: x })
    } catch (error: any) {
      if (error.error === 'file_exist') {
        return response.status(400).json(error)
      } else {
        return response.status(400).json({ code: 'Unexpected error.' })
      }
    }
  }
}