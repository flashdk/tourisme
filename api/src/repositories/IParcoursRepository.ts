import { Parcours } from "../entities/Parcours";
import { ICreateParcourstDTO } from "../useCases/Parcours/CreateParcours/CreateParcoursDTO";
import { IGetParcoursRequestDTO } from "../useCases/Parcours/GetParcours/GetParcoursDTO";
import { IGetParcoursByIdRequestDTO } from "../useCases/Parcours/GetParcoursById/GetParcoursByIdDTO";

import { IUpdateParcoursRequestDTO } from "../useCases/Parcours/UpdateParcours/UpdateParcoursDTO";

export interface IParcoursRepository {
  save(parcours: ICreateParcourstDTO): Promise<Parcours>;
  getParcours(): Promise<IGetParcoursRequestDTO[]>
  getParcoursById(id: string): Promise<IGetParcoursByIdRequestDTO>
  delete(parcours: string) : Promise<void>
  updateParcours(parcours: IUpdateParcoursRequestDTO) : Promise<IUpdateParcoursRequestDTO>
}