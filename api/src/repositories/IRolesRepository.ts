import { Role } from "../entities/Role";

import { ICreateRoleRequestDTO } from "../useCases/Role/CreateRole/CreateRoleDTO";
import { IGetRoleByIdRequestDTO } from "../useCases/Role/GetRoleById/GetRoleByIdDTO";
import { IUpdateRoleRequestDTO } from "../useCases/Role/UpdateRole/UpdateRoleDTO";
import { IGetRoleRequestDTO } from "../useCases/Role/GetRoles/GetRoleDTO";
import { Pagination } from "./../helpers/helpers"

export interface IRolesRepository {
  save(role: ICreateRoleRequestDTO): Promise<Role>;
  getRoles(pagination: Pagination): Promise<IGetRoleRequestDTO[]>;
  getRoleById(id: string): Promise<IGetRoleByIdRequestDTO>;
  updateRole(role: IUpdateRoleRequestDTO): Promise<IUpdateRoleRequestDTO>;
  delete(id: string): Promise<void>;
}