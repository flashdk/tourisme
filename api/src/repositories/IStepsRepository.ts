import { Steps } from "../entities/Steps";

import { ICreateStepsDTO } from "../useCases/Steps/Create/CreateStepsDTO";
import { IGetStepsByIdDTO } from "../useCases/Steps/GetStepsById/GetStepsByIdDTO";
import { IUpdateStepsDTO } from "../useCases/Steps/UpdateSteps/UpdateStepsDTO";
import { IGetStepsDTO } from "../useCases/Steps/GetSteps/GetStepsDTO";

export interface IStepsRepository {
  save(steps: ICreateStepsDTO): Promise<Steps>;
  get(): Promise<IGetStepsDTO[]>;
  getById(id: string): Promise<IGetStepsByIdDTO>;
  update(steps: IUpdateStepsDTO): Promise<Steps>;
  delete(id: string): Promise<void>;
}