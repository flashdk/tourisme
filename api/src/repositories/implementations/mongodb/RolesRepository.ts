import { Role, RoleModel } from "../../../entities/Role";
import { IRolesRepository } from "../../IRolesRepository";
import { ICreateRoleRequestDTO } from "../../../useCases/Role/CreateRole/CreateRoleDTO";
import { IGetRoleRequestDTO } from "../../../useCases/Role/GetRoles/GetRoleDTO";
import { IGetRoleByIdRequestDTO } from "../../../useCases/Role/GetRoleById/GetRoleByIdDTO";
import { ObjectId } from "mongodb";
import { IUpdateRoleRequestDTO } from "../../../useCases/Role/UpdateRole/UpdateRoleDTO";
import { Pagination } from "./../../../helpers/helpers"

// import escapeStringRegexp from 'escape-string-regexp';

export class RoleRepository implements IRolesRepository {
  async getRoleById(id: string): Promise<IGetRoleByIdRequestDTO> {
    try {
      const role = await RoleModel.findOne({ _id: id });

      if (!role) {
        throw { succes: false, message: "cet role n'existe pas" };
      }

      return role as IGetRoleByIdRequestDTO;
    } catch (error) {
      throw { succes: false, message: error };
    }
  }

  async getRoles(pagination: Pagination): Promise<IGetRoleRequestDTO[]> {
    console.log('paggination search', );
    
    const sort = pagination.sort ? pagination.sort : { name: 1 }
    const search = {}
    // const search = !pagination.search
    //   ? {}
    //   : {
    //     $or: [
    //       // { 'title': new RegExp('.*'+pagination.search +'*.', 'i') },
    //     ]
    //   }
    try {
      const roles = await RoleModel.find(search).sort(sort).skip(pagination.page * pagination.perPage).limit(pagination.perPage);
      if (search) {
        console.log('search exist');
      }
      return roles as IGetRoleRequestDTO[];
    } catch (error) {
      throw {
        success: false,
        message: error,
      };
    }
  }

  async delete(id: string): Promise<void> {
    try {
      const exist = await RoleModel.findOne({ _id: new ObjectId(id) })

      if (!exist) {
        throw {
          success: false,
          message: "Suppresion impossible car l'élément n'existe pas",
        };
      }
      await RoleModel.remove({ _id: new ObjectId(id) });
    } catch (error) {
      throw {
        success: false,
        message: error,
      };
    }
  }

  async updateRole(
    role: IUpdateRoleRequestDTO
  ): Promise<IUpdateRoleRequestDTO> {
    try {
      const saveRole = await RoleModel.findByIdAndUpdate(
        { _id: role._id },
        { $set: { ...role } },
        { new: true }
      );

      return saveRole as IUpdateRoleRequestDTO;
    } catch (error) {
      throw { success: false, message: error };
    }
  }

  async save(role: ICreateRoleRequestDTO): Promise<Role> {
    const exist = await RoleModel.findOne().and([{ title: role.title }]);

    if (exist) {
      throw {
        error: "role_exist",
        success: false,
        message: "Ce role exist deja",
      };
    }

    const newRole = new RoleModel(role);

    await newRole.save();
    return newRole;
  }
}
