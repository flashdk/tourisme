import { Steps, StepsModel } from "../../../entities/Steps";
import { IStepsRepository } from "../../IStepsRepository";
import { ICreateStepsDTO } from "../../../useCases/Steps/Create/CreateStepsDTO";
import { IGetStepsDTO } from "../../../useCases/Steps/GetSteps/GetStepsDTO";
import { IGetStepsByIdDTO } from "../../../useCases/Steps/GetStepsById/GetStepsByIdDTO";
import { ObjectId } from "mongodb";
import { IUpdateStepsDTO } from "../../../useCases/Steps/UpdateSteps/UpdateStepsDTO";

export class StepsRepository implements IStepsRepository {
  async getById(id: string): Promise<IGetStepsByIdDTO> {
    try {
      const response = await StepsModel.findOne({ _id: id });

      if (!response) {
        throw { succes: false, message: "cet step n'existe pas" };
      }

      return response as IGetStepsByIdDTO;
    } catch (error) {
      throw { succes: false, message: error };
    }
  }

  async get(): Promise<IGetStepsDTO[]> {
    try {
      const response = await StepsModel.find();
      return response as IGetStepsDTO[];
    } catch (error) {
      throw {
        success: false,
        message: error,
      };
    }
  }

  async delete(id: string): Promise<void> {
    try {
      const exist = await StepsModel.findOne({_id: new ObjectId(id)})
      
      if(!exist) {
        throw {
          success: false,
          message: "Suppresion impossible car l'élément n'existe pas",
        };
      }
      await StepsModel.remove({_id: new ObjectId(id)});
    } catch (error) {
      throw {
        success: false,
        message: error,
      };
    }
  }

  async update(
    steps: IUpdateStepsDTO
  ): Promise<IUpdateStepsDTO> {
    try {
      const retriveData = await StepsModel.findByIdAndUpdate(
        { _id: steps.id },
        { $set: {...steps} },
        { new: true }
      );

      return retriveData as IUpdateStepsDTO;
    } catch (error) {
      throw { success: false, message: error };
    }
  }

  async save(steps: ICreateStepsDTO): Promise<Steps> {

    // steps.forEach(element => {
    //   const exist = await StepsModel.findOne().and([{ title: element.title }]);
    //   if (exist) {
    //     throw {
    //       error: "steps_exist",
    //       success: false,
    //       message: "Cet etatpe exist deja",
    //     };
    //   }
    // });

    try {
      const exist = await StepsModel.findOne().and([{ title: steps.title }]);

      if (exist) {
        throw {
          error: "steps_exist",
          success: false,
          message: "Cet etatpe exist deja",
        };
      }

      const newData = new StepsModel(steps);

      await newData.save();
      return newData;
    } catch(err) {
      throw {
        success: false,
        message: err,
      };
    }
    

  }
}
