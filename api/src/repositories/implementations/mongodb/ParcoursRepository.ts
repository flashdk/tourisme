import { ObjectId } from "mongodb";
import { Parcours, ParcoursModel } from "../../../entities/Parcours";
import { IParcoursRepository } from "../../IParcoursRepository";
import { ICreateParcourstDTO } from "../../../useCases/Parcours/CreateParcours/CreateParcoursDTO";

import { IGetParcoursRequestDTO } from "../../../useCases/Parcours/GetParcours/GetParcoursDTO";
import { IGetParcoursByIdRequestDTO } from "../../../useCases/Parcours/GetParcoursById/GetParcoursByIdDTO";
import { IUpdateParcoursRequestDTO } from "../../../useCases/Parcours/UpdateParcours/UpdateParcoursDTO";

export class ParcoursRepository implements IParcoursRepository {

  async getParcoursById(id: string): Promise<IGetParcoursByIdRequestDTO> {
    try {
      const parcours = await ParcoursModel.findOne({ _id: id });

      if (!parcours) {
        throw { succes: false, message: "cet role n'existe pas" };
      }

      return parcours as IGetParcoursByIdRequestDTO;
    } catch (error) {
      throw { succes: false, message: error };
    }
  }

  async getParcours(): Promise<IGetParcoursRequestDTO[]> {
    try {
      const parcours = await ParcoursModel.find();
      return parcours as IGetParcoursRequestDTO[];
    } catch (error) {
      throw {
        success: false,
        message: error,
      };
    }
  }

  async delete(id: string): Promise<void> {
    try {
      const exist = await ParcoursModel.findOne({_id: new ObjectId(id)})
      
      if(!exist) {
        throw {
          success: false,
          message: "Suppresion impossible car l'élément n'existe pas",
        };
      }
      await ParcoursModel.remove({_id: new ObjectId(id)});
    } catch (error) {
      throw {
        success: false,
        message: error,
      };
    }
  }

  async updateParcours(
    paracours: IUpdateParcoursRequestDTO
  ): Promise<IUpdateParcoursRequestDTO> {
    try {
      const saveParcours = await ParcoursModel.findByIdAndUpdate(
        { _id: paracours.id },
        { $set: {...paracours} },
        { new: true }
      );

      return saveParcours as IUpdateParcoursRequestDTO;
    } catch (error) {
      throw { success: false, message: error };
    }
  }

  async save(parcours: ICreateParcourstDTO): Promise<Parcours> {
    const exist = await ParcoursModel.findOne().and([{ title: parcours.title }]);

    if (exist) {
      throw {
        error: "parcours_exist",
        success: false,
        message: "Ce parcours exist deja",
      };
    }

    const newParcours = new ParcoursModel(parcours);

    await newParcours.save();
    return newParcours;
  }
}
