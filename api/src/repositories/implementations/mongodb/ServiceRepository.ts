import { Service, ServiceModel } from "../../../entities/Service";
import { Steps, StepsModel } from "../../../entities/Steps";
import { ICreateServiceRequestDTO } from "../../../useCases/services/CreateService/CreateServiceDTO";
import { IServiceRequestDTO } from "../../../useCases/services/GetServices/GetServiceDTO";
import { IGetServiceByIdRequestDTO } from "../../../useCases/services/GetServiceById/GetServiceByIdDTO";
import { ObjectId } from "mongodb";
import { IUpdateServiceRequestDTO } from "../../../useCases/services/UpdateService/UpdateServiceDTO";
import { Pagination } from "./../../../helpers/helpers"
import { IServiceRepository } from "../../IServicesRepository";

// import escapeStringRegexp from 'escape-string-regexp';

export class ServiceRepository implements IServiceRepository {
  async getServices(pagination: Pagination, createdBy: string): Promise<IServiceRequestDTO[]> {    
    const sort = pagination.sort ? pagination.sort : { name: 1 }
    const search = {}
    // const search = !pagination.search
    //   ? {}
    //   : {
    //     $or: [
    //       // { 'title': new RegExp('.*'+pagination.search +'*.', 'i') },
    //     ]
    //   }
    try {
      const services = await ServiceModel.find({createdBy, search}).sort(sort).skip(pagination.page * pagination.perPage).limit(pagination.perPage);
      if (search) {
        console.log('search exist');
      }
      return services as IServiceRequestDTO[];
    } catch (error) {
      throw {
        success: false,
        message: error,
      };
    }
  }

  async getServiceById(id: string): Promise<IGetServiceByIdRequestDTO> {
    try {
      const service = await ServiceModel.findOne({ _id: id });
  
      if (!service) {
        throw { succes: false, message: "cet role n'existe pas" };
      }
  
      return service as IGetServiceByIdRequestDTO;
    } catch (error) {
      throw { succes: false, message: error };
    }
  }
  
  async updateService(service: IUpdateServiceRequestDTO): Promise<IUpdateServiceRequestDTO> {
    try {
      const saveService = await ServiceModel.findByIdAndUpdate(
        { _id: service._id },
        { $set: { ...service } },
        { new: true }
      );

      return saveService as IUpdateServiceRequestDTO;
    } catch (error) {
      throw { success: false, message: error };
    }
  }

  async delete(item: any): Promise<any> {
    try {
      // verfy if service is used on parcours
      const isUsedOnStep = await StepsModel.find({createdBy: item.createdBy, services: item.id})
      // console.log('isUsedOnStep', isUsedOnStep);

      if(isUsedOnStep.length > 0) {
        throw {
          success: false,
          error: 'failed_verify',
          message: "text.serviceIsUsedOnStep",
        }
      }

      const exist = await ServiceModel.findOne({ _id: new ObjectId(item.id) })

      if (!exist) {
        throw {
          success: false,
          message: "Suppresion impossible car l'élément n'existe pas",
        };
      }
      await ServiceModel.remove({ _id: new ObjectId(item.id) });
    } catch (error) {
      throw {
        success: false,
        message: error,
      };
    }
  }

  async save(service: ICreateServiceRequestDTO): Promise<Service> {
    // const exist = await ServiceModel.findOne().and([{ title: service.title }]);
    const exist = await ServiceModel.findOne({ title: service.title });

    if (exist) {
      throw {
        error: "service_exist",
        success: false,
        message: "Ce service exist deja",
      };
    }

    const newService = new ServiceModel(service);

    await newService.save();
    return newService;
  }
}
