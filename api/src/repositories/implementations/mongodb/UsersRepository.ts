import { IUsersRepository } from "../../IUsersRepository";
import { User, UserModel } from "../../../entities/User";
import { ICreateUserRequestDTO } from "../../../useCases/User/CreateUser/CreateUserDTO";
import { IGetUserRequestDTO } from "../../../useCases/User/GetUsers/GetUserDTO";
import { IUpdateUserRequestDTO } from "../../../useCases/User/UpdateUsers/UpdateUserDTO";
import { IDeleteUserRequestDTO } from "../../../useCases/User/DeleteUser/DeleteUserDTO";
import {
  IAuthUserRequestDTO,
  IAuthUserResponsetDTO,
} from "../../../useCases/User/Auth/AuthUserDTO";
import { IGetUserByIdRequestDTO } from "../../../useCases/User/GetUserById/GetUserByIdDTO";
import { IUserRequestDTO } from "../../../useCases/User/UserExist/UserDTO";
import { ObjectId } from "mongodb";
const bcrypt = require("bcrypt");
const jwt = require("jsonwebtoken");

export class PostgresUsersRepository implements IUsersRepository {
  async getUser(value: string): Promise<IUserRequestDTO> {
    try {
      let user;
      const data = JSON.parse(value);

      if (data.email) {
        user = await UserModel.findOne({ email: data.email });
      }

      if (data.telephone) {
        user = await UserModel.findOne({ telephone: data.telephone });
      }

      if (!user) {
        throw { succes: false, message: "cet utilisateur n'existe pas" };
      }

      return user as IUserRequestDTO;
    } catch (error: any) {
      throw { succes: false, message: error };
    }
  }
  async getUserById(id: string): Promise<IGetUserByIdRequestDTO> {
    try {
      const user = await UserModel.findOne({ _id: id });

      if (!user) {
        throw { succes: false, message: "cet utilisateur n'existe pas" };
      }

      return user as IGetUserByIdRequestDTO;
    } catch (error) {
      throw { succes: false, message: error };
    }
  }

  async auth(user: IAuthUserRequestDTO): Promise<IAuthUserResponsetDTO> {
    
    return (
      UserModel.findOne({ email: user.email })
        // .or([{ email: user.email }, { telephone: user.telephone }])
        .then((data: any) => {
          
          if (!data) {
            return Promise.reject(
              new Error(
                JSON.stringify({
                  success: false,
                  message: "Address email non valide",
                })
              )
            );
          }

          return bcrypt
            .compare(user.password, data.password)
            .then((valid: any) => {
              if (!valid) {
                return Promise.reject(
                  new Error(
                    JSON.stringify({
                      success: false,
                      message: "Nom d'utilisateur ou mot de passe incorrect",
                    })
                  )
                );
              }

              return {
                token: jwt.sign(
                  { userId: data._id },
                  "OZVV5TpP4U6wJ&thaCORZ@EQ",
                  { expiresIn: "24h" }
                ),
              };

              // const authUser: IAuthUserResponsetDTO = {
              //   userId: data._id,
              //   token: jwt.sign(
              //     { userId: data._id },
              //     'OZVV5TpP4U6wJ&thaCORZ@EQ',
              //     { expiresIn: '24h' }
              //   )
              // }
              // return authUser
            })
            .catch((error: any) => {
              return Promise.reject(
                new Error(
                  JSON.stringify({
                    success: false,
                    message: "Authentication failed",
                  })
                )
              );
            });
        })
    );
  }

  async delete(id: string): Promise<void> {
    try {
      const exist = await UserModel.findOne({ _id: new ObjectId(id) });

      if (!exist) {
        throw {
          success: false,
          message: "Suppresion impossible car l'élément n'existe pas",
        };
      }
      await UserModel.remove({ _id: new ObjectId(id) });
    } catch (error) {
      throw {
        success: false,
        message: error,
      };
    }
  }

  async updateUsers(user: IUpdateUserRequestDTO): Promise<User> {
    const savedUser = await UserModel.findByIdAndUpdate(
      { _id: user.id },
      user,
      { new: true }
    );

    return savedUser as User;
  }

  async getUsers(): Promise<unknown> {
    try {
      const users = await UserModel.find().populate("role").exec();
      return users;

    } catch (err) {
      throw {
        success: false,
        message: err,
      };
    }
  }

  async findByEmail(email: string): Promise<User> {
    // const user = this.users.find(user => user.email === email);
    // const user = this.usersRepository.findByEmail(email);

    // const user = UserModel.findOne(email)

    // return user;
    throw new Error("Method not implemented.");
  }

  async save(user: ICreateUserRequestDTO): Promise<User> {
    const exist = await UserModel.findOne().or([
      { email: user.email },
      { telephone: user.telephone },
    ]);

    if (exist) {
      throw {
        error: "user_exist",
        success: false,
        message: "Cet utilisateur exist deja",
      };
    }

    const newUser = new UserModel(user);

    await newUser.save();
    return newUser;
  }
}
