import { Service } from "../entities/Service";

import { ICreateServiceRequestDTO } from "../useCases/services/CreateService/CreateServiceDTO";
import { IGetServiceByIdRequestDTO } from "../useCases/services/GetServiceById/GetServiceByIdDTO";
import { IUpdateServiceRequestDTO } from "../useCases/services/UpdateService/UpdateServiceDTO";
import { IServiceRequestDTO } from "../useCases/services/GetServices/GetServiceDTO";
import { Pagination } from "../helpers/helpers"

export interface IServiceRepository {
  save(service: ICreateServiceRequestDTO): Promise<Service>;
  getServices(pagination: Pagination, createdBy: string): Promise<IServiceRequestDTO[]>;
  getServiceById(id: string): Promise<IGetServiceByIdRequestDTO>;
  updateService(role: IUpdateServiceRequestDTO): Promise<IUpdateServiceRequestDTO>;
  delete(item: any): Promise<any>;
}