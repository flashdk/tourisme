import { User } from "../entities/User";
import { ICreateUserRequestDTO } from "../useCases/User/CreateUser/CreateUserDTO";
import { IGetUserRequestDTO } from "../useCases/User/GetUsers/GetUserDTO";
import { IGetUserByIdRequestDTO } from "../useCases/User/GetUserById/GetUserByIdDTO";
import { IUserRequestDTO } from "../useCases/User/UserExist/UserDTO";
import { IUpdateUserRequestDTO } from "../useCases/User/UpdateUsers/UpdateUserDTO";
import { IDeleteUserRequestDTO } from "../useCases/User/DeleteUser/DeleteUserDTO";
import { IAuthUserRequestDTO, IAuthUserResponsetDTO } from "../useCases/User/Auth/AuthUserDTO";

export interface IUsersRepository {
  findByEmail(email: string): Promise<User>;
  save(user: ICreateUserRequestDTO): Promise<User>;
  getUsers(): Promise<unknown>
  getUserById(id: string): Promise<IGetUserByIdRequestDTO>
  getUser(value: string): Promise<IUserRequestDTO>
  updateUsers(user: IUpdateUserRequestDTO) : Promise<User>
  delete(user: string) : Promise<void>
  auth(user : IAuthUserRequestDTO) : Promise<IAuthUserResponsetDTO>
}