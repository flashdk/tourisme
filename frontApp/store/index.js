import Vue from 'vue'
// import { localize } from 'vee-validate'

Vue.config.performance = true

export const state = () => ({
  authenticated: false,
  currentPageTitle: '',
  locale: ''
})

export const getters = {
  getSubscription: state => state.authenticated,
  getCurrentPage: state => state.currentPageTitle,
  getLocale: state => state.locale
}

export const mutations = {
  IS_USER_AUTHENTICARED (state, value) {
    state.authenticated = value
  },
  SET_CURRENT_PAGE (state, value) {
    state.currentPageTitle = value
  },
  SET_LOCALE (state, value) {
    state.locale = value
  }
}

export const actions = {
  IS_USER_AUTHENTICARED ({ commit }, payload) {
    commit('IS_USER_AUTHENTICARED', payload)
  },
  SET_CURRENT_PAGE ({ commit }, payload) {
    commit('SET_CURRENT_PAGE', payload)
  },
  SET_LOCALE ({ commit }, locale) {
    commit('SET_LOCALE', locale)
  }
}
