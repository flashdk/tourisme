exports.ids = [11];
exports.modules = {

/***/ 76:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(83);
if(content.__esModule) content = content.default;
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add CSS to SSR context
var add = __webpack_require__(6).default
module.exports.__inject__ = function (context) {
  add("e0f6cf7e", content, true, context)
};

/***/ }),

/***/ 82:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_style_loader_index_js_ref_7_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_7_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_dist_cjs_js_ref_7_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_7_oneOf_1_3_node_modules_nuxt_components_dist_loader_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_tabCelText_vue_vue_type_style_index_0_id_f0a2cf4e_prod_scoped_true_lang_scss__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(76);
/* harmony import */ var _node_modules_vue_style_loader_index_js_ref_7_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_7_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_dist_cjs_js_ref_7_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_7_oneOf_1_3_node_modules_nuxt_components_dist_loader_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_tabCelText_vue_vue_type_style_index_0_id_f0a2cf4e_prod_scoped_true_lang_scss__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_vue_style_loader_index_js_ref_7_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_7_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_dist_cjs_js_ref_7_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_7_oneOf_1_3_node_modules_nuxt_components_dist_loader_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_tabCelText_vue_vue_type_style_index_0_id_f0a2cf4e_prod_scoped_true_lang_scss__WEBPACK_IMPORTED_MODULE_0__);
/* harmony reexport (unknown) */ for(var __WEBPACK_IMPORT_KEY__ in _node_modules_vue_style_loader_index_js_ref_7_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_7_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_dist_cjs_js_ref_7_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_7_oneOf_1_3_node_modules_nuxt_components_dist_loader_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_tabCelText_vue_vue_type_style_index_0_id_f0a2cf4e_prod_scoped_true_lang_scss__WEBPACK_IMPORTED_MODULE_0__) if(["default"].indexOf(__WEBPACK_IMPORT_KEY__) < 0) (function(key) { __webpack_require__.d(__webpack_exports__, key, function() { return _node_modules_vue_style_loader_index_js_ref_7_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_7_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_dist_cjs_js_ref_7_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_7_oneOf_1_3_node_modules_nuxt_components_dist_loader_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_tabCelText_vue_vue_type_style_index_0_id_f0a2cf4e_prod_scoped_true_lang_scss__WEBPACK_IMPORTED_MODULE_0__[key]; }) }(__WEBPACK_IMPORT_KEY__));


/***/ }),

/***/ 83:
/***/ (function(module, exports, __webpack_require__) {

// Imports
var ___CSS_LOADER_API_IMPORT___ = __webpack_require__(5);
var ___CSS_LOADER_EXPORT___ = ___CSS_LOADER_API_IMPORT___(function(i){return i[1]});
// Module
___CSS_LOADER_EXPORT___.push([module.i, ".table-item[data-v-f0a2cf4e]{align-items:center;display:flex;flex-basis:100%;height:2.9rem;min-width:10.5rem;padding:3px;text-align:left}.table-item[data-v-f0a2cf4e],.table-item.ellipsis[data-v-f0a2cf4e]{overflow:hidden;text-overflow:ellipsis;white-space:nowrap}.responsive.table-item[data-v-f0a2cf4e]{background-color:var(--main-color);border-bottom:1px solid #ccc;font-size:13px;padding-left:200px!important;position:relative}.responsive.table-item[data-v-f0a2cf4e],.responsive.table-item div[data-v-f0a2cf4e]{text-align:center!important}.responsive.table-item[data-v-f0a2cf4e]:nth-child(odd){background-color:#eee}.responsive.table-item[data-v-f0a2cf4e]:before{align-items:center;background-color:#000;bottom:0;color:#fff;content:var(--column-title);display:flex;font-size:13px;font-weight:700;left:0;padding:12px 0 12px 12px;position:absolute;top:0;width:196px}.center[data-v-f0a2cf4e]{text-align:center!important}.left[data-v-f0a2cf4e]{text-align:left!important}.right[data-v-f0a2cf4e]{text-align:right!important}.text-danger[data-v-f0a2cf4e]{color:var(--color-danger-dark)}.text-light-green[data-v-f0a2cf4e]{color:var(--color-success-light)}.text-black[data-v-f0a2cf4e]{color:#000}", ""]);
// Exports
___CSS_LOADER_EXPORT___.locals = {};
module.exports = ___CSS_LOADER_EXPORT___;


/***/ }),

/***/ 85:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
// ESM COMPAT FLAG
__webpack_require__.r(__webpack_exports__);

// CONCATENATED MODULE: ./node_modules/babel-loader/lib??ref--2-0!./node_modules/vue-loader/lib/loaders/templateLoader.js??ref--6!./node_modules/@nuxt/components/dist/loader.js??ref--0-0!./node_modules/vue-loader/lib??vue-loader-options!./components/simpleTable/dataTypes/tabCelText.vue?vue&type=template&id=f0a2cf4e&scoped=true
var render = function render() {
  var _vm = this,
    _c = _vm._self._c;
  return _c('div', {
    staticClass: "table-item"
  }, [_vm._ssrNode("<div" + _vm._ssrClass("ellipsis w-100", [_vm.textPosition, _vm.textColor]) + " data-v-f0a2cf4e>", "</div>", [_vm._t("default")], 2)]);
};
var staticRenderFns = [];

// CONCATENATED MODULE: ./components/simpleTable/dataTypes/tabCelText.vue?vue&type=template&id=f0a2cf4e&scoped=true

// CONCATENATED MODULE: ./node_modules/babel-loader/lib??ref--2-0!./node_modules/@nuxt/components/dist/loader.js??ref--0-0!./node_modules/vue-loader/lib??vue-loader-options!./components/simpleTable/dataTypes/tabCelText.vue?vue&type=script&lang=js
/* harmony default export */ var tabCelTextvue_type_script_lang_js = ({
  props: {
    textPosition: {
      type: String,
      required: false,
      default: 'left'
    },
    textColor: {
      type: String,
      required: false,
      default: ''
    }
  }
});
// CONCATENATED MODULE: ./components/simpleTable/dataTypes/tabCelText.vue?vue&type=script&lang=js
 /* harmony default export */ var dataTypes_tabCelTextvue_type_script_lang_js = (tabCelTextvue_type_script_lang_js); 
// EXTERNAL MODULE: ./node_modules/vue-loader/lib/runtime/componentNormalizer.js
var componentNormalizer = __webpack_require__(3);

// CONCATENATED MODULE: ./components/simpleTable/dataTypes/tabCelText.vue



function injectStyles (context) {
  
  var style0 = __webpack_require__(82)
if (style0.__inject__) style0.__inject__(context)

}

/* normalize component */

var component = Object(componentNormalizer["a" /* default */])(
  dataTypes_tabCelTextvue_type_script_lang_js,
  render,
  staticRenderFns,
  false,
  injectStyles,
  "f0a2cf4e",
  "54161bea"
  
)

/* harmony default export */ var tabCelText = __webpack_exports__["default"] = (component.exports);

/***/ })

};;
//# sourceMappingURL=simple-table-data-types-tab-cel-text.js.map