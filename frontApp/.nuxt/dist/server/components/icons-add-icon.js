exports.ids = [6];
exports.modules = {

/***/ 101:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
// ESM COMPAT FLAG
__webpack_require__.r(__webpack_exports__);

// CONCATENATED MODULE: ./node_modules/babel-loader/lib??ref--2-0!./node_modules/vue-loader/lib/loaders/templateLoader.js??ref--6!./node_modules/@nuxt/components/dist/loader.js??ref--0-0!./node_modules/vue-loader/lib??vue-loader-options!./components/icons/AddIcon.vue?vue&type=template&id=ebb9cbee&scoped=true
var render = function render() {
  var _vm = this,
    _c = _vm._self._c;
  return _c('svg', {
    attrs: {
      "xmlns": "http://www.w3.org/2000/svg",
      "width": _vm.width,
      "height": _vm.height,
      "viewBox": "0 0 27.971 27.971"
    }
  }, [_vm._ssrNode("<g transform=\"translate(-3.375 -3.375)\" data-v-ebb9cbee><path d=\"M23.757,16.6H18.748V11.588a1.076,1.076,0,1,0-2.152,0V16.6H11.588a1.03,1.03,0,0,0-1.076,1.076,1.041,1.041,0,0,0,1.076,1.076H16.6v5.009a1.042,1.042,0,0,0,1.076,1.076,1.07,1.07,0,0,0,1.076-1.076V18.748h5.009a1.076,1.076,0,1,0,0-2.152Z\" transform=\"translate(-0.312 -0.312)\"" + _vm._ssrClass(null, _vm.color) + " data-v-ebb9cbee></path> <path d=\"M17.36,5.258A12.1,12.1,0,1,1,8.8,8.8,12.023,12.023,0,0,1,17.36,5.258m0-1.883A13.985,13.985,0,1,0,31.346,17.36,13.983,13.983,0,0,0,17.36,3.375Z\" transform=\"translate(0 0)\"" + _vm._ssrClass(null, _vm.color) + " data-v-ebb9cbee></path></g>")]);
};
var staticRenderFns = [];

// CONCATENATED MODULE: ./components/icons/AddIcon.vue?vue&type=template&id=ebb9cbee&scoped=true

// CONCATENATED MODULE: ./node_modules/babel-loader/lib??ref--2-0!./node_modules/@nuxt/components/dist/loader.js??ref--0-0!./node_modules/vue-loader/lib??vue-loader-options!./components/icons/AddIcon.vue?vue&type=script&lang=js
/* harmony default export */ var AddIconvue_type_script_lang_js = ({
  props: {
    fill: {
      type: String,
      required: false,
      default: () => 'none'
    },
    stroke: {
      type: String,
      required: false,
      default: () => '#285881'
    },
    strokeWidth: {
      type: String,
      required: false,
      default: () => '2.083px'
    },
    width: {
      type: String,
      required: false,
      default: '27.971'
    },
    height: {
      type: String,
      required: false,
      default: '20.971'
    },
    color: {
      type: String,
      require: false,
      default: 'primary',
      validator(val) {
        return ['black', 'primary', 'grey', 'gray', 'white'].includes(val);
      }
    }
  }
});
// CONCATENATED MODULE: ./components/icons/AddIcon.vue?vue&type=script&lang=js
 /* harmony default export */ var icons_AddIconvue_type_script_lang_js = (AddIconvue_type_script_lang_js); 
// EXTERNAL MODULE: ./node_modules/vue-loader/lib/runtime/componentNormalizer.js
var componentNormalizer = __webpack_require__(3);

// CONCATENATED MODULE: ./components/icons/AddIcon.vue



function injectStyles (context) {
  
  var style0 = __webpack_require__(95)
if (style0.__inject__) style0.__inject__(context)

}

/* normalize component */

var component = Object(componentNormalizer["a" /* default */])(
  icons_AddIconvue_type_script_lang_js,
  render,
  staticRenderFns,
  false,
  injectStyles,
  "ebb9cbee",
  "37360f60"
  
)

/* harmony default export */ var AddIcon = __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ 79:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(96);
if(content.__esModule) content = content.default;
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add CSS to SSR context
var add = __webpack_require__(6).default
module.exports.__inject__ = function (context) {
  add("5b4f35f7", content, true, context)
};

/***/ }),

/***/ 95:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_style_loader_index_js_ref_7_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_7_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_dist_cjs_js_ref_7_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_7_oneOf_1_3_node_modules_nuxt_components_dist_loader_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_AddIcon_vue_vue_type_style_index_0_id_ebb9cbee_prod_lang_scss_scoped_true__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(79);
/* harmony import */ var _node_modules_vue_style_loader_index_js_ref_7_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_7_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_dist_cjs_js_ref_7_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_7_oneOf_1_3_node_modules_nuxt_components_dist_loader_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_AddIcon_vue_vue_type_style_index_0_id_ebb9cbee_prod_lang_scss_scoped_true__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_vue_style_loader_index_js_ref_7_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_7_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_dist_cjs_js_ref_7_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_7_oneOf_1_3_node_modules_nuxt_components_dist_loader_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_AddIcon_vue_vue_type_style_index_0_id_ebb9cbee_prod_lang_scss_scoped_true__WEBPACK_IMPORTED_MODULE_0__);
/* harmony reexport (unknown) */ for(var __WEBPACK_IMPORT_KEY__ in _node_modules_vue_style_loader_index_js_ref_7_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_7_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_dist_cjs_js_ref_7_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_7_oneOf_1_3_node_modules_nuxt_components_dist_loader_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_AddIcon_vue_vue_type_style_index_0_id_ebb9cbee_prod_lang_scss_scoped_true__WEBPACK_IMPORTED_MODULE_0__) if(["default"].indexOf(__WEBPACK_IMPORT_KEY__) < 0) (function(key) { __webpack_require__.d(__webpack_exports__, key, function() { return _node_modules_vue_style_loader_index_js_ref_7_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_7_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_dist_cjs_js_ref_7_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_7_oneOf_1_3_node_modules_nuxt_components_dist_loader_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_AddIcon_vue_vue_type_style_index_0_id_ebb9cbee_prod_lang_scss_scoped_true__WEBPACK_IMPORTED_MODULE_0__[key]; }) }(__WEBPACK_IMPORT_KEY__));


/***/ }),

/***/ 96:
/***/ (function(module, exports, __webpack_require__) {

// Imports
var ___CSS_LOADER_API_IMPORT___ = __webpack_require__(5);
var ___CSS_LOADER_EXPORT___ = ___CSS_LOADER_API_IMPORT___(function(i){return i[1]});
// Module
___CSS_LOADER_EXPORT___.push([module.i, ".gray[data-v-ebb9cbee],.grey[data-v-ebb9cbee]{fill:#d2d8eb}.primary[data-v-ebb9cbee]{fill:var(--color-primary)}.black[data-v-ebb9cbee]{fill:#000}.white[data-v-ebb9cbee]{fill:#fff}", ""]);
// Exports
___CSS_LOADER_EXPORT___.locals = {};
module.exports = ___CSS_LOADER_EXPORT___;


/***/ })

};;
//# sourceMappingURL=icons-add-icon.js.map