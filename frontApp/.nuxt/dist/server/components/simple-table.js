exports.ids = [9];
exports.modules = {

/***/ 84:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
// ESM COMPAT FLAG
__webpack_require__.r(__webpack_exports__);

// CONCATENATED MODULE: ./node_modules/babel-loader/lib??ref--2-0!./node_modules/vue-loader/lib/loaders/templateLoader.js??ref--6!./node_modules/@nuxt/components/dist/loader.js??ref--0-0!./node_modules/vue-loader/lib??vue-loader-options!./components/simpleTable/index.vue?vue&type=template&id=494caff7
var render = function render() {
  var _vm = this,
    _c = _vm._self._c;
  return _c('div', [_vm._ssrNode("<div class=\"pagination\"><div class=\"mt-[30px] flex justify-end\"><div>1/12</div></div></div> "), _vm._ssrNode("<div class=\"s-table\">", "</div>", [_vm._ssrNode("<div" + _vm._ssrClass("table-header", {
    'responsive': _vm.smallScreen
  }) + ">", "</div>", [_vm._ssrNode("<div class=\"table-header__row\">", "</div>", [_vm._l(_vm.columns, function (col) {
    return [_vm._t("tHeader", null, {
      "col": col
    })];
  })], 2)]), _vm._ssrNode(" "), _vm._ssrNode("<div class=\"table-body\">", "</div>", [_vm._l(_vm.data, function (row) {
    return [_vm._t("default", null, {
      "row": row
    })];
  })], 2)], 2)], 2);
};
var staticRenderFns = [];

// CONCATENATED MODULE: ./components/simpleTable/index.vue?vue&type=template&id=494caff7

// CONCATENATED MODULE: ./node_modules/babel-loader/lib??ref--2-0!./node_modules/@nuxt/components/dist/loader.js??ref--0-0!./node_modules/vue-loader/lib??vue-loader-options!./components/simpleTable/index.vue?vue&type=script&lang=js
/* harmony default export */ var simpleTablevue_type_script_lang_js = ({
  props: {
    columns: {
      type: Array,
      required: true
    },
    data: {
      type: Array,
      required: true
    }
  },
  data() {
    return {
      smallScreen: false
    };
  },
  mounted() {
    this.handleResize();
    window.addEventListener('resize', this.handleResize);
  },
  destroyed() {
    window.removeEventListener('resize', this.handleResize);
  },
  methods: {
    handleResize() {
      if (window.innerWidth <= 825) {
        this.smallScreen = true;
      } else {
        this.smallScreen = false;
      }
    }
  }
});
// CONCATENATED MODULE: ./components/simpleTable/index.vue?vue&type=script&lang=js
 /* harmony default export */ var components_simpleTablevue_type_script_lang_js = (simpleTablevue_type_script_lang_js); 
// EXTERNAL MODULE: ./node_modules/vue-loader/lib/runtime/componentNormalizer.js
var componentNormalizer = __webpack_require__(3);

// CONCATENATED MODULE: ./components/simpleTable/index.vue



function injectStyles (context) {
  
  
}

/* normalize component */

var component = Object(componentNormalizer["a" /* default */])(
  components_simpleTablevue_type_script_lang_js,
  render,
  staticRenderFns,
  false,
  injectStyles,
  null,
  "4c8cb47a"
  
)

/* harmony default export */ var simpleTable = __webpack_exports__["default"] = (component.exports);

/***/ })

};;
//# sourceMappingURL=simple-table.js.map