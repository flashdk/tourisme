exports.ids = [13];
exports.modules = {

/***/ 72:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
// ESM COMPAT FLAG
__webpack_require__.r(__webpack_exports__);

// CONCATENATED MODULE: ./node_modules/babel-loader/lib??ref--2-0!./node_modules/vue-loader/lib/loaders/templateLoader.js??ref--6!./node_modules/@nuxt/components/dist/loader.js??ref--0-0!./node_modules/vue-loader/lib??vue-loader-options!./components/Spinner.vue?vue&type=template&id=327186fa
var render = function render() {
  var _vm = this,
    _c = _vm._self._c;
  return _c('svg', {
    staticStyle: {
      "shape-rendering": "auto"
    },
    attrs: {
      "xmlns": "http://www.w3.org/2000/svg",
      "width": _vm.width,
      "height": _vm.height,
      "viewBox": "0 0 100 100",
      "preserveAspectRatio": "xMidYMid"
    }
  }, [_vm._ssrNode("<path d=\"M10 50A40 40 0 0 0 90 50A40 42 0 0 1 10 50\"" + _vm._ssrAttr("stroke", _vm.stroke) + _vm._ssrAttr("fill", _vm.fill) + " stroke-width=\"5px\">", "</path>", [_c('animateTransform', {
    attrs: {
      "attributeName": "transform",
      "type": "rotate",
      "dur": "1s",
      "repeatCount": "indefinite",
      "keyTimes": "0;1",
      "values": "0 50 51;360 50 51"
    }
  })], 1)]);
};
var staticRenderFns = [];

// CONCATENATED MODULE: ./components/Spinner.vue?vue&type=template&id=327186fa

// CONCATENATED MODULE: ./node_modules/babel-loader/lib??ref--2-0!./node_modules/@nuxt/components/dist/loader.js??ref--0-0!./node_modules/vue-loader/lib??vue-loader-options!./components/Spinner.vue?vue&type=script&lang=js
/* harmony default export */ var Spinnervue_type_script_lang_js = ({
  props: {
    width: {
      required: false,
      type: String,
      default: () => '20px'
    },
    height: {
      required: false,
      type: String,
      default: () => '20px'
    },
    stroke: {
      required: false,
      type: String,
      default: () => '#000000'
    },
    fill: {
      required: false,
      type: String,
      default: () => '#000000'
    }
  }
});
// CONCATENATED MODULE: ./components/Spinner.vue?vue&type=script&lang=js
 /* harmony default export */ var components_Spinnervue_type_script_lang_js = (Spinnervue_type_script_lang_js); 
// EXTERNAL MODULE: ./node_modules/vue-loader/lib/runtime/componentNormalizer.js
var componentNormalizer = __webpack_require__(3);

// CONCATENATED MODULE: ./components/Spinner.vue





/* normalize component */

var component = Object(componentNormalizer["a" /* default */])(
  components_Spinnervue_type_script_lang_js,
  render,
  staticRenderFns,
  false,
  null,
  null,
  "8ecfb218"
  
)

/* harmony default export */ var Spinner = __webpack_exports__["default"] = (component.exports);

/***/ })

};;
//# sourceMappingURL=spinner.js.map