exports.ids = [1,3,13];
exports.modules = {

/***/ 112:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_style_loader_index_js_ref_7_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_7_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_dist_cjs_js_ref_7_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_7_oneOf_1_3_node_modules_nuxt_components_dist_loader_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_addTag_vue_vue_type_style_index_0_id_c0ec4fbe_prod_lang_scss_scoped_true__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(99);
/* harmony import */ var _node_modules_vue_style_loader_index_js_ref_7_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_7_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_dist_cjs_js_ref_7_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_7_oneOf_1_3_node_modules_nuxt_components_dist_loader_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_addTag_vue_vue_type_style_index_0_id_c0ec4fbe_prod_lang_scss_scoped_true__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_vue_style_loader_index_js_ref_7_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_7_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_dist_cjs_js_ref_7_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_7_oneOf_1_3_node_modules_nuxt_components_dist_loader_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_addTag_vue_vue_type_style_index_0_id_c0ec4fbe_prod_lang_scss_scoped_true__WEBPACK_IMPORTED_MODULE_0__);
/* harmony reexport (unknown) */ for(var __WEBPACK_IMPORT_KEY__ in _node_modules_vue_style_loader_index_js_ref_7_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_7_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_dist_cjs_js_ref_7_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_7_oneOf_1_3_node_modules_nuxt_components_dist_loader_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_addTag_vue_vue_type_style_index_0_id_c0ec4fbe_prod_lang_scss_scoped_true__WEBPACK_IMPORTED_MODULE_0__) if(["default"].indexOf(__WEBPACK_IMPORT_KEY__) < 0) (function(key) { __webpack_require__.d(__webpack_exports__, key, function() { return _node_modules_vue_style_loader_index_js_ref_7_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_7_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_dist_cjs_js_ref_7_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_7_oneOf_1_3_node_modules_nuxt_components_dist_loader_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_addTag_vue_vue_type_style_index_0_id_c0ec4fbe_prod_lang_scss_scoped_true__WEBPACK_IMPORTED_MODULE_0__[key]; }) }(__WEBPACK_IMPORT_KEY__));


/***/ }),

/***/ 113:
/***/ (function(module, exports, __webpack_require__) {

// Imports
var ___CSS_LOADER_API_IMPORT___ = __webpack_require__(5);
var ___CSS_LOADER_EXPORT___ = ___CSS_LOADER_API_IMPORT___(function(i){return i[1]});
// Module
___CSS_LOADER_EXPORT___.push([module.i, "button[data-v-c0ec4fbe]{border:none;border-radius:5;color:var(--color-black);cursor:default;font-size:12;min-width:10;outline:none!important;padding:5 10;text-align:center}button.grey[data-v-c0ec4fbe]{background-color:#c7c7c7}button.blue-grey[data-v-c0ec4fbe]{background-color:var(--color-primary-variant)}button.green[data-v-c0ec4fbe]{background-color:var(--color-success-light)}button.light-green[data-v-c0ec4fbe]{background-color:#b7edd1}button.danger[data-v-c0ec4fbe]{background-color:var(--color-danger-light);color:#fff}button.light-danger[data-v-c0ec4fbe]{background-color:var(--color-danger-dark);color:#fff}button .delete[data-v-c0ec4fbe]{bottom:0;font-size:28px;left:-5px;position:absolute;top:-23px;transform:rotate(47deg);width:12px}", ""]);
// Exports
___CSS_LOADER_EXPORT___.locals = {};
module.exports = ___CSS_LOADER_EXPORT___;


/***/ }),

/***/ 129:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
// ESM COMPAT FLAG
__webpack_require__.r(__webpack_exports__);

// CONCATENATED MODULE: ./node_modules/babel-loader/lib??ref--2-0!./node_modules/vue-loader/lib/loaders/templateLoader.js??ref--6!./node_modules/@nuxt/components/dist/loader.js??ref--0-0!./node_modules/vue-loader/lib??vue-loader-options!./components/addTag.vue?vue&type=template&id=c0ec4fbe&scoped=true
var render = function render() {
  var _vm = this,
    _c = _vm._self._c;
  return _c('button', {
    staticClass: "flex items-center",
    class: `${_vm.localBgColor} ${_vm.btnClass}`,
    on: {
      "click": function ($event) {
        $event.stopPropagation();
        return _vm.emitClick.apply(null, arguments);
      }
    }
  }, [_vm._ssrNode("<div class=\"mr-2\" data-v-c0ec4fbe>" + _vm._ssrEscape(_vm._s(_vm.label)) + "</div> " + (_vm.props.deleteIcon ? "<div class=\"relative mx-[6px]\" data-v-c0ec4fbe><div class=\"delete pl-1 pr-1 cursor-pointer\" data-v-c0ec4fbe>+</div></div>" : "<!---->"))]);
};
var staticRenderFns = [];

// CONCATENATED MODULE: ./components/addTag.vue?vue&type=template&id=c0ec4fbe&scoped=true

// CONCATENATED MODULE: ./node_modules/babel-loader/lib??ref--2-0!./node_modules/@nuxt/components/dist/loader.js??ref--0-0!./node_modules/vue-loader/lib??vue-loader-options!./components/addTag.vue?vue&type=script&lang=js
/* harmony default export */ var addTagvue_type_script_lang_js = ({
  props: {
    label: {
      type: String,
      required: true
    },
    deleteIcon: {
      type: Boolean,
      required: false,
      default: false
    },
    bgColor: {
      type: String,
      required: false,
      default: 'grey',
      validator(val) {
        return ['grey', 'blue-grey', 'blue', 'warning', 'danger', 'light-danger', 'pink', 'green', 'light-green'].includes(val);
      }
    },
    btnClass: {
      type: String,
      required: false,
      default: ''
    }
  },
  data() {
    return {
      localBgColor: 'grey'
    };
  },
  mounted() {
    if (props.bgColor === 'grey') {
      localBgColor.value = 'grey';
    } else if (props.bgColor === 'blue-grey') {
      localBgColor.value = 'blue-grey';
    } else if (props.bgColor === 'green') {
      localBgColor.value = 'green';
    } else if (props.bgColor === 'light-green') {
      localBgColor.value = 'light-green';
    } else if (props.bgColor === 'danger') {
      localBgColor.value = 'danger';
    } else if (props.bgColor === 'light-danger') {
      localBgColor.value = 'light-danger';
    }
  },
  methods: {
    emitClick() {
      this.$emit('click');
    }
  }
});
// CONCATENATED MODULE: ./components/addTag.vue?vue&type=script&lang=js
 /* harmony default export */ var components_addTagvue_type_script_lang_js = (addTagvue_type_script_lang_js); 
// EXTERNAL MODULE: ./node_modules/vue-loader/lib/runtime/componentNormalizer.js
var componentNormalizer = __webpack_require__(3);

// CONCATENATED MODULE: ./components/addTag.vue



function injectStyles (context) {
  
  var style0 = __webpack_require__(112)
if (style0.__inject__) style0.__inject__(context)

}

/* normalize component */

var component = Object(componentNormalizer["a" /* default */])(
  components_addTagvue_type_script_lang_js,
  render,
  staticRenderFns,
  false,
  injectStyles,
  "c0ec4fbe",
  "6567b60e"
  
)

/* harmony default export */ var addTag = __webpack_exports__["default"] = (component.exports);

/* nuxt-component-imports */
installComponents(component, {Button: __webpack_require__(70).default})


/***/ }),

/***/ 70:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
// ESM COMPAT FLAG
__webpack_require__.r(__webpack_exports__);

// CONCATENATED MODULE: ./node_modules/babel-loader/lib??ref--2-0!./node_modules/vue-loader/lib/loaders/templateLoader.js??ref--6!./node_modules/@nuxt/components/dist/loader.js??ref--0-0!./node_modules/vue-loader/lib??vue-loader-options!./components/button.vue?vue&type=template&id=050195de&scoped=true
var render = function render() {
  var _vm = this,
    _c = _vm._self._c;
  return _c('button', {
    staticClass: "dk-flex items-center justify-center",
    on: {
      "click": function ($event) {
        return _vm.$emit('click', $event);
      }
    }
  }, [_vm.showSpinner ? _c('spinner', {
    staticClass: "mr-2",
    attrs: {
      "height": "15px",
      "stroke": "#fff",
      "fill": "#fff"
    }
  }) : _vm._e(), _vm._ssrNode(" " + (_vm.icon ? "<i" + _vm._ssrClass("pr-2", _vm.icon) + " data-v-050195de></i>" : "<!---->") + " <span data-v-050195de>" + _vm._ssrEscape(_vm._s(_vm.title)) + "</span>")], 2);
};
var staticRenderFns = [];

// CONCATENATED MODULE: ./components/button.vue?vue&type=template&id=050195de&scoped=true

// CONCATENATED MODULE: ./node_modules/babel-loader/lib??ref--2-0!./node_modules/@nuxt/components/dist/loader.js??ref--0-0!./node_modules/vue-loader/lib??vue-loader-options!./components/button.vue?vue&type=script&lang=js
/* harmony default export */ var buttonvue_type_script_lang_js = ({
  props: {
    title: {
      type: String,
      required: true
    },
    showSpinner: {
      type: Boolean,
      required: false,
      default: false
    },
    icon: {
      type: String,
      required: false,
      default: ''
    }
  }
});
// CONCATENATED MODULE: ./components/button.vue?vue&type=script&lang=js
 /* harmony default export */ var components_buttonvue_type_script_lang_js = (buttonvue_type_script_lang_js); 
// EXTERNAL MODULE: ./node_modules/vue-loader/lib/runtime/componentNormalizer.js
var componentNormalizer = __webpack_require__(3);

// CONCATENATED MODULE: ./components/button.vue



function injectStyles (context) {
  
  var style0 = __webpack_require__(73)
if (style0.__inject__) style0.__inject__(context)

}

/* normalize component */

var component = Object(componentNormalizer["a" /* default */])(
  components_buttonvue_type_script_lang_js,
  render,
  staticRenderFns,
  false,
  injectStyles,
  "050195de",
  "655d3987"
  
)

/* harmony default export */ var components_button = __webpack_exports__["default"] = (component.exports);

/* nuxt-component-imports */
installComponents(component, {Spinner: __webpack_require__(72).default,Button: __webpack_require__(70).default})


/***/ }),

/***/ 71:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(74);
if(content.__esModule) content = content.default;
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add CSS to SSR context
var add = __webpack_require__(6).default
module.exports.__inject__ = function (context) {
  add("72f4a40c", content, true, context)
};

/***/ }),

/***/ 72:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
// ESM COMPAT FLAG
__webpack_require__.r(__webpack_exports__);

// CONCATENATED MODULE: ./node_modules/babel-loader/lib??ref--2-0!./node_modules/vue-loader/lib/loaders/templateLoader.js??ref--6!./node_modules/@nuxt/components/dist/loader.js??ref--0-0!./node_modules/vue-loader/lib??vue-loader-options!./components/Spinner.vue?vue&type=template&id=327186fa
var render = function render() {
  var _vm = this,
    _c = _vm._self._c;
  return _c('svg', {
    staticStyle: {
      "shape-rendering": "auto"
    },
    attrs: {
      "xmlns": "http://www.w3.org/2000/svg",
      "width": _vm.width,
      "height": _vm.height,
      "viewBox": "0 0 100 100",
      "preserveAspectRatio": "xMidYMid"
    }
  }, [_vm._ssrNode("<path d=\"M10 50A40 40 0 0 0 90 50A40 42 0 0 1 10 50\"" + _vm._ssrAttr("stroke", _vm.stroke) + _vm._ssrAttr("fill", _vm.fill) + " stroke-width=\"5px\">", "</path>", [_c('animateTransform', {
    attrs: {
      "attributeName": "transform",
      "type": "rotate",
      "dur": "1s",
      "repeatCount": "indefinite",
      "keyTimes": "0;1",
      "values": "0 50 51;360 50 51"
    }
  })], 1)]);
};
var staticRenderFns = [];

// CONCATENATED MODULE: ./components/Spinner.vue?vue&type=template&id=327186fa

// CONCATENATED MODULE: ./node_modules/babel-loader/lib??ref--2-0!./node_modules/@nuxt/components/dist/loader.js??ref--0-0!./node_modules/vue-loader/lib??vue-loader-options!./components/Spinner.vue?vue&type=script&lang=js
/* harmony default export */ var Spinnervue_type_script_lang_js = ({
  props: {
    width: {
      required: false,
      type: String,
      default: () => '20px'
    },
    height: {
      required: false,
      type: String,
      default: () => '20px'
    },
    stroke: {
      required: false,
      type: String,
      default: () => '#000000'
    },
    fill: {
      required: false,
      type: String,
      default: () => '#000000'
    }
  }
});
// CONCATENATED MODULE: ./components/Spinner.vue?vue&type=script&lang=js
 /* harmony default export */ var components_Spinnervue_type_script_lang_js = (Spinnervue_type_script_lang_js); 
// EXTERNAL MODULE: ./node_modules/vue-loader/lib/runtime/componentNormalizer.js
var componentNormalizer = __webpack_require__(3);

// CONCATENATED MODULE: ./components/Spinner.vue





/* normalize component */

var component = Object(componentNormalizer["a" /* default */])(
  components_Spinnervue_type_script_lang_js,
  render,
  staticRenderFns,
  false,
  null,
  null,
  "8ecfb218"
  
)

/* harmony default export */ var Spinner = __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ 73:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_style_loader_index_js_ref_7_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_7_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_dist_cjs_js_ref_7_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_7_oneOf_1_3_node_modules_nuxt_components_dist_loader_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_button_vue_vue_type_style_index_0_id_050195de_prod_lang_scss_scoped_true__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(71);
/* harmony import */ var _node_modules_vue_style_loader_index_js_ref_7_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_7_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_dist_cjs_js_ref_7_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_7_oneOf_1_3_node_modules_nuxt_components_dist_loader_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_button_vue_vue_type_style_index_0_id_050195de_prod_lang_scss_scoped_true__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_vue_style_loader_index_js_ref_7_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_7_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_dist_cjs_js_ref_7_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_7_oneOf_1_3_node_modules_nuxt_components_dist_loader_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_button_vue_vue_type_style_index_0_id_050195de_prod_lang_scss_scoped_true__WEBPACK_IMPORTED_MODULE_0__);
/* harmony reexport (unknown) */ for(var __WEBPACK_IMPORT_KEY__ in _node_modules_vue_style_loader_index_js_ref_7_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_7_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_dist_cjs_js_ref_7_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_7_oneOf_1_3_node_modules_nuxt_components_dist_loader_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_button_vue_vue_type_style_index_0_id_050195de_prod_lang_scss_scoped_true__WEBPACK_IMPORTED_MODULE_0__) if(["default"].indexOf(__WEBPACK_IMPORT_KEY__) < 0) (function(key) { __webpack_require__.d(__webpack_exports__, key, function() { return _node_modules_vue_style_loader_index_js_ref_7_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_7_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_dist_cjs_js_ref_7_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_7_oneOf_1_3_node_modules_nuxt_components_dist_loader_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_button_vue_vue_type_style_index_0_id_050195de_prod_lang_scss_scoped_true__WEBPACK_IMPORTED_MODULE_0__[key]; }) }(__WEBPACK_IMPORT_KEY__));


/***/ }),

/***/ 74:
/***/ (function(module, exports, __webpack_require__) {

// Imports
var ___CSS_LOADER_API_IMPORT___ = __webpack_require__(5);
var ___CSS_LOADER_EXPORT___ = ___CSS_LOADER_API_IMPORT___(function(i){return i[1]});
// Module
___CSS_LOADER_EXPORT___.push([module.i, ".dk-btn[data-v-050195de],.dk-btn--black[data-v-050195de],.dk-btn--black__outline[data-v-050195de],.dk-btn--blue[data-v-050195de],.dk-btn--danger[data-v-050195de],.dk-btn--empty[data-v-050195de],.dk-btn--empty__borderless[data-v-050195de],.dk-btn--primary[data-v-050195de],.dk-btn--primary__outline[data-v-050195de],.dk-btn--secondary[data-v-050195de],.dk-btn--secondary__outline[data-v-050195de],.dk-btn--sm[data-v-050195de],.dk-btn--sm--blue[data-v-050195de],.dk-btn--sm--blue-dark[data-v-050195de],.dk-btn--sm--danger[data-v-050195de],.dk-btn--sm--dark-green[data-v-050195de],.dk-btn--sm--dark-grey[data-v-050195de],.dk-btn--sm--grey[data-v-050195de],.dk-btn--sm--info[data-v-050195de],.dk-btn--sm--orange[data-v-050195de],.dk-btn--sm--pink[data-v-050195de],.dk-btn--sm--purple[data-v-050195de],.dk-btn--sm--success[data-v-050195de],.dk-btn--sm--success-dark[data-v-050195de],.dk-btn--sm--warning[data-v-050195de],.dk-btn--success[data-v-050195de],.dk-btn--warning[data-v-050195de]{border:1px solid transparent;border-radius:5px;display:inline-block;font-size:14px;min-width:120px;outline:none!important;padding:16px;text-align:center}.disabled.dk-btn--black[data-v-050195de],.disabled.dk-btn--black__outline[data-v-050195de],.disabled.dk-btn--blue[data-v-050195de],.disabled.dk-btn--danger[data-v-050195de],.disabled.dk-btn--empty[data-v-050195de],.disabled.dk-btn--empty__borderless[data-v-050195de],.disabled.dk-btn--primary[data-v-050195de],.disabled.dk-btn--primary__outline[data-v-050195de],.disabled.dk-btn--secondary[data-v-050195de],.disabled.dk-btn--secondary__outline[data-v-050195de],.disabled.dk-btn--sm[data-v-050195de],.disabled.dk-btn--sm--blue[data-v-050195de],.disabled.dk-btn--sm--blue-dark[data-v-050195de],.disabled.dk-btn--sm--danger[data-v-050195de],.disabled.dk-btn--sm--dark-green[data-v-050195de],.disabled.dk-btn--sm--dark-grey[data-v-050195de],.disabled.dk-btn--sm--grey[data-v-050195de],.disabled.dk-btn--sm--info[data-v-050195de],.disabled.dk-btn--sm--orange[data-v-050195de],.disabled.dk-btn--sm--pink[data-v-050195de],.disabled.dk-btn--sm--purple[data-v-050195de],.disabled.dk-btn--sm--success[data-v-050195de],.disabled.dk-btn--sm--success-dark[data-v-050195de],.disabled.dk-btn--sm--warning[data-v-050195de],.disabled.dk-btn--success[data-v-050195de],.disabled.dk-btn--warning[data-v-050195de],.dk-btn.disabled[data-v-050195de]{opacity:.5;pointer-events:none}.dk-btn--sm[data-v-050195de],.dk-btn--sm--blue[data-v-050195de],.dk-btn--sm--blue-dark[data-v-050195de],.dk-btn--sm--danger[data-v-050195de],.dk-btn--sm--dark-green[data-v-050195de],.dk-btn--sm--dark-grey[data-v-050195de],.dk-btn--sm--grey[data-v-050195de],.dk-btn--sm--info[data-v-050195de],.dk-btn--sm--orange[data-v-050195de],.dk-btn--sm--pink[data-v-050195de],.dk-btn--sm--purple[data-v-050195de],.dk-btn--sm--success[data-v-050195de],.dk-btn--sm--success-dark[data-v-050195de],.dk-btn--sm--warning[data-v-050195de]{border-radius:3px;min-width:81px;padding:5px 16px}@media screen and (max-width:1680px){.dk-btn--sm[data-v-050195de],.dk-btn--sm--blue[data-v-050195de],.dk-btn--sm--blue-dark[data-v-050195de],.dk-btn--sm--danger[data-v-050195de],.dk-btn--sm--dark-green[data-v-050195de],.dk-btn--sm--dark-grey[data-v-050195de],.dk-btn--sm--grey[data-v-050195de],.dk-btn--sm--info[data-v-050195de],.dk-btn--sm--orange[data-v-050195de],.dk-btn--sm--pink[data-v-050195de],.dk-btn--sm--purple[data-v-050195de],.dk-btn--sm--success[data-v-050195de],.dk-btn--sm--success-dark[data-v-050195de],.dk-btn--sm--warning[data-v-050195de]{min-width:71px;padding:5px 10px}}.dk-btn--sm--info[data-v-050195de]{background-color:var(--app-secondary-color);border:1px solid var(--app-secondary-color);color:var(--color-white)}.dk-btn--sm--success[data-v-050195de]{background-color:var(--color-success);border:1px solid var(--color-success);color:var(--color-white)}.dk-btn--sm--purple[data-v-050195de]{background-color:#6e36da;border:1px solid #6e36da;color:var(--color-white)}.dk-btn--sm--dark-green[data-v-050195de]{background-color:#1f9d8a;border:1px solid #1f9d8a;color:var(--color-white)}.dk-btn--sm--success-dark[data-v-050195de]{background-color:#2e9615;border:1px solid #2e9615;color:var(--color-white)}.dk-btn--sm--warning[data-v-050195de]{background-color:var(--color-warning);border:1px solid var(--color-warning);color:var(--color-white)}.dk-btn--sm--grey[data-v-050195de]{background-color:#d2d8eb;border:1px solid #d2d8eb;color:var(--color-dark)}.dk-btn--sm--dark-grey[data-v-050195de]{background-color:var(--color-dark);border:1px solid var(--color-dark);color:var(--color-white)}.dk-btn--sm--pink[data-v-050195de]{background-color:#ff2478;border:1px solid #ff2478;color:var(--color-white)}.dk-btn--sm--orange[data-v-050195de]{background-color:#e8541d;border:1px solid #e8541d;color:var(--color-white)}.dk-btn--sm--blue[data-v-050195de],.dk-btn--sm--blue-dark[data-v-050195de]{background-color:#2c77ff;border:1px solid #2c77ff;color:var(--color-white)}.dk-btn--sm--danger[data-v-050195de]{background-color:red;border:1px solid red;color:var(--color-white)}.dk-btn--secondary[data-v-050195de]{background-color:var(--app-secondary-color);border:none;color:var(--color-white)}@media(hover:hover){.dk-btn--secondary[data-v-050195de]:hover{background-color:var(--app-secondary-color);color:var(--color-white);-webkit-text-decoration:none;text-decoration:none}}.dk-btn--secondary__outline[data-v-050195de]{background-color:var(--color-white);border-color:var(--app-secondary-color);color:var(--app-secondary-color);-webkit-text-decoration:none;text-decoration:none}@media(hover:hover){.dk-btn--secondary__outline[data-v-050195de]:hover{background-color:var(--app-secondary-color);color:var(--color-white);-webkit-text-decoration:none;text-decoration:none}}.dk-btn--primary[data-v-050195de]{background-color:var(--color-primary);border-color:var(--color-primary);color:var(--color-white);-webkit-text-decoration:none;text-decoration:none}@media(hover:hover){.dk-btn--primary[data-v-050195de]:hover{background-color:var(--color-primary);color:var(--color-white);-webkit-text-decoration:none;text-decoration:none}}.dk-btn--primary__outline[data-v-050195de]{background-color:var(--color-white);border-color:var(--color-primary);color:var(--color-primary);-webkit-text-decoration:none;text-decoration:none}@media(hover:hover){.dk-btn--primary__outline[data-v-050195de]:hover{background-color:var(--color-primary);color:var(--color-white);-webkit-text-decoration:none;text-decoration:none}}.dk-btn--success[data-v-050195de]{background-color:var(--color-success);border-color:var(--color-success);color:var(--color-white)}@media(hover:hover){.dk-btn--success[data-v-050195de]:hover{background-color:var(--color-success);border-color:var(--color-success);color:var(--color-white)}}.dk-btn--blue[data-v-050195de]{background-color:var(--color-primary-variant);border-color:var(--color-primary-variant);color:var(--color-white)}@media(hover:hover){.dk-btn--blue[data-v-050195de]:hover{background-color:var(--color-primary-variant);border-color:var(--color-primary-variant);color:var(--color-white)}}.dk-btn--danger[data-v-050195de]{background-color:var(--color-danger);border-color:var(--color-danger);color:var(--color-white)}@media(hover:hover){.dk-btn--danger[data-v-050195de]:hover{background-color:var(--color-danger);border-color:var(--color-danger);color:var(--color-white)}}.dk-btn--warning[data-v-050195de]{background-color:var(--color-warning);border-color:var(--color-warning);color:var(--color-white)}@media(hover:hover){.dk-btn--warning[data-v-050195de]:hover{background-color:var(--color-warning);border-color:var(--color-warning);color:var(--color-white)}}.dk-btn--black[data-v-050195de]{background-color:var(--color-dark);color:var(--color-white)}@media(hover:hover){.dk-btn--black[data-v-050195de]:hover{background-color:var(--color-dark);color:var(--color-white)}}.dk-btn--black__outline[data-v-050195de]{background-color:var(--color-white);border-color:var(--color-dark);color:var(--color-dark)}.dk-btn--empty[data-v-050195de],.dk-btn--empty[data-v-050195de]:hover,.dk-btn--empty__borderless[data-v-050195de]{background-color:transparent;color:var(--color-dark)}.dk-btn--empty__borderless[data-v-050195de]{border-width:0}", ""]);
// Exports
___CSS_LOADER_EXPORT___.locals = {};
module.exports = ___CSS_LOADER_EXPORT___;


/***/ }),

/***/ 99:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(113);
if(content.__esModule) content = content.default;
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add CSS to SSR context
var add = __webpack_require__(6).default
module.exports.__inject__ = function (context) {
  add("0a2d84ea", content, true, context)
};

/***/ })

};;
//# sourceMappingURL=add-tag.js.map