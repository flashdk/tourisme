exports.ids = [19,3,13];
exports.modules = {

/***/ 119:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(131);
if(content.__esModule) content = content.default;
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add CSS to SSR context
var add = __webpack_require__(6).default
module.exports.__inject__ = function (context) {
  add("b5a91eee", content, true, context)
};

/***/ }),

/***/ 130:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_style_loader_index_js_ref_7_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_7_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_dist_cjs_js_ref_7_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_7_oneOf_1_3_node_modules_nuxt_components_dist_loader_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_SingIn_vue_vue_type_style_index_0_id_01bfb82c_prod_lang_scss_scoped_true__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(119);
/* harmony import */ var _node_modules_vue_style_loader_index_js_ref_7_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_7_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_dist_cjs_js_ref_7_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_7_oneOf_1_3_node_modules_nuxt_components_dist_loader_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_SingIn_vue_vue_type_style_index_0_id_01bfb82c_prod_lang_scss_scoped_true__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_vue_style_loader_index_js_ref_7_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_7_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_dist_cjs_js_ref_7_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_7_oneOf_1_3_node_modules_nuxt_components_dist_loader_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_SingIn_vue_vue_type_style_index_0_id_01bfb82c_prod_lang_scss_scoped_true__WEBPACK_IMPORTED_MODULE_0__);
/* harmony reexport (unknown) */ for(var __WEBPACK_IMPORT_KEY__ in _node_modules_vue_style_loader_index_js_ref_7_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_7_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_dist_cjs_js_ref_7_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_7_oneOf_1_3_node_modules_nuxt_components_dist_loader_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_SingIn_vue_vue_type_style_index_0_id_01bfb82c_prod_lang_scss_scoped_true__WEBPACK_IMPORTED_MODULE_0__) if(["default"].indexOf(__WEBPACK_IMPORT_KEY__) < 0) (function(key) { __webpack_require__.d(__webpack_exports__, key, function() { return _node_modules_vue_style_loader_index_js_ref_7_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_7_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_dist_cjs_js_ref_7_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_7_oneOf_1_3_node_modules_nuxt_components_dist_loader_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_SingIn_vue_vue_type_style_index_0_id_01bfb82c_prod_lang_scss_scoped_true__WEBPACK_IMPORTED_MODULE_0__[key]; }) }(__WEBPACK_IMPORT_KEY__));


/***/ }),

/***/ 131:
/***/ (function(module, exports, __webpack_require__) {

// Imports
var ___CSS_LOADER_API_IMPORT___ = __webpack_require__(5);
var ___CSS_LOADER_EXPORT___ = ___CSS_LOADER_API_IMPORT___(function(i){return i[1]});
// Module
___CSS_LOADER_EXPORT___.push([module.i, ".sing-in[data-v-01bfb82c]{min-width:350px;padding:20px}.sing-in .logo[data-v-01bfb82c]{align-items:center;background-color:#8d6e63;border-radius:50%;display:flex;height:60px;justify-content:center;margin:0 auto;overflow:hidden;width:60px}.sing-in .logo img[data-v-01bfb82c]{overflow:hidden;width:100%}.sing-in .forget-pwd[data-v-01bfb82c]{font-size:14px;margin:-8px 0 35px;text-align:right;-webkit-text-decoration:underline;text-decoration:underline}.sing-in .actions[data-v-01bfb82c]{display:flex;flex-direction:column;gap:12px;margin-top:30px}.sing-in .actions .network-auth[data-v-01bfb82c]{display:grid;grid-template-columns:repeat(2,1fr);grid-gap:12px;gap:12px}", ""]);
// Exports
___CSS_LOADER_EXPORT___.locals = {};
module.exports = ___CSS_LOADER_EXPORT___;


/***/ }),

/***/ 150:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
// ESM COMPAT FLAG
__webpack_require__.r(__webpack_exports__);

// CONCATENATED MODULE: ./node_modules/babel-loader/lib??ref--2-0!./node_modules/vue-loader/lib/loaders/templateLoader.js??ref--6!./node_modules/@nuxt/components/dist/loader.js??ref--0-0!./node_modules/vue-loader/lib??vue-loader-options!./pages/Authentification/UserAuth.vue?vue&type=template&id=436161b8
var render = function render() {
  var _vm = this,
    _c = _vm._self._c;
  return _c('div', {
    staticClass: "user-auth"
  }, [_vm.currentPage === 'signup' ? _c('Resigter') : _vm.currentPage === 'resetPwd' ? _c('ResetPassword') : _c('SingIn')], 1);
};
var staticRenderFns = [];

// CONCATENATED MODULE: ./pages/Authentification/UserAuth.vue?vue&type=template&id=436161b8

// CONCATENATED MODULE: ./node_modules/babel-loader/lib??ref--2-0!./node_modules/vue-loader/lib/loaders/templateLoader.js??ref--6!./node_modules/@nuxt/components/dist/loader.js??ref--0-0!./node_modules/vue-loader/lib??vue-loader-options!./pages/Authentification/ResetPassword.vue?vue&type=template&id=6a5ad7ef
var ResetPasswordvue_type_template_id_6a5ad7ef_render = function render() {
  var _vm = this,
    _c = _vm._self._c;
  return _c('div', {
    staticClass: "reste-pwd"
  }, [_vm._ssrNode("\n  reset pwd\n")]);
};
var ResetPasswordvue_type_template_id_6a5ad7ef_staticRenderFns = [];

// CONCATENATED MODULE: ./pages/Authentification/ResetPassword.vue?vue&type=template&id=6a5ad7ef

// CONCATENATED MODULE: ./node_modules/babel-loader/lib??ref--2-0!./node_modules/@nuxt/components/dist/loader.js??ref--0-0!./node_modules/vue-loader/lib??vue-loader-options!./pages/Authentification/ResetPassword.vue?vue&type=script&lang=js
/* harmony default export */ var ResetPasswordvue_type_script_lang_js = ({});
// CONCATENATED MODULE: ./pages/Authentification/ResetPassword.vue?vue&type=script&lang=js
 /* harmony default export */ var Authentification_ResetPasswordvue_type_script_lang_js = (ResetPasswordvue_type_script_lang_js); 
// EXTERNAL MODULE: ./node_modules/vue-loader/lib/runtime/componentNormalizer.js
var componentNormalizer = __webpack_require__(3);

// CONCATENATED MODULE: ./pages/Authentification/ResetPassword.vue





/* normalize component */

var component = Object(componentNormalizer["a" /* default */])(
  Authentification_ResetPasswordvue_type_script_lang_js,
  ResetPasswordvue_type_template_id_6a5ad7ef_render,
  ResetPasswordvue_type_template_id_6a5ad7ef_staticRenderFns,
  false,
  null,
  null,
  "26acc895"
  
)

/* harmony default export */ var ResetPassword = (component.exports);
// CONCATENATED MODULE: ./node_modules/babel-loader/lib??ref--2-0!./node_modules/vue-loader/lib/loaders/templateLoader.js??ref--6!./node_modules/@nuxt/components/dist/loader.js??ref--0-0!./node_modules/vue-loader/lib??vue-loader-options!./pages/Authentification/Resigter.vue?vue&type=template&id=e7dffeaa&scoped=true
var Resigtervue_type_template_id_e7dffeaa_scoped_true_render = function render() {
  var _vm = this,
    _c = _vm._self._c;
  return _c('div', [_vm._ssrNode("register")]);
};
var Resigtervue_type_template_id_e7dffeaa_scoped_true_staticRenderFns = [];

// CONCATENATED MODULE: ./pages/Authentification/Resigter.vue?vue&type=template&id=e7dffeaa&scoped=true

// CONCATENATED MODULE: ./node_modules/babel-loader/lib??ref--2-0!./node_modules/@nuxt/components/dist/loader.js??ref--0-0!./node_modules/vue-loader/lib??vue-loader-options!./pages/Authentification/Resigter.vue?vue&type=script&lang=js
/* harmony default export */ var Resigtervue_type_script_lang_js = ({});
// CONCATENATED MODULE: ./pages/Authentification/Resigter.vue?vue&type=script&lang=js
 /* harmony default export */ var Authentification_Resigtervue_type_script_lang_js = (Resigtervue_type_script_lang_js); 
// CONCATENATED MODULE: ./pages/Authentification/Resigter.vue



function injectStyles (context) {
  
  
}

/* normalize component */

var Resigter_component = Object(componentNormalizer["a" /* default */])(
  Authentification_Resigtervue_type_script_lang_js,
  Resigtervue_type_template_id_e7dffeaa_scoped_true_render,
  Resigtervue_type_template_id_e7dffeaa_scoped_true_staticRenderFns,
  false,
  injectStyles,
  "e7dffeaa",
  "c2349e30"
  
)

/* harmony default export */ var Resigter = (Resigter_component.exports);
// CONCATENATED MODULE: ./node_modules/babel-loader/lib??ref--2-0!./node_modules/vue-loader/lib/loaders/templateLoader.js??ref--6!./node_modules/@nuxt/components/dist/loader.js??ref--0-0!./node_modules/vue-loader/lib??vue-loader-options!./pages/Authentification/SingIn.vue?vue&type=template&id=01bfb82c&scoped=true
var SingInvue_type_template_id_01bfb82c_scoped_true_render = function render() {
  var _vm = this,
    _c = _vm._self._c;
  return _c('div', {
    staticClass: "sing-in max-w-[450px]"
  }, [_vm._ssrNode("<div class=\"logo\" data-v-01bfb82c><img src=\"/logo.png\" alt data-v-01bfb82c></div> <h1 class=\"py-[16px] text-center text-[30px] font-medium\" data-v-01bfb82c>\n    Tourism en afrique\n  </h1> <div class=\"sub-title pb-[32px] text-center\" data-v-01bfb82c>\n    Lorem ipsum, dolor sit amet consectetur adipisicing elit. Iste,\n    perspiciatis.\n  </div> "), _c('ValidationObserver', {
    attrs: {
      "slim": ""
    },
    scopedSlots: _vm._u([{
      key: "default",
      fn: function ({
        handleSubmit
      }) {
        return [_c('form', {
          on: {
            "submit": function ($event) {
              $event.preventDefault();
              return handleSubmit(_vm.submit);
            },
            "keypress": function ($event) {
              if (!$event.type.indexOf('key') && _vm._k($event.keyCode, "enter", 13, $event.key, "Enter")) return null;
              $event.preventDefault();
              return handleSubmit(_vm.submit);
            }
          }
        }, [_c('ValidationProvider', {
          attrs: {
            "name": "email",
            "rules": "required|email"
          },
          scopedSlots: _vm._u([{
            key: "default",
            fn: function ({
              errors
            }) {
              return [_c('div', [_c('input', {
                directives: [{
                  name: "model",
                  rawName: "v-model",
                  value: _vm.email,
                  expression: "email"
                }],
                staticClass: "form--input",
                attrs: {
                  "type": "text"
                },
                domProps: {
                  "value": _vm.email
                },
                on: {
                  "input": function ($event) {
                    if ($event.target.composing) return;
                    _vm.email = $event.target.value;
                  }
                }
              }), _vm._v(" "), _c('div', {
                staticClass: "mt-1 text--danger"
              }, [_vm._v(_vm._s(errors[0]))])])];
            }
          }], null, true)
        }), _vm._v(" "), _c('ValidationProvider', {
          attrs: {
            "name": "password",
            "rules": "required"
          },
          scopedSlots: _vm._u([{
            key: "default",
            fn: function ({
              errors
            }) {
              return [_c('div', {
                staticClass: "mt-[20px]"
              }, [_c('input', {
                directives: [{
                  name: "model",
                  rawName: "v-model",
                  value: _vm.password,
                  expression: "password"
                }],
                staticClass: "form--input",
                attrs: {
                  "type": "password"
                },
                domProps: {
                  "value": _vm.password
                },
                on: {
                  "input": function ($event) {
                    if ($event.target.composing) return;
                    _vm.password = $event.target.value;
                  }
                }
              }), _vm._v(" "), _c('div', {
                staticClass: "mt-1 text--danger"
              }, [_vm._v(_vm._s(errors[0]))])])];
            }
          }], null, true)
        }), _vm._v(" "), _c('div', {
          staticClass: "actions"
        }, [_c('Button', {
          staticClass: "dk-btn--black",
          attrs: {
            "title": "Login",
            "show-spinner": _vm.savingData
          }
        }), _vm._v(" "), _c('div', {
          staticClass: "network-auth"
        }, [_c('Button', {
          staticClass: "dk-btn--danger",
          attrs: {
            "title": "Gmail"
          },
          on: {
            "click": function ($event) {
              $event.preventDefault();
            }
          }
        }), _vm._v(" "), _c('Button', {
          staticClass: "dk-btn--blue",
          attrs: {
            "title": "Facebook"
          },
          on: {
            "click": function ($event) {
              $event.preventDefault();
            }
          }
        })], 1)], 1)], 1)];
      }
    }])
  })], 2);
};
var SingInvue_type_template_id_01bfb82c_scoped_true_staticRenderFns = [];

// CONCATENATED MODULE: ./pages/Authentification/SingIn.vue?vue&type=template&id=01bfb82c&scoped=true

// EXTERNAL MODULE: ./components/button.vue + 4 modules
var components_button = __webpack_require__(70);

// CONCATENATED MODULE: ./node_modules/babel-loader/lib??ref--2-0!./node_modules/@nuxt/components/dist/loader.js??ref--0-0!./node_modules/vue-loader/lib??vue-loader-options!./pages/Authentification/SingIn.vue?vue&type=script&lang=js

/* harmony default export */ var SingInvue_type_script_lang_js = ({
  components: {
    Button: components_button["default"]
  },
  data() {
    return {
      email: '',
      password: '',
      savingData: false
    };
  },
  methods: {
    async submit() {
      this.savingData = true;
      try {
        const token = await this.$auth.loginWith('local', {
          data: {
            email: this.email,
            password: this.password
          }
        });
        if (token) {
          this.$store.dispatch('IS_USER_AUTHENTICARED', true);
          this.$notify({
            message: 'Authentification réussie'
          });
          this.$router.push({
            name: 'admin-dashboard',
            params: {
              locale: this.$locale
            }
          });
        }
        this.savingData = false;
      } catch (error) {
        this.$notify({
          type: 'error',
          message: error.response.data.message
        });
        this.savingData = false;
      }
    }
  }
});
// CONCATENATED MODULE: ./pages/Authentification/SingIn.vue?vue&type=script&lang=js
 /* harmony default export */ var Authentification_SingInvue_type_script_lang_js = (SingInvue_type_script_lang_js); 
// CONCATENATED MODULE: ./pages/Authentification/SingIn.vue



function SingIn_injectStyles (context) {
  
  var style0 = __webpack_require__(130)
if (style0.__inject__) style0.__inject__(context)

}

/* normalize component */

var SingIn_component = Object(componentNormalizer["a" /* default */])(
  Authentification_SingInvue_type_script_lang_js,
  SingInvue_type_template_id_01bfb82c_scoped_true_render,
  SingInvue_type_template_id_01bfb82c_scoped_true_staticRenderFns,
  false,
  SingIn_injectStyles,
  "01bfb82c",
  "46b753ce"
  
)

/* harmony default export */ var SingIn = (SingIn_component.exports);

/* nuxt-component-imports */
installComponents(SingIn_component, {Button: __webpack_require__(70).default})

// CONCATENATED MODULE: ./node_modules/babel-loader/lib??ref--2-0!./node_modules/@nuxt/components/dist/loader.js??ref--0-0!./node_modules/vue-loader/lib??vue-loader-options!./pages/Authentification/UserAuth.vue?vue&type=script&lang=js



/* harmony default export */ var UserAuthvue_type_script_lang_js = ({
  components: {
    SingIn: SingIn,
    Resigter: Resigter,
    ResetPassword: ResetPassword
  },
  layout: 'back/adminAuth',
  data() {
    return {
      currentPage: ''
    };
  },
  watch: {
    '$route.params.selectedPage': {
      immediate: true,
      handler(value) {
        switch (value) {
          case 'signin':
            this.currentPage = 'signin';
            break;
          case 'signup':
            this.currentPage = 'signup';
            break;
          case 'resetPwd':
            this.currentPage = 'resetPwd';
            break;
        }
      }
    }
  },
  mounted() {
    const codeLangue = navigator.language.split('-')[0];
    if (['fr', 'en'].includes(codeLangue)) {
      this.$store.dispatch('SET_LOCALE', codeLangue);
    } else {
      this.$store.dispatch('SET_LOCALE', 'fr');
    }
  }
});
// CONCATENATED MODULE: ./pages/Authentification/UserAuth.vue?vue&type=script&lang=js
 /* harmony default export */ var Authentification_UserAuthvue_type_script_lang_js = (UserAuthvue_type_script_lang_js); 
// CONCATENATED MODULE: ./pages/Authentification/UserAuth.vue





/* normalize component */

var UserAuth_component = Object(componentNormalizer["a" /* default */])(
  Authentification_UserAuthvue_type_script_lang_js,
  render,
  staticRenderFns,
  false,
  null,
  null,
  "d1351150"
  
)

/* harmony default export */ var UserAuth = __webpack_exports__["default"] = (UserAuth_component.exports);

/***/ }),

/***/ 70:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
// ESM COMPAT FLAG
__webpack_require__.r(__webpack_exports__);

// CONCATENATED MODULE: ./node_modules/babel-loader/lib??ref--2-0!./node_modules/vue-loader/lib/loaders/templateLoader.js??ref--6!./node_modules/@nuxt/components/dist/loader.js??ref--0-0!./node_modules/vue-loader/lib??vue-loader-options!./components/button.vue?vue&type=template&id=050195de&scoped=true
var render = function render() {
  var _vm = this,
    _c = _vm._self._c;
  return _c('button', {
    staticClass: "dk-flex items-center justify-center",
    on: {
      "click": function ($event) {
        return _vm.$emit('click', $event);
      }
    }
  }, [_vm.showSpinner ? _c('spinner', {
    staticClass: "mr-2",
    attrs: {
      "height": "15px",
      "stroke": "#fff",
      "fill": "#fff"
    }
  }) : _vm._e(), _vm._ssrNode(" " + (_vm.icon ? "<i" + _vm._ssrClass("pr-2", _vm.icon) + " data-v-050195de></i>" : "<!---->") + " <span data-v-050195de>" + _vm._ssrEscape(_vm._s(_vm.title)) + "</span>")], 2);
};
var staticRenderFns = [];

// CONCATENATED MODULE: ./components/button.vue?vue&type=template&id=050195de&scoped=true

// CONCATENATED MODULE: ./node_modules/babel-loader/lib??ref--2-0!./node_modules/@nuxt/components/dist/loader.js??ref--0-0!./node_modules/vue-loader/lib??vue-loader-options!./components/button.vue?vue&type=script&lang=js
/* harmony default export */ var buttonvue_type_script_lang_js = ({
  props: {
    title: {
      type: String,
      required: true
    },
    showSpinner: {
      type: Boolean,
      required: false,
      default: false
    },
    icon: {
      type: String,
      required: false,
      default: ''
    }
  }
});
// CONCATENATED MODULE: ./components/button.vue?vue&type=script&lang=js
 /* harmony default export */ var components_buttonvue_type_script_lang_js = (buttonvue_type_script_lang_js); 
// EXTERNAL MODULE: ./node_modules/vue-loader/lib/runtime/componentNormalizer.js
var componentNormalizer = __webpack_require__(3);

// CONCATENATED MODULE: ./components/button.vue



function injectStyles (context) {
  
  var style0 = __webpack_require__(73)
if (style0.__inject__) style0.__inject__(context)

}

/* normalize component */

var component = Object(componentNormalizer["a" /* default */])(
  components_buttonvue_type_script_lang_js,
  render,
  staticRenderFns,
  false,
  injectStyles,
  "050195de",
  "655d3987"
  
)

/* harmony default export */ var components_button = __webpack_exports__["default"] = (component.exports);

/* nuxt-component-imports */
installComponents(component, {Spinner: __webpack_require__(72).default,Button: __webpack_require__(70).default})


/***/ }),

/***/ 71:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(74);
if(content.__esModule) content = content.default;
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add CSS to SSR context
var add = __webpack_require__(6).default
module.exports.__inject__ = function (context) {
  add("72f4a40c", content, true, context)
};

/***/ }),

/***/ 72:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
// ESM COMPAT FLAG
__webpack_require__.r(__webpack_exports__);

// CONCATENATED MODULE: ./node_modules/babel-loader/lib??ref--2-0!./node_modules/vue-loader/lib/loaders/templateLoader.js??ref--6!./node_modules/@nuxt/components/dist/loader.js??ref--0-0!./node_modules/vue-loader/lib??vue-loader-options!./components/Spinner.vue?vue&type=template&id=327186fa
var render = function render() {
  var _vm = this,
    _c = _vm._self._c;
  return _c('svg', {
    staticStyle: {
      "shape-rendering": "auto"
    },
    attrs: {
      "xmlns": "http://www.w3.org/2000/svg",
      "width": _vm.width,
      "height": _vm.height,
      "viewBox": "0 0 100 100",
      "preserveAspectRatio": "xMidYMid"
    }
  }, [_vm._ssrNode("<path d=\"M10 50A40 40 0 0 0 90 50A40 42 0 0 1 10 50\"" + _vm._ssrAttr("stroke", _vm.stroke) + _vm._ssrAttr("fill", _vm.fill) + " stroke-width=\"5px\">", "</path>", [_c('animateTransform', {
    attrs: {
      "attributeName": "transform",
      "type": "rotate",
      "dur": "1s",
      "repeatCount": "indefinite",
      "keyTimes": "0;1",
      "values": "0 50 51;360 50 51"
    }
  })], 1)]);
};
var staticRenderFns = [];

// CONCATENATED MODULE: ./components/Spinner.vue?vue&type=template&id=327186fa

// CONCATENATED MODULE: ./node_modules/babel-loader/lib??ref--2-0!./node_modules/@nuxt/components/dist/loader.js??ref--0-0!./node_modules/vue-loader/lib??vue-loader-options!./components/Spinner.vue?vue&type=script&lang=js
/* harmony default export */ var Spinnervue_type_script_lang_js = ({
  props: {
    width: {
      required: false,
      type: String,
      default: () => '20px'
    },
    height: {
      required: false,
      type: String,
      default: () => '20px'
    },
    stroke: {
      required: false,
      type: String,
      default: () => '#000000'
    },
    fill: {
      required: false,
      type: String,
      default: () => '#000000'
    }
  }
});
// CONCATENATED MODULE: ./components/Spinner.vue?vue&type=script&lang=js
 /* harmony default export */ var components_Spinnervue_type_script_lang_js = (Spinnervue_type_script_lang_js); 
// EXTERNAL MODULE: ./node_modules/vue-loader/lib/runtime/componentNormalizer.js
var componentNormalizer = __webpack_require__(3);

// CONCATENATED MODULE: ./components/Spinner.vue





/* normalize component */

var component = Object(componentNormalizer["a" /* default */])(
  components_Spinnervue_type_script_lang_js,
  render,
  staticRenderFns,
  false,
  null,
  null,
  "8ecfb218"
  
)

/* harmony default export */ var Spinner = __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ 73:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_style_loader_index_js_ref_7_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_7_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_dist_cjs_js_ref_7_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_7_oneOf_1_3_node_modules_nuxt_components_dist_loader_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_button_vue_vue_type_style_index_0_id_050195de_prod_lang_scss_scoped_true__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(71);
/* harmony import */ var _node_modules_vue_style_loader_index_js_ref_7_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_7_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_dist_cjs_js_ref_7_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_7_oneOf_1_3_node_modules_nuxt_components_dist_loader_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_button_vue_vue_type_style_index_0_id_050195de_prod_lang_scss_scoped_true__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_vue_style_loader_index_js_ref_7_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_7_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_dist_cjs_js_ref_7_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_7_oneOf_1_3_node_modules_nuxt_components_dist_loader_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_button_vue_vue_type_style_index_0_id_050195de_prod_lang_scss_scoped_true__WEBPACK_IMPORTED_MODULE_0__);
/* harmony reexport (unknown) */ for(var __WEBPACK_IMPORT_KEY__ in _node_modules_vue_style_loader_index_js_ref_7_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_7_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_dist_cjs_js_ref_7_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_7_oneOf_1_3_node_modules_nuxt_components_dist_loader_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_button_vue_vue_type_style_index_0_id_050195de_prod_lang_scss_scoped_true__WEBPACK_IMPORTED_MODULE_0__) if(["default"].indexOf(__WEBPACK_IMPORT_KEY__) < 0) (function(key) { __webpack_require__.d(__webpack_exports__, key, function() { return _node_modules_vue_style_loader_index_js_ref_7_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_7_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_dist_cjs_js_ref_7_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_7_oneOf_1_3_node_modules_nuxt_components_dist_loader_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_button_vue_vue_type_style_index_0_id_050195de_prod_lang_scss_scoped_true__WEBPACK_IMPORTED_MODULE_0__[key]; }) }(__WEBPACK_IMPORT_KEY__));


/***/ }),

/***/ 74:
/***/ (function(module, exports, __webpack_require__) {

// Imports
var ___CSS_LOADER_API_IMPORT___ = __webpack_require__(5);
var ___CSS_LOADER_EXPORT___ = ___CSS_LOADER_API_IMPORT___(function(i){return i[1]});
// Module
___CSS_LOADER_EXPORT___.push([module.i, ".dk-btn[data-v-050195de],.dk-btn--black[data-v-050195de],.dk-btn--black__outline[data-v-050195de],.dk-btn--blue[data-v-050195de],.dk-btn--danger[data-v-050195de],.dk-btn--empty[data-v-050195de],.dk-btn--empty__borderless[data-v-050195de],.dk-btn--primary[data-v-050195de],.dk-btn--primary__outline[data-v-050195de],.dk-btn--secondary[data-v-050195de],.dk-btn--secondary__outline[data-v-050195de],.dk-btn--sm[data-v-050195de],.dk-btn--sm--blue[data-v-050195de],.dk-btn--sm--blue-dark[data-v-050195de],.dk-btn--sm--danger[data-v-050195de],.dk-btn--sm--dark-green[data-v-050195de],.dk-btn--sm--dark-grey[data-v-050195de],.dk-btn--sm--grey[data-v-050195de],.dk-btn--sm--info[data-v-050195de],.dk-btn--sm--orange[data-v-050195de],.dk-btn--sm--pink[data-v-050195de],.dk-btn--sm--purple[data-v-050195de],.dk-btn--sm--success[data-v-050195de],.dk-btn--sm--success-dark[data-v-050195de],.dk-btn--sm--warning[data-v-050195de],.dk-btn--success[data-v-050195de],.dk-btn--warning[data-v-050195de]{border:1px solid transparent;border-radius:5px;display:inline-block;font-size:14px;min-width:120px;outline:none!important;padding:16px;text-align:center}.disabled.dk-btn--black[data-v-050195de],.disabled.dk-btn--black__outline[data-v-050195de],.disabled.dk-btn--blue[data-v-050195de],.disabled.dk-btn--danger[data-v-050195de],.disabled.dk-btn--empty[data-v-050195de],.disabled.dk-btn--empty__borderless[data-v-050195de],.disabled.dk-btn--primary[data-v-050195de],.disabled.dk-btn--primary__outline[data-v-050195de],.disabled.dk-btn--secondary[data-v-050195de],.disabled.dk-btn--secondary__outline[data-v-050195de],.disabled.dk-btn--sm[data-v-050195de],.disabled.dk-btn--sm--blue[data-v-050195de],.disabled.dk-btn--sm--blue-dark[data-v-050195de],.disabled.dk-btn--sm--danger[data-v-050195de],.disabled.dk-btn--sm--dark-green[data-v-050195de],.disabled.dk-btn--sm--dark-grey[data-v-050195de],.disabled.dk-btn--sm--grey[data-v-050195de],.disabled.dk-btn--sm--info[data-v-050195de],.disabled.dk-btn--sm--orange[data-v-050195de],.disabled.dk-btn--sm--pink[data-v-050195de],.disabled.dk-btn--sm--purple[data-v-050195de],.disabled.dk-btn--sm--success[data-v-050195de],.disabled.dk-btn--sm--success-dark[data-v-050195de],.disabled.dk-btn--sm--warning[data-v-050195de],.disabled.dk-btn--success[data-v-050195de],.disabled.dk-btn--warning[data-v-050195de],.dk-btn.disabled[data-v-050195de]{opacity:.5;pointer-events:none}.dk-btn--sm[data-v-050195de],.dk-btn--sm--blue[data-v-050195de],.dk-btn--sm--blue-dark[data-v-050195de],.dk-btn--sm--danger[data-v-050195de],.dk-btn--sm--dark-green[data-v-050195de],.dk-btn--sm--dark-grey[data-v-050195de],.dk-btn--sm--grey[data-v-050195de],.dk-btn--sm--info[data-v-050195de],.dk-btn--sm--orange[data-v-050195de],.dk-btn--sm--pink[data-v-050195de],.dk-btn--sm--purple[data-v-050195de],.dk-btn--sm--success[data-v-050195de],.dk-btn--sm--success-dark[data-v-050195de],.dk-btn--sm--warning[data-v-050195de]{border-radius:3px;min-width:81px;padding:5px 16px}@media screen and (max-width:1680px){.dk-btn--sm[data-v-050195de],.dk-btn--sm--blue[data-v-050195de],.dk-btn--sm--blue-dark[data-v-050195de],.dk-btn--sm--danger[data-v-050195de],.dk-btn--sm--dark-green[data-v-050195de],.dk-btn--sm--dark-grey[data-v-050195de],.dk-btn--sm--grey[data-v-050195de],.dk-btn--sm--info[data-v-050195de],.dk-btn--sm--orange[data-v-050195de],.dk-btn--sm--pink[data-v-050195de],.dk-btn--sm--purple[data-v-050195de],.dk-btn--sm--success[data-v-050195de],.dk-btn--sm--success-dark[data-v-050195de],.dk-btn--sm--warning[data-v-050195de]{min-width:71px;padding:5px 10px}}.dk-btn--sm--info[data-v-050195de]{background-color:var(--app-secondary-color);border:1px solid var(--app-secondary-color);color:var(--color-white)}.dk-btn--sm--success[data-v-050195de]{background-color:var(--color-success);border:1px solid var(--color-success);color:var(--color-white)}.dk-btn--sm--purple[data-v-050195de]{background-color:#6e36da;border:1px solid #6e36da;color:var(--color-white)}.dk-btn--sm--dark-green[data-v-050195de]{background-color:#1f9d8a;border:1px solid #1f9d8a;color:var(--color-white)}.dk-btn--sm--success-dark[data-v-050195de]{background-color:#2e9615;border:1px solid #2e9615;color:var(--color-white)}.dk-btn--sm--warning[data-v-050195de]{background-color:var(--color-warning);border:1px solid var(--color-warning);color:var(--color-white)}.dk-btn--sm--grey[data-v-050195de]{background-color:#d2d8eb;border:1px solid #d2d8eb;color:var(--color-dark)}.dk-btn--sm--dark-grey[data-v-050195de]{background-color:var(--color-dark);border:1px solid var(--color-dark);color:var(--color-white)}.dk-btn--sm--pink[data-v-050195de]{background-color:#ff2478;border:1px solid #ff2478;color:var(--color-white)}.dk-btn--sm--orange[data-v-050195de]{background-color:#e8541d;border:1px solid #e8541d;color:var(--color-white)}.dk-btn--sm--blue[data-v-050195de],.dk-btn--sm--blue-dark[data-v-050195de]{background-color:#2c77ff;border:1px solid #2c77ff;color:var(--color-white)}.dk-btn--sm--danger[data-v-050195de]{background-color:red;border:1px solid red;color:var(--color-white)}.dk-btn--secondary[data-v-050195de]{background-color:var(--app-secondary-color);border:none;color:var(--color-white)}@media(hover:hover){.dk-btn--secondary[data-v-050195de]:hover{background-color:var(--app-secondary-color);color:var(--color-white);-webkit-text-decoration:none;text-decoration:none}}.dk-btn--secondary__outline[data-v-050195de]{background-color:var(--color-white);border-color:var(--app-secondary-color);color:var(--app-secondary-color);-webkit-text-decoration:none;text-decoration:none}@media(hover:hover){.dk-btn--secondary__outline[data-v-050195de]:hover{background-color:var(--app-secondary-color);color:var(--color-white);-webkit-text-decoration:none;text-decoration:none}}.dk-btn--primary[data-v-050195de]{background-color:var(--color-primary);border-color:var(--color-primary);color:var(--color-white);-webkit-text-decoration:none;text-decoration:none}@media(hover:hover){.dk-btn--primary[data-v-050195de]:hover{background-color:var(--color-primary);color:var(--color-white);-webkit-text-decoration:none;text-decoration:none}}.dk-btn--primary__outline[data-v-050195de]{background-color:var(--color-white);border-color:var(--color-primary);color:var(--color-primary);-webkit-text-decoration:none;text-decoration:none}@media(hover:hover){.dk-btn--primary__outline[data-v-050195de]:hover{background-color:var(--color-primary);color:var(--color-white);-webkit-text-decoration:none;text-decoration:none}}.dk-btn--success[data-v-050195de]{background-color:var(--color-success);border-color:var(--color-success);color:var(--color-white)}@media(hover:hover){.dk-btn--success[data-v-050195de]:hover{background-color:var(--color-success);border-color:var(--color-success);color:var(--color-white)}}.dk-btn--blue[data-v-050195de]{background-color:var(--color-primary-variant);border-color:var(--color-primary-variant);color:var(--color-white)}@media(hover:hover){.dk-btn--blue[data-v-050195de]:hover{background-color:var(--color-primary-variant);border-color:var(--color-primary-variant);color:var(--color-white)}}.dk-btn--danger[data-v-050195de]{background-color:var(--color-danger);border-color:var(--color-danger);color:var(--color-white)}@media(hover:hover){.dk-btn--danger[data-v-050195de]:hover{background-color:var(--color-danger);border-color:var(--color-danger);color:var(--color-white)}}.dk-btn--warning[data-v-050195de]{background-color:var(--color-warning);border-color:var(--color-warning);color:var(--color-white)}@media(hover:hover){.dk-btn--warning[data-v-050195de]:hover{background-color:var(--color-warning);border-color:var(--color-warning);color:var(--color-white)}}.dk-btn--black[data-v-050195de]{background-color:var(--color-dark);color:var(--color-white)}@media(hover:hover){.dk-btn--black[data-v-050195de]:hover{background-color:var(--color-dark);color:var(--color-white)}}.dk-btn--black__outline[data-v-050195de]{background-color:var(--color-white);border-color:var(--color-dark);color:var(--color-dark)}.dk-btn--empty[data-v-050195de],.dk-btn--empty[data-v-050195de]:hover,.dk-btn--empty__borderless[data-v-050195de]{background-color:transparent;color:var(--color-dark)}.dk-btn--empty__borderless[data-v-050195de]{border-width:0}", ""]);
// Exports
___CSS_LOADER_EXPORT___.locals = {};
module.exports = ___CSS_LOADER_EXPORT___;


/***/ })

};;
//# sourceMappingURL=19.js.map