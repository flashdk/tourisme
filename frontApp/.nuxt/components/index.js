export const Spinner = () => import('../../components/Spinner.vue' /* webpackChunkName: "components/spinner" */).then(c => wrapFunctional(c.default || c))
export const AddTag = () => import('../../components/addTag.vue' /* webpackChunkName: "components/add-tag" */).then(c => wrapFunctional(c.default || c))
export const Avatar = () => import('../../components/avatar.vue' /* webpackChunkName: "components/avatar" */).then(c => wrapFunctional(c.default || c))
export const Button = () => import('../../components/button.vue' /* webpackChunkName: "components/button" */).then(c => wrapFunctional(c.default || c))
export const CustomMultiSelecte = () => import('../../components/customMultiSelecte.vue' /* webpackChunkName: "components/custom-multi-selecte" */).then(c => wrapFunctional(c.default || c))
export const DatePiker = () => import('../../components/datePiker.vue' /* webpackChunkName: "components/date-piker" */).then(c => wrapFunctional(c.default || c))
export const ParcoursCard = () => import('../../components/parcoursCard.vue' /* webpackChunkName: "components/parcours-card" */).then(c => wrapFunctional(c.default || c))
export const UploadFile = () => import('../../components/uploadFile.vue' /* webpackChunkName: "components/upload-file" */).then(c => wrapFunctional(c.default || c))
export const IconsAddIcon = () => import('../../components/icons/AddIcon.vue' /* webpackChunkName: "components/icons-add-icon" */).then(c => wrapFunctional(c.default || c))
export const SimpleTable = () => import('../../components/simpleTable/index.vue' /* webpackChunkName: "components/simple-table" */).then(c => wrapFunctional(c.default || c))
export const SimpleTableRow = () => import('../../components/simpleTable/tableRow.vue' /* webpackChunkName: "components/simple-table-row" */).then(c => wrapFunctional(c.default || c))
export const Modal = () => import('../../components/modal/index.vue' /* webpackChunkName: "components/modal" */).then(c => wrapFunctional(c.default || c))
export const SimpleTableDataTypesTabCelTag = () => import('../../components/simpleTable/dataTypes/tabCelTag.vue' /* webpackChunkName: "components/simple-table-data-types-tab-cel-tag" */).then(c => wrapFunctional(c.default || c))
export const SimpleTableDataTypesTabCelText = () => import('../../components/simpleTable/dataTypes/tabCelText.vue' /* webpackChunkName: "components/simple-table-data-types-tab-cel-text" */).then(c => wrapFunctional(c.default || c))

// nuxt/nuxt.js#8607
function wrapFunctional(options) {
  if (!options || !options.functional) {
    return options
  }

  const propKeys = Array.isArray(options.props) ? options.props : Object.keys(options.props || {})

  return {
    render(h) {
      const attrs = {}
      const props = {}

      for (const key in this.$attrs) {
        if (propKeys.includes(key)) {
          props[key] = this.$attrs[key]
        } else {
          attrs[key] = this.$attrs[key]
        }
      }

      return h(options, {
        on: this.$listeners,
        attrs,
        props,
        scopedSlots: this.$scopedSlots,
      }, this.$slots.default)
    }
  }
}
