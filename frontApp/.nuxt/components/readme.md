# Discovered Components

This is an auto-generated list of components discovered by [nuxt/components](https://github.com/nuxt/components).

You can directly use them in pages and other components without the need to import them.

**Tip:** If a component is conditionally rendered with `v-if` and is big, it is better to use `Lazy` or `lazy-` prefix to lazy load.

- `<Spinner>` | `<spinner>` (components/Spinner.vue)
- `<AddTag>` | `<add-tag>` (components/addTag.vue)
- `<Avatar>` | `<avatar>` (components/avatar.vue)
- `<Button>` | `<button>` (components/button.vue)
- `<CustomMultiSelecte>` | `<custom-multi-selecte>` (components/customMultiSelecte.vue)
- `<DatePiker>` | `<date-piker>` (components/datePiker.vue)
- `<ParcoursCard>` | `<parcours-card>` (components/parcoursCard.vue)
- `<UploadFile>` | `<upload-file>` (components/uploadFile.vue)
- `<IconsAddIcon>` | `<icons-add-icon>` (components/icons/AddIcon.vue)
- `<SimpleTable>` | `<simple-table>` (components/simpleTable/index.vue)
- `<SimpleTableRow>` | `<simple-table-row>` (components/simpleTable/tableRow.vue)
- `<Modal>` | `<modal>` (components/modal/index.vue)
- `<SimpleTableDataTypesTabCelTag>` | `<simple-table-data-types-tab-cel-tag>` (components/simpleTable/dataTypes/tabCelTag.vue)
- `<SimpleTableDataTypesTabCelText>` | `<simple-table-data-types-tab-cel-text>` (components/simpleTable/dataTypes/tabCelText.vue)
