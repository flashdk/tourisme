export const Constants = {
  COMPONENT_OPTIONS_KEY: "nuxtI18n",
  STRATEGIES: {"PREFIX":"prefix","PREFIX_EXCEPT_DEFAULT":"prefix_except_default","PREFIX_AND_DEFAULT":"prefix_and_default","NO_PREFIX":"no_prefix"},
  REDIRECT_ON_OPTIONS: {"ALL":"all","ROOT":"root","NO_PREFIX":"no prefix"},
}
export const nuxtOptions = {
  isUniversalMode: true,
  trailingSlash: undefined,
}
export const options = {
  vueI18n: {"fallbackLocale":"en","messages":{"en":{"menu":{"user":"users","dashboard":"Dashboard"},"text":{"bonjour":"hello everybody","lookup":"Boom en","serviceIsUsedOnStep":"This service is attached to one or more stages of a journey. Please delete them to continue.","confirmServiceDel":"The service is successfully removed"}},"fr":{"menu":{"user":"utilisateurs","dashboard":"Tableau de board"},"text":{"bonjour":"Bonjour tout le monde 12","lookup":"Boom fr","serviceIsUsedOnStep":"Ce service est rattaché à un ou plusieurs étapes d'un parcours. Veuillez les supprimer pour continuer.","confirmServiceDel":"Le service est supprimé avec succes"}}}},
  vueI18nLoader: false,
  locales: [{"code":"en","iso":"en-US","name":"English"},{"code":"fr","iso":"fr-FR","name":"Français"}],
  defaultLocale: "fr",
  defaultDirection: "ltr",
  routesNameSeparator: "___",
  defaultLocaleRouteNameSuffix: "default",
  sortRoutes: true,
  strategy: "prefix_except_default",
  lazy: false,
  langDir: null,
  rootRedirect: null,
  detectBrowserLanguage: {"alwaysRedirect":false,"cookieAge":365,"cookieCrossOrigin":false,"cookieDomain":null,"cookieKey":"i18n_redirected","cookieSecure":false,"fallbackLocale":"","redirectOn":"root","useCookie":true},
  differentDomains: false,
  baseUrl: "",
  vuex: {"moduleName":"i18n","syncRouteParams":true},
  parsePages: true,
  pages: {},
  skipSettingLocaleOnNavigate: false,
  onBeforeLanguageSwitch: () => {},
  onLanguageSwitched: () => null,
  normalizedLocales: [{"code":"en","iso":"en-US","name":"English"},{"code":"fr","iso":"fr-FR","name":"Français"}],
  localeCodes: ["en","fr"],
  additionalMessages: [],
}
export const localeFiles = {
  0: "{",
  1: "}",
}

export const localeMessages = {}
