export default ($axios) => {
  return {
    addFile: async (params) => {
      // console.log('adddddd', params)
      return await $axios.post('/uploadFile', params, {
        headers: {
          'Content-Type': 'multipart/form-data'
        }
      }).then(res => res.data.data)
    }

    // deleteUser: async (params) => {
    //   return await $axios.delete(`/users/${params._id}`, {})
    // }
  }
}
