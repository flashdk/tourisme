export default ($axios) => {
  return {
    getUser: async (params) => {
      return await $axios.get('/users', { params })
    },

    authUser: async (data) => {
      return await $axios.post('/auth', data)
    }
  }
}
