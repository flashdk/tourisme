export default ($axios) => {
  return {
    getSteps: (params) => {
      return $axios.get('/steps', { params }).then(res => res.data)
    },

    getStep: (params) => {
      return $axios.get('/steps', { params }).then(res => res.data)
    },

    addStep: async (params) => {
      console.log('patam', params)
      return await $axios.post('/steps', params).then(res => res.data)
    },

    updateStep: async (params, id) => {
      return await $axios.put(`/steps/${id}`, params).then(res => res.data)
    },

    deleteStep: async (params) => {
      return await $axios.delete(`/steps/${params._id}`, {})
    }
  }
}
