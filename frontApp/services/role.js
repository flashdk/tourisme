export default ($axios) => {
  return {
    getRoles: async (params) => {
      return await $axios.get('/roles', { params })
        .then(res => res.data.data)
        .catch(error => error)
    },

    getRole: async (params) => {
      return await $axios.get('/roles', { params })
        .then(res => res.data)
        .catch(error => error)
    },

    addRole: async (data) => {
      return await $axios.get('/roles', { data })
        .then(res => res.data)
        .catch(error => error)
    },

    updatRole: async (data) => {
      return await $axios.put('/roles', { data })
        .then(res => res.data)
        .catch(error => error)/*  */
    }
  }
}
