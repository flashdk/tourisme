export default ($axios) => {
  return {
    getCourses: (params) => {
      return $axios.get('/parcours', { params, cache: false }).then(res => res.data)
    },

    getCourse: (params) => {
      return $axios.get('/parcours', { params }).then(res => res.data)
    },

    addCourse: async (params) => {
      return await $axios.post('/parcours', params).then(res => res.data)
    },

    updateCourse: async (params, id) => {
      return await $axios.put(`/parcours/${id}`, params, {
        headers: {
          'Content-Type': 'multipart/form-data'
        }
      }).then(res => res.data)
    },

    deleteCourse: async (params) => {
      return await $axios.delete(`/parcours/${params._id}`, {})
    }
  }
}
