export default ($axios) => {
  return {
    getServices: async (params) => {
      return await $axios.get('/Services', { params, cache: false })
        .then(res => res.data.data)
        .catch(error => error)
    },

    getService: async (params) => {
      return await $axios.get('/Services', { params })
        .then(res => res.data)
        .catch(error => error)
    },

    addService: async (data) => {
      return await $axios.post('/Services', { ...data })
        .then(res => res.data)
        .catch(error => error.response.data)
    },

    updatService: async (data) => {
      return await $axios.put('/Services', { ...data })
        .then(res => res.data)
        .catch(error => error)/*  */
    },

    deleteService: async (data) => {
      return await $axios.delete(`/Services/${data._id}?createdBy=${data.createdBy}`, {})
        .then(res => res.data)
        .catch(error => error.response.data)
    }
  }
}
