export default ($axios) => {
  return {
    getUsers: (params) => {
      return $axios.get('/users', { params }).then(res => res.data)
    },

    getUser: (params) => {
      return $axios.get('/users', { params }).then(res => res.data)
    },

    addUser: async (params) => {
      return await $axios.post('/users', params, {
        headers: {
          'Content-Type': 'multipart/form-data'
        }
      })
    },

    updateUser: async (params, id) => {
      return await $axios.put(`/users/${id}`, params, {
        headers: {
          'Content-Type': 'multipart/form-data'
        }
      }).then(res => res.data)
    },

    deleteUser: async (params) => {
      return await $axios.delete(`/users/${params._id}`, {})
        .catch(error => error)
    }
  }
}
