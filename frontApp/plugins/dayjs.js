import * as dayjs from 'dayjs'

export default function (_, inject) {
  inject('dayjs', dayjs)
}
