import Vue from 'vue'
// import createStringGenerator from 'crypto-random-string'
import debounce from 'lodash.debounce'
// import sanitizeHtml from 'sanitize-html'

// const sanitizeOptions = {
//   allowedTags: sanitizeHtml.defaults.allowedTags.concat(['img']),
//   allowedAttributes: {
//     '*': ['class', 'style', 'height', 'width', 'href', 'name', 'title', 'srcset', 'target', 'src', 'loading']
//   }
// }

// const removeTagsOptions = {
//   allowedTags: [],
//   allowedAttributes: {
//     '*': ['class', 'style', 'height', 'width', 'href', 'name', 'title', 'srcset', 'target', 'src', 'loading']
//   }
// }

const App = {
  install (Vue) {
    Vue.mixin({
      computed: {
        $locale () {
          return this.$store.state.locale
        }
      },
      methods: {
        // $sanitizeHtml (text) {
        //   return sanitizeHtml(text, sanitizeOptions)
        // },
        // $removeHtml (text) {
        //   return sanitizeHtml(text, removeTagsOptions)
        // },
        // $random (opts) {
        //   return createStringGenerator({ length: 16, ...opts })
        // },
        // $cryptoRandomString (opts) {
        //   return createStringGenerator({ length: 16, ...opts })
        // },
        $debounce (fn, time) {
          return debounce(fn, time)
        },
        $isEmptyObject (obj) {
          return Object.keys(obj).length === 0
        },
        $dateControl (startDate, endDate) {
          return new Date(startDate).getTime() <= new Date(endDate).getTime()
        }
      }
    })
  }
}

Vue.use(App)

export default App
