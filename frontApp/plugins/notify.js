import Vue from 'vue'

import iziToast from 'izitoast'
import 'izitoast/dist/css/iziToast.min.css'

Vue.prototype.$notify = function (options = {}) {
  if (options.type === 'error' || options.type === 'warning') {
    iziToast.warning({
      ...options,
      position: 'bottomCenter'
    })
  } else {
    iziToast.success({
      ...options,
      position: 'bottomCenter'
    })
  }
}
