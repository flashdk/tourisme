import AuthService from '@/services/auth'
import RoleService from '@/services/role'
import UserService from '@/services/users'
import CourseService from '@/services/course'
import StepsService from '@/services/steps'
import serviceService from '@/services/service'
import UploadFile from '@/services/uploadFile'

export default ({ $axios }, inject) => {
  const allMethods = {
    ...AuthService($axios),
    ...RoleService($axios),
    ...UserService($axios),
    ...CourseService($axios),
    ...UploadFile($axios),
    ...StepsService($axios),
    ...serviceService($axios)

    // import another service here
  }
  const methods = Object.keys(allMethods)
  methods.forEach((method) => {
    inject(method, allMethods[method])
  })
}
