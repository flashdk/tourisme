import Vue from 'vue'
import { localize, setInteractionMode, ValidationObserver, ValidationProvider, extend } from 'vee-validate'
import { required, required_if, email, oneOf, min, max, min_value as minValue, max_value as maxValue } from 'vee-validate/dist/rules'
import isURL from 'validator/lib/isURL'
import isEmail from 'validator/lib/isEmail'
// import en from 'vee-validate/dist/locale/en.json'
// import fr from 'vee-validate/dist/locale/fr.json'
// import bannedEmail from '@/helpers/bannedEmail'

setInteractionMode('eager')

setInteractionMode('betterEager', ({ errors }) => {
  // become slightly aggressive if the field is invalid.

  if (errors.length) {
    return {
      on: ['change'],
      debounce: 350
    }
  }

  // validate immediately after leaving the field.
  return {
    on: ['change'],
    debounce: 0
  }
})

// Override the default message.
extend('required', required)
extend('required_if', required_if)

extend('oneOf', oneOf)

extend('min', min)

extend('w-min-value', minValue)
extend('w-max-value', maxValue)

extend('max', max)

// extend('validAccountEmail', {
//   validate: (value, args) => {
//     const ok = email.validate(value, args)
//     if (ok) {
//       const domain = value.split('@')[1]
//       return !bannedEmail.includes(domain)
//     } else {
//       return false
//     }
//   },
//   params: [
//     {
//       name: 'multiple',
//       default: false
//     }
//   ]
// })

// extend('email', {
//   validate: (value, args) => {
//     const ok = email.validate(value, args)

//     if (ok) {
//       const domain = value.split('@')[1]
//       return !bannedEmail.includes(domain)
//     } else {
//       return false
//     }
//   },
//   params: [
//     {
//       name: 'multiple',
//       default: false
//     }
//   ]
// })

extend('min-array-length', {
  validate: (value, args) => {
    const min = args.min
    return {
      required: true,
      valid: (Array.isArray(value) && value.length >= min) || false
    }
  },
  params: ['min']
})

extend('email', {
  ...email,
  message: 'This field must be a valid email'
})

extend('url', {
  validate: (value) => {
    return {
      valid: isEmail(value) ? false : isURL(value)
    }
  },
  params: ['min']
})

extend('notYopmail', {
  validate: (value) => {
    return {
      valid: isEmail(value) && !value.split('@')[1].trim().toLowerCase().startsWith('yopmail')
    }
  },
  params: ['min']
})

extend('min-date', {
  validate: (value, args) => {
    const min = args.min

    return new Date(value).getTime() >= new Date(min).getTime()
  },
  params: ['min']
})

extend('max-date', {
  validate: (value, args) => {
    const max = args.max
    return {
      required: true,
      valid: value <= max || false
    }
  },
  params: ['max']
})

extend('file-required', {
  params: [
    {
      name: 'allowFalse',
      default: true
    }
  ],
  computesRequired: true,
  validate (value, _a) {
    let ok = true
    if (!value || (value && (!value.location || !value.name))) {
      ok = false
    }
    return ok
  }
})

extend('max-array-length', {
  validate: (value, args) => {
    const max = args.max
    return {
      required: true,
      valid: (Array.isArray(value) && value.length <= max) || false
    }
  },
  params: ['max']
})

// localize({
//   en: {
//     messages: {
//       ...en.messages,
//       'file-required': 'This field is required',
//       required: 'This field is required',
//       email: 'This field is invalid email',
//       validAccountEmail: 'This field is invalid email',
//       'min-date': (_, obj) => `This field must be greater than  ${obj.min ? obj.min : ''}`,
//       'max-date': (_, obj) => `This field must be smaller than ${obj.max ? obj.max : ''}`,
//       'min-array-length': 'This field requiered at least {min} choice(s)',
//       'max-array-length': 'This field requiered a maximum of {max} choice(s)',
//       'w-min-value': (_, obj) => `This field must be greater than ${obj.min}`,
//       'w-max-value': (_, obj) => `This field must be smaller than ${obj.max}`,
//       url: 'This field is not a valid url',
//       min: (_, obj) => `This field must be greater than ${obj.min}`,
//       max: (_, obj) => `This field must be smaller than ${obj.max}`
//     }
//   },
//   fr: {
//     messages: {
//       ...fr.messages,
//       'file-required': 'Ce champ est obligatoire',
//       required: 'Ce champ est obligatoire',
//       email: () => 'adresse email invalide',
//       validAccountEmail: () => 'adresse email invalide',
//       'min-date': (_, obj) => `Ce champ doit être plus grand que  ${obj.min ? obj.min : ''}`,
//       'max-date': (_, obj) => `Ce champ doit  être plus petit que ${obj.max ? obj.max : ''}`,
//       'min-array-length': 'Ce champ necessite au minimum {min} choix',
//       'max-array-length': 'Ce champ necessite au maximum {max} choix',
//       'w-min-value': (_, obj) => `Ce champ doit être plus grand que ${obj.min}`,
//       'w-max-value': (_, obj) => `Ce champ doit être plus petit que ${obj.max}`,
//       url: 'Ce champ doit être un lien valide',
//       min: (_, obj) => `Ce champ doit être plus grand que ${obj.min}`,
//       max: (_, obj) => `Ce champ doit être plus petit que ${obj.max}`

//     }
//   }
// })

Vue.component('ValidationObserver', ValidationObserver)
Vue.component('ValidationProvider', ValidationProvider)
