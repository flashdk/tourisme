module.exports = {
  menu: {
    user: 'users',
    dashboard: 'Dashboard'
  },
  text: {
    bonjour: 'hello everybody',
    lookup: 'Boom en',
    serviceIsUsedOnStep: 'This service is attached to one or more stages of a journey. Please delete them to continue.',
    confirmServiceDel: 'The service is successfully removed'
  }
}
