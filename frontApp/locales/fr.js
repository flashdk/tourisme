module.exports = {
  menu: {
    user: 'utilisateurs',
    dashboard: 'Tableau de board'
  },
  text: {
    bonjour: 'Bonjour tout le monde 12',
    lookup: 'Boom fr',
    serviceIsUsedOnStep: 'Ce service est rattaché à un ou plusieurs étapes d\'un parcours. Veuillez les supprimer pour continuer.',
    confirmServiceDel: 'Le service est supprimé avec succes'
  }
}
