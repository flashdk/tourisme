import Vue from 'vue'
import Router from 'vue-router'

const AuthPage = () => import('~/pages/Authentification/UserAuth').then(m => m.default || m)
const AdminDashboard = () => import('~/pages').then(m => m.default || m)
const UserRole = () => import('~/pages/Admin/roles').then(m => m.default || m)
const Users = () => import('~/pages/Admin/users').then(m => m.default || m)
const Course = () => import('~/pages/Admin/course').then(m => m.default || m)
const Services = () => import('~/pages/Admin/services').then(m => m.default || m)
const PublicHomePage = () => import('~/pages/public/home').then(m => m.default || m)

Vue.use(Router)

export function createRouter () {
  return new Router({
    mode: 'history',
    routes: [
      {
        path: '/:locale/auth/:selectedPage',
        name: 'auth',
        component: AuthPage
      },
      {
        path: '/',
        name: 'PublicHomePage',
        component: PublicHomePage
      },
      {
        path: '/:locale',
        name: 'PublicHomePage',
        component: PublicHomePage
      },
      {
        path: '/:locale/admin/dashboard',
        name: 'admin-dashboard',
        component: AdminDashboard
      },
      {
        path: '/:locale/admin/roles',
        name: 'user-role',
        component: UserRole
      },
      {
        path: '/:locale/admin/users',
        name: 'users',
        component: Users
      },
      {
        path: '/:locale/admin/course',
        name: 'course',
        component: Course
      },
      {
        path: '/:locale/admin/services',
        name: 'services',
        component: Services
      }
    ]
  })
}
