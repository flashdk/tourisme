export default {
  // Global page headers: https://go.nuxtjs.dev/config-head
  head: {
    title: 'africaTourismFront',
    htmlAttrs: {
      lang: 'fr'
    },
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: '' },
      { name: 'format-detection', content: 'telephone=no' }
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' },
      { rel: 'stylesheet', href: 'https://fonts.googleapis.com/icon?family=Material+Icons' },
      { rel: 'stylesheet', href: 'https://fonts.googleapis.com/css2?family=Material+Symbols+Outlined:opsz,wght,FILL,GRAD@48,400,0,0' },
      { rel: 'stylesheet', href: 'https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.2.0/css/all.min.css' }
    ]
  },

  server: {
    port: 3000, // par défaut: 3000
    host: '0.0.0.0', // par défaut: localhost
  },

  // Global CSS: https://go.nuxtjs.dev/config-css
  css: [
    '@/assets/sass/main.scss'
  ],

  // Plugins to run before rendering page: https://go.nuxtjs.dev/config-plugins
  plugins: [
    '~/plugins/dayjs.js',
    '~/plugins/service.js',
    '~/plugins/mixins.js',
    { src: '~/plugins/notify.js', ssr: false },
    { src: '~/plugins/vee-validate.js', ssr: false }
  ],

  // Auto import components: https://go.nuxtjs.dev/config-components
  components: true,

  // publicRuntimeConfig: {
  //   supportedLangs: ['fr', 'en']
  // },

  // Modules for dev and build (recommended): https://go.nuxtjs.dev/config-modules
  buildModules: [
    // https://go.nuxtjs.dev/typescript
    '@nuxt/typescript-build',
    '@nuxtjs/dotenv',
    ['@nuxtjs/router',
      {
        keepDefaultRouter: false,
        filename: 'router.js'
      }
    ]
  ],

  // Modules: https://go.nuxtjs.dev/config-modules
  modules: [
    // https://go.nuxtjs.dev/axios
    '@nuxtjs/axios',
    '@nuxtjs/i18n',
    '@nuxtjs/auth-next',
    '@nuxtjs/tailwindcss'
  ],

  i18n: {
    /* module options */
    locales: [
      { code: 'en', iso: 'en-US', name: 'English' },
      { code: 'fr', iso: 'fr-FR', name: 'Français' }
    ],
    defaultLocale: 'fr',
    vueI18n: {
      fallbackLocale: 'en',
      messages: {
        en: require('./locales/en.js'),
        fr: require('./locales/fr.js')
      }
    }
  },

  auth: {
    strategies: {
      local: {
        user: {
          property: ''
        },
        endpoints: {
          login: { url: '/auth', method: 'post' },
          logout: { url: '/logout', method: 'post' },
          user: { url: '/users', method: 'get' }
        },
        tokenType: ''
      }
    }
  },

  // Axios module configuration: https://go.nuxtjs.dev/config-axios
  axios: {
    // Workaround to avoid enforcing hard-coded localhost:3000: https://github.com/nuxt-community/axios-module/issues/308
    baseURL: process.env.BASE_URL
  },

  // Build Configuration: https://go.nuxtjs.dev/config-build
  build: {
  }

  // serverMiddleware: [
  //   ''
  // ],

  // router: {
  //   base: '/',
  //   middleware: [
  //     'auth'
  //   ]
  // }
}
