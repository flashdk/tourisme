module.exports = {
  root: true,
  env: {
    browser: true,
    node: true
  },
  extends: [
    '@nuxtjs/eslint-config-typescript',
    'plugin:nuxt/recommended'
  ],
  plugins: [],
  // add your custom rules here
  rules: {
    'vue/multi-word-component-names': 'off',
    'no-console': 'off',
    camelcase: 'off',
    'vue/html-closing-bracket-newline': 'off',
    'vue/script-setup-uses-vars': 'off',
    'vue/multiline-html-element-content-newline': 'off',
    'vue/attributes-order': 'warn',
    'vue/no-multi-spaces': 'error',
    'vue/order-in-components': 'error',
    'vue/max-attributes-per-line': 'off',
    'vue/html-quotes': ['error', 'double'],
    'vue/html-self-closing': ['error', {
      html: {
        void: 'any',
        normal: 'any',
        component: 'always'
      }
    }],
    'vue/html-closing-bracket-spacing': ['error', {
      startTag: 'never',
      endTag: 'never',
      selfClosingTag: 'always'
    }],
    'vue/component-name-in-template-casing': ['error', 'PascalCase', {
      ignores: ['nuxt-link', 'no-ssr', 'nuxt']
    }],
    'vue/singleline-html-element-content-newline': 'off'
  }
}
